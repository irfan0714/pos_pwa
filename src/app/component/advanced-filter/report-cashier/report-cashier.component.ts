import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-report-cashier',
  templateUrl: './report-cashier.component.html',
  styleUrls: ['./report-cashier.component.scss'],
})
export class ReportCashierComponent implements OnInit {

  @Input() cashierList: any[];
  @Input() counterList: any[];
  @Input() userList: any[];
  @Input() typeList: any[];
  @Input() customerResellerList: any[];
  @Input() paramCounterId: any;
  @Input() paramCashierId: any;
  @Input() paramUserId: any;
  @Input() paramTransactionTypeId: any;
  @Input() paramRekapPer: any;
  @Input() paramPeriodStart: any;
  @Input() paramPeriodEnd: any;
  @Input() paramReceiptNumber: any;
  @Input() paramTypeId: any;
  @Input() paramResellerId: any;

  baseUrl: any;
  db: any;
  counterId: any;
  cashierId: any;
  userId: any;
  typeId: any;
  resellerId: any;
  transactionTypeId: any = '0';
  rekapPer: any = '0';
  periodStart: any;
  periodEnd: any;
  periodYear: any;
  receiptNumber: any;

  counterNameCheckBox: any[] = [];
  showCounterCheckBox: boolean = false;

  constructor(
    public modalController: ModalController,
    private utilService: UtilService,
    private toastCtrl: ToastController,
    private alertController: AlertController,
  ) {
    this.baseUrl = environment.apiUrl;
  }

  ngOnInit() {

    let todayDate = new Date();
    let convertDateStart = this.paramPeriodStart ? this.utilService.convertDate(this.paramPeriodStart) : this.utilService.convertDate(todayDate);
    let convertDateEnd = this.paramPeriodEnd ? this.utilService.convertDate(this.paramPeriodEnd) : this.utilService.convertDate(todayDate);
    let startDate = convertDateStart.years + '-' + convertDateStart.months + '-' + convertDateStart.dates;
    let endDate = convertDateEnd.years + '-' + convertDateEnd.months + '-' + convertDateEnd.dates;
    this.periodStart = startDate;
    this.periodEnd = endDate;

    this.transactionTypeId = this.paramTransactionTypeId ? this.paramTransactionTypeId : '0';
    this.rekapPer = this.paramRekapPer ? this.paramRekapPer : '0';
    this.receiptNumber = this.paramReceiptNumber ? this.paramReceiptNumber : '';
    this.typeId = this.paramTypeId ? parseInt(this.paramTypeId) : '';
    this.counterId = this.paramCounterId ? this.paramCounterId : '';
    this.cashierId = this.paramCashierId ? parseInt(this.paramCashierId) : '';
    this.userId = this.paramUserId ? parseInt(this.paramUserId) : '';
    this.resellerId = this.paramResellerId ? parseInt(this.paramResellerId) : '';

    let counterIdSplit = this.counterId !== '' ? String(this.counterId).split(',') : [];
    for(let x = 0; x < this.counterList.length; x++) {
      let isChecked = false;
      if(counterIdSplit.length > 0) {
        let findId = counterIdSplit.indexOf(String(this.counterList[x].id));
        isChecked = findId !== -1 ? true : false;
      }

      let data = {
        'counter_id': this.counterList[x].id,
        'counter_name': this.counterList[x].counter_name,
        'isChecked': isChecked
      };

      this.counterNameCheckBox.push(data);
    }
  }

  async dismissModal(code: any) {
    let dataReturn: any;

    let listCounterId: any = '';
    if(this.counterNameCheckBox.length > 0) {
      for(let i = 0; i < this.counterNameCheckBox.length; i++) {
        if(this.counterNameCheckBox[i].isChecked === true) {
          if(listCounterId === '') {
            listCounterId = this.counterNameCheckBox[i].counter_id;
          } else {
            listCounterId = listCounterId + ',' + this.counterNameCheckBox[i].counter_id;
          }
        }
      }

      this.counterId = listCounterId;
    }

    if(code === 'generate') {
      if(this.transactionTypeId === '0' || this.transactionTypeId === '1') {
        dataReturn = this.transactionTypeId + '#' + this.rekapPer + '#' + this.periodStart +
          '#' + this.periodEnd + '#' + this.receiptNumber + '#' + this.counterId + '#' + this.cashierId +
          '#' + this.userId + '#' + this.typeId;
      }

      if(this.transactionTypeId === '2') {
        dataReturn = this.transactionTypeId + '#' + this.rekapPer + '#' + this.periodStart +
          '#' + this.periodEnd + '#' + this.receiptNumber + '#' + this.counterId + '#' + this.cashierId +
          '#' + this.userId + '#' + this.typeId + '#' + this.resellerId;
      }
    }
    if(code === 'close') {
      dataReturn = 'null';
    }
    
    await this.modalController.dismiss(dataReturn);
  }

  showCounterListCheckBox() {
    if(this.showCounterCheckBox === false) {
      this.showCounterCheckBox = true;
    } else {
      this.showCounterCheckBox = false;
    }
  }

  /*openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  getCounterList() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let selectQuery = 'SELECT * FROM mst_counters';
      this.db.transaction((tx) => {
        tx.executeSql(selectQuery, [],
          (tx, result) => {
            if(result.rows.length > 0) {
              this.utilService.loadingDismiss();
              this.counterList = [];
              for(let x = 0; x < result.rows.length; x++) {
                let counterData = {
                  'id': result.rows[x].id,
                  'counter_name': result.rows[x].counter_name
                };
                this.counterList.push(counterData);
              }
            }
        }, (error) => {
          this.utilService.loadingDismiss();
          console.log(error);
        });
      });
    });
  }

  getCashierList() {
    let selectQuery = 'SELECT * FROM mst_cashiers';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.cashierList = [];
            for(let x = 0; x < result.rows.length; x++) {
              let cashierData = {
                'id': result.rows[x].id,
                'cashier_name': result.rows[x].cashier_name
              };
              this.cashierList.push(cashierData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getUserList() {
    let selectQuery = 'SELECT * FROM users';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.userList = [];
            for(let x = 0; x < result.rows.length; x++) {
              let userData = {
                'id': result.rows[x].id,
                'user_name': result.rows[x].name
              };
              this.userList.push(userData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }*/

}
