import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';

@Component({
  selector: 'app-report-product-request',
  templateUrl: './report-product-request.component.html',
  styleUrls: ['./report-product-request.component.scss'],
})
export class ReportProductRequestComponent implements OnInit {

  @Input() counterList: any[];
  @Input() warehouseList: any[];
  @Input() paramProductRequestId: any;
  @Input() paramCounterId: any;
  @Input() paramWarehouseId: any;
  @Input() paramDocDateStart: any;
  @Input() paramDocDateEnd: any;
  @Input() paramNeedDateStart: any;
  @Input() paramNeedDateEnd: any;

  productRequestId: any;
  counterId: any;
  warehouseId: any;
  docDateStart: any;
  docDateEnd: any;
  needDateStart: any;
  needDateEnd: any;

  constructor(
    public modalController: ModalController,
    private utilService: UtilService) { }

  ngOnInit() {
    let convertDate = this.utilService.convertDate(new Date());
    let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;
    this.productRequestId = this.paramProductRequestId ? this.paramProductRequestId : null;
    this.docDateStart = this.paramDocDateStart ? this.paramDocDateStart : todayDate;
    this.docDateEnd = this.paramDocDateEnd ? this.paramDocDateEnd : todayDate;
    this.needDateStart = this.paramNeedDateStart ? this.paramNeedDateStart : todayDate;
    this.needDateEnd = this.paramNeedDateEnd ? this.paramNeedDateEnd : todayDate;
    this.counterId = this.paramCounterId ? parseInt(this.paramCounterId) : parseInt(this.counterList[0].id);
    this.warehouseId = this.paramWarehouseId ? parseInt(this.paramWarehouseId) : parseInt(this.warehouseList[0].id);
  }

  changeWarehouseList(event: any) {
    let counterId = event.target.value;
    if(counterId) {
      let filterWarehouse = this.warehouseList.filter(x => x.counter_id === counterId);
      this.warehouseList = filterWarehouse;
      this.warehouseList.unshift({
        'id': 0,
        'counter_id': 0,
        'warehouse_name': 'SEMUA GUDANG'
      });
    }
  }

  async dismissModal(code: any) {
    let dataReturn: any[] = [];

    if(code === 'generate') {
      let convertDocDateStart = this.utilService.convertDate(this.docDateStart);
      let convertDocDateEnd = this.utilService.convertDate(this.docDateEnd);
      let convertNeedDateStart = this.utilService.convertDate(this.needDateStart);
      let convertNeedDateEnd = this.utilService.convertDate(this.needDateEnd);
      let counterSelected = this.counterId ? this.counterList.find(x => x.id === this.counterId) : null;
      let warehouseSelected = this.warehouseId !== null || this.warehouseId !== undefined ? this.warehouseList.find(x => x.id === this.warehouseId) : null;
      
      let data = {
        productRequestId: this.productRequestId ? this.productRequestId : null,
        counterId: this.counterId,
        counterName: counterSelected !== null ? counterSelected.counter_name : null,
        warehouseId: this.warehouseId,
        warehouseName : warehouseSelected !== null ? warehouseSelected.warehouse_name : null,
        docDateStart: convertDocDateStart.years + '-' + convertDocDateStart.months + '-' + convertDocDateStart.dates,
        docDateEnd: convertDocDateEnd.years + '-' + convertDocDateEnd.months + '-' + convertDocDateEnd.dates,
        needDateStart: convertNeedDateStart.years + '-' + convertNeedDateStart.months + '-' + convertNeedDateStart.dates,
        needDateEnd: convertNeedDateEnd.years + '-' + convertNeedDateEnd.months + '-' + convertNeedDateEnd.dates
      };

      dataReturn.push(data);
    }

    if(code === 'close') {
      dataReturn = [];
    }

    await this.modalController.dismiss(dataReturn);
  }

}
