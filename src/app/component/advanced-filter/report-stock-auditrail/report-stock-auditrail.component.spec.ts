import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportStockAuditrailComponent } from './report-stock-auditrail.component';

describe('ReportStockAuditrailComponent', () => {
  let component: ReportStockAuditrailComponent;
  let fixture: ComponentFixture<ReportStockAuditrailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportStockAuditrailComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportStockAuditrailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
