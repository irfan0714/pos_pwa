import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { IonicSelectableComponent } from 'ionic-selectable';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-report-stock-auditrail',
  templateUrl: './report-stock-auditrail.component.html',
  styleUrls: ['./report-stock-auditrail.component.scss'],
})
export class ReportStockAuditrailComponent implements OnInit {

  @Input() productList: Product[];
  @Input() counterList: any[];
  @Input() warehouseList: any[];
  @Input() paramMonthId: any;
  @Input() paramYear: any;
  @Input() paramCounterId: any;
  @Input() paramProduct: any;
  @Input() paramWarehouseId: any;
  
  productSearch: Product[];
  counterId: any;
  productId: any;
  warehouseId: any;
  monthId: any;
  year: any;
  monthList: any[] = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  
  constructor(
    public modalController: ModalController,
    private utilService: UtilService) { }

  ngOnInit() {
    let convertDate = this.utilService.convertDate(new Date());
    let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

    this.monthId = this.paramMonthId ? this.paramMonthId : 0;
    this.year = this.paramYear ? this.paramYear + '-01-01' : todayDate;
    this.counterId = this.paramCounterId ? parseInt(this.paramCounterId) : parseInt(this.counterList[0].id);
    this.productId = this.paramProduct ? this.paramProduct : null;
    this.warehouseId = this.paramWarehouseId ? parseInt(this.paramWarehouseId) : parseInt(this.warehouseList[0].id);
  }

  changeWarehouseList(event: any) {
    let counterId = event.target.value;
    if(counterId) {
      let filterWarehouse = this.warehouseList.filter(x => x.counter_id === counterId);
      this.warehouseList = filterWarehouse;
      this.warehouseList.unshift({
        'id': 0,
        'counter_id': 0,
        'warehouse_name': 'SEMUA GUDANG'
      });
    }
  }

  async dismissModal(code: any) {
    let dataReturn: any[] = [];
    let convertDate = this.utilService.convertDate(this.year);

    if(code === 'generate') {
      let counterSelected = this.counterId ? this.counterList.find(x => x.id === this.counterId) : null;
      let warehouseSelected = this.warehouseId !== null || this.warehouseId !== undefined ? this.warehouseList.find(x => x.id === this.warehouseId) : null;
      
      let data = {
        monthId: this.monthId,
        monthName: this.monthList[this.monthId],
        year: convertDate.years,
        counterId: this.counterId,
        counterName: counterSelected !== null ? counterSelected.counter_name : null,
        productId: this.productId,
        warehouseId: this.warehouseId,
        warehouseName : warehouseSelected !== null ? warehouseSelected.warehouse_name : null
      };

      dataReturn.push(data);
    }

    if(code === 'close') {
      dataReturn = [];
    }

    await this.modalController.dismiss(dataReturn);
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    event.component.endSearch();
  }

}
