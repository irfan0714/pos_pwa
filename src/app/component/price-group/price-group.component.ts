import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-price-group',
  templateUrl: './price-group.component.html',
  styleUrls: ['./price-group.component.scss'],
})
export class PriceGroupComponent implements OnInit {

  @Input() productId: any;
  @Input() productName: any;
  @Input() productPrice: any;

  newProductPrice: number = 0;

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {}

  async dismissModal() {
    let close: any[] = [];

    if(this.newProductPrice !== 0) {
      let productData = {
        product_id: this.productId,
        price: this.newProductPrice
      };

      close.push(productData);
    } else {
      let productData = {
        product_id: this.productId,
        price: this.productPrice
      };

      close.push(productData);
    }
    
    await this.modalController.dismiss(close);
  }

}
