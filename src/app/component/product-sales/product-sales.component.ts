import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-product-sales',
  templateUrl: './product-sales.component.html',
  styleUrls: ['./product-sales.component.scss'],
})
export class ProductSalesComponent implements OnInit {

  @Input() pCode: any;
  @Input() productName: any;
  @Input() qtyFree: any;
  @Input() valPromoNominal: any;
  @Input() valPromoPercentage: any;

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  async dismissModal() {
    let close: any[] = [];

    let productSalesData = {
      qtyFree: this.qtyFree ? this.qtyFree : 0,
      valPromoNominal: this.valPromoNominal ? this.valPromoNominal : 0,
      valPromoPercentage: this.valPromoPercentage ? this.valPromoPercentage : 0
    };

    close.push(productSalesData);
    
    await this.modalController.dismiss(close);
  }

}
