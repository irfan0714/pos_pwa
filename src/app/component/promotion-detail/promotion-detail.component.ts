import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { UtilService } from '../../service/util.service';

@Component({
  selector: 'app-promotion-detail',
  templateUrl: './promotion-detail.component.html',
  styleUrls: ['./promotion-detail.component.scss'],
})
export class PromotionDetailComponent implements OnInit {

  productBarcode: any = "";
  @Input() mstBarcodeList: any[];
  @Input() mstProductList: any[];
  @Input() productScan: any;
  @Input() productFreeItem: any;
  @Input() action: any;
  productList: any[] = [];

  constructor(
    public modalController: ModalController,
    private utilService: UtilService,
    private toastCtrl: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    if(this.action === 'product') {
      if(this.productScan !== null) {
        this.setProductList(this.productScan);
      }
    }

    if(this.action === 'bonus') {
      if(this.productFreeItem !== null) {
        this.setProductList(this.productFreeItem);
      }
    }
  }

  setProductList(product: any) {
    this.productList = [];
    let listProduct: any[] = product.split(';');
    if(listProduct.length > 0) {
      for(let x = 0; x < listProduct.length; x++) {
        if(this.mstProductList[parseInt(listProduct[x])] !== undefined) {
          this.productList.push({
            id: this.mstProductList[parseInt(listProduct[x])]['id'],
            barcode: this.mstProductList[parseInt(listProduct[x])]['barcode'],
            name: this.mstProductList[parseInt(listProduct[x])]['product_name'],
            stock: this.mstProductList[parseInt(listProduct[x])]['stock'],
            terpakai: this.mstProductList[parseInt(listProduct[x])]['terpakai']
          });
        }
      }
    }
  }

  async dismissModal() {
    await this.modalController.dismiss(this.productList.length > 0 ? this.productList : '');
  }

  scrollDownEnd() {
    setTimeout(()=>{   
        var elem = document.getElementById('item');
        elem.scrollTop = elem.scrollHeight;
    }, 50);
  }

  getProduct(productBarcode: any) {
    if(productBarcode !== '' && productBarcode !== undefined) {
      let checkItemBarcode: any[] = this.productList.find(x => x.barcode === productBarcode);
      let checkItemPcode: any[] = this.productList.find(x => x.id === productBarcode);

      if (checkItemBarcode === undefined && checkItemPcode === undefined) {
        if (this.mstBarcodeList[productBarcode] !== undefined) {
          this.productList.push({
            id: this.mstBarcodeList[productBarcode]['id'],
            barcode: this.mstBarcodeList[productBarcode]['barcode'],
            name: this.mstBarcodeList[productBarcode]['product_name'],
            stock: this.mstBarcodeList[productBarcode]['stock'],
            terpakai: this.mstBarcodeList[productBarcode]['terpakai']
          });
        } else if (this.mstProductList[parseInt(productBarcode)] !== undefined) {
          this.productList.push({
            id: this.mstProductList[parseInt(productBarcode)]['id'],
            barcode: this.mstProductList[parseInt(productBarcode)]['barcode'],
            name: this.mstProductList[parseInt(productBarcode)]['product_name'],
            stock: this.mstProductList[parseInt(productBarcode)]['stock'],
            terpakai: this.mstProductList[parseInt(productBarcode)]['terpakai']
          });
        } else {
          let message = 'Barcode tidak valid, coba gunakan kode produk.';
          this.showAlertBarcodeNotValid(message);
        }
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Product sudah ada di list!' }).then(t => t.present());
      }
    }

    this.productBarcode = "";
  }

  async showAlertBarcodeNotValid(message: any) {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: message,
      backdropDismiss: true,
      buttons: [
        {
          text: 'OK',
          handler: () => {}
        }
      ]
    });

    await alert.present();
  }

  deleteDetail(index: any) {
    this.productList.splice(index, 1);
  }

}
