import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-promotion-free-item',
  templateUrl: './promotion-free-item.component.html',
  styleUrls: ['./promotion-free-item.component.scss'],
})
export class PromotionFreeItemComponent implements OnInit {

  @Input() freeItemList: any[];

  constructor(
    public modalController: ModalController) { }

  ngOnInit() { }

  async dismissModal() {
    await this.modalController.dismiss('');
  }

  async chooseProduct(data: any) {
    await this.modalController.dismiss(data);
  }

}
