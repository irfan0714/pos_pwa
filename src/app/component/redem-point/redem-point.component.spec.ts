import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RedemPointComponent } from './redem-point.component';

describe('RedemPointComponent', () => {
  let component: RedemPointComponent;
  let fixture: ComponentFixture<RedemPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedemPointComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RedemPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
