import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportCashierCounterListComponent } from './report-cashier-counter-list.component';

describe('ReportCashierCounterListComponent', () => {
  let component: ReportCashierCounterListComponent;
  let fixture: ComponentFixture<ReportCashierCounterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportCashierCounterListComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportCashierCounterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
