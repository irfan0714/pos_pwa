export enum OtherExpencesStatus {
    Pending = 0,
    Ok = 1,
    Void = 2
}