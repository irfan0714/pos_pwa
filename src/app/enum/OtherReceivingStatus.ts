export enum OtherReceivingStatus {
    Pending = 0,
    Ok = 1,
    Void = 2
}