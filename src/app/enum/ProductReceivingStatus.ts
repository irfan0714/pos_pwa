export enum ProductReceivingStatus {
    Open = 0,
    Closed = 1,
    Void = 2
}