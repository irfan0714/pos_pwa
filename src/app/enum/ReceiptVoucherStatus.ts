export enum ReceiptVoucherStatus {
    Pending = 0,
    Ok = 1,
    Void = 2
}