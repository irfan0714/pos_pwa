export enum StockOpnameStatus {
    Pending = 0,
    Closed = 1,
    Void = 2
}