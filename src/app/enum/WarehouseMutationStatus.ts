export enum WarehouseMutationStatus {
    Pending = 0,
    Ok = 1,
    Void = 2,
    Reject = 3
}