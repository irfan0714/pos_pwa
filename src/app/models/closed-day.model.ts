export class ClosedDay {
    
    year: string;
    month: string;
    year_before: string;
    month_before: string;
    counter_id: number;

    constructor(model?) {
        model = model || {};
        this.year = model.year || null;
        this.month = model.month || null;
        this.year_before = model.year_before || null;
        this.month_before = model.month_before || null;
        this.counter_id = model.counter_id || 0;
    }
}