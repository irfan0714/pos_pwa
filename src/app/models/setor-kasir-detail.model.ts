export class SetorKasirDetail {

    id: number;
    cashier_deposite_id: string;
    payment_method_id: number;

    constructor(model?) {
        model = model || {};
        this.id = model.id || 0;
        this.cashier_deposite_id = model.counter_id || null;
        this.payment_method_id = model.user_id || 0;
    }
}