export class UserProfile {
    email: string;
    id: number;
    name: string;
    role_id: number;
    branch_id: number;
    counter_id: number;
    username: string;
    created_by: string;
    created_at: string;
    updated_by: string;
    updated_at: string;
    counter_detail: any;
    branch_detail: any;
    counter_id_list: any;
    role_detail: any;

    /**
     *
     * @param model
     */
     constructor(model?) {
        model = model || {};
        this.email = model.email || null;
        this.id = model.id || 0;
        this.name = model.name || null;
        this.branch_id = model.branch_id || 0;
        this.role_id = model.role_id || 0;
        this.counter_id = model.counter_id || 0;
        this.username = model.username || null;
        this.created_by = model.created_by || null;
        this.created_at = model.created_at || null;
        this.updated_by = model.updated_by || null;
        this.updated_at = model.updated_at || null;
        this.counter_detail = model.counter_detail || null;
        this.branch_detail = model.branch_detail || null;
        this.counter_id_list = model.counter_id_list || null;
        this.role_detail = model.role_detail || null;
     }
}