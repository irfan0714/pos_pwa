import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CashierCreatePage } from './cashier-create.page';

const routes: Routes = [
  {
    path: '',
    component: CashierCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CashierCreatePageRoutingModule {}
