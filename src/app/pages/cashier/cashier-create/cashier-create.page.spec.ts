import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CashierCreatePage } from './cashier-create.page';

describe('CashierCreatePage', () => {
  let component: CashierCreatePage;
  let fixture: ComponentFixture<CashierCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashierCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CashierCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
