import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CashierEditPage } from './cashier-edit.page';

const routes: Routes = [
  {
    path: '',
    component: CashierEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CashierEditPageRoutingModule {}
