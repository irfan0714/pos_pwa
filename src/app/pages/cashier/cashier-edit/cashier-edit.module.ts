import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CashierEditPageRoutingModule } from './cashier-edit-routing.module';

import { CashierEditPage } from './cashier-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CashierEditPageRoutingModule
  ],
  declarations: [CashierEditPage]
})
export class CashierEditPageModule {}
