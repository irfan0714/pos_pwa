import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CashierEditPage } from './cashier-edit.page';

describe('CashierEditPage', () => {
  let component: CashierEditPage;
  let fixture: ComponentFixture<CashierEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashierEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CashierEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
