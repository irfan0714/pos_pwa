import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CashierPageRoutingModule } from './cashier-routing.module';

import { CashierPage } from './cashier.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CashierPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [CashierPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class CashierPageModule {}
