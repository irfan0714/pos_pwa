import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClosedDayPage } from './closed-day.page';

const routes: Routes = [
  {
    path: '',
    component: ClosedDayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClosedDayPageRoutingModule {}
