import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClosedDayPageRoutingModule } from './closed-day-routing.module';

import { ClosedDayPage } from './closed-day.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ClosedDayPageRoutingModule
  ],
  declarations: [ClosedDayPage]
})
export class ClosedDayPageModule {}
