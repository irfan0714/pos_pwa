import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClosedDayPage } from './closed-day.page';

describe('ClosedDayPage', () => {
  let component: ClosedDayPage;
  let fixture: ComponentFixture<ClosedDayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosedDayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClosedDayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
