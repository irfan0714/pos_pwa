import { TestBed } from '@angular/core/testing';

import { ClosedDayService } from './closed-day.service';

describe('ClosedDayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClosedDayService = TestBed.get(ClosedDayService);
    expect(service).toBeTruthy();
  });
});
