import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClosedMonthPage } from './closed-month.page';

const routes: Routes = [
  {
    path: '',
    component: ClosedMonthPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClosedMonthPageRoutingModule {}
