import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClosedMonthPage } from './closed-month.page';

describe('ClosedMonthPage', () => {
  let component: ClosedMonthPage;
  let fixture: ComponentFixture<ClosedMonthPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosedMonthPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClosedMonthPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
