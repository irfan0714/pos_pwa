import { TestBed } from '@angular/core/testing';

import { ClosedMonthService } from './closed-month.service';

describe('ClosedMonthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClosedMonthService = TestBed.get(ClosedMonthService);
    expect(service).toBeTruthy();
  });
});
