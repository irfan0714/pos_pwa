import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CounterCreatePage } from './counter-create.page';

const routes: Routes = [
  {
    path: '',
    component: CounterCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CounterCreatePageRoutingModule {}
