import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CounterCreatePageRoutingModule } from './counter-create-routing.module';

import { CounterCreatePage } from './counter-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CounterCreatePageRoutingModule
  ],
  declarations: [CounterCreatePage]
})
export class CounterCreatePageModule {}
