import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CounterCreatePage } from './counter-create.page';

describe('CounterCreatePage', () => {
  let component: CounterCreatePage;
  let fixture: ComponentFixture<CounterCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CounterCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
