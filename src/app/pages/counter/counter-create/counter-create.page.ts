import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { UtilService } from '../../../service/util.service';
import { UserProfile } from '../../../models/user-profile.model';
import { CounterService } from '../counter.service';
import { MstCounter } from '../../../models/mst-counter.model';

@Component({
  selector: 'app-counter-create',
  templateUrl: './counter-create.page.html',
  styleUrls: ['./counter-create.page.scss'],
})
export class CounterCreatePage implements OnInit {

  token: any;
  formCounterCreate: FormGroup;
  branchList: any[] = [];
  userProfile: UserProfile = new UserProfile();

  constructor(
    private fb: FormBuilder,
    private storage: Storage,
    private utilService: UtilService,
    private alertController: AlertController,
    private toastCtrl: ToastController,
    private navCtrl: NavController,
    private counterService: CounterService
  ) { }

  ngOnInit() {
    this.buildFormCounterCreate();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.utilService.loadingPresent('Harap tunggu...')
        .then(() => {
          this.userProfile = new UserProfile(profile);
          this.counterService.getCounterforCreate({ "token": this.token }).subscribe((response) => {
            this.utilService.loadingDismiss();
            this.branchList = response.results.branch_data;
          });
        });
      }
    });
  }

  buildFormCounterCreate() {
    this.formCounterCreate = this.fb.group({
      branchId: [0, Validators.required],
      counterCode: [null, Validators.required],
      counterName: [null, Validators.required],
      transDate: [null, Validators.required],
      firstAddress: [null, Validators.required],
      lastAddress: [null, Validators.required],
      phone: [null, Validators.required],
      footerText: [null, Validators.required],
      timezone: [null, Validators.required]
    });
  }

  inputCounter() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const counterForm = this.formCounterCreate.getRawValue();
      let dateConvert = this.utilService.convertDate(counterForm.transDate);
      let newTransDate = dateConvert.years + '-' + dateConvert.months + '-' + dateConvert.dates;
      let getDatePeriodStock = new Date(new Date(newTransDate).getFullYear(), new Date().getMonth() + 1, 0);
      let periodConvert = this.utilService.convertDate(getDatePeriodStock);
      let periodStock = periodConvert.years + '-' + periodConvert.months + '-' + periodConvert.dates;
      const mstCounter = new MstCounter();
      mstCounter.branch_id = counterForm.branchId;
      mstCounter.counter_code = counterForm.counterCode;
      mstCounter.counter_name = counterForm.counterName;
      mstCounter.trans_date = newTransDate;
      mstCounter.stock_period = periodStock;
      mstCounter.first_address = counterForm.firstAddress;
      mstCounter.last_address = counterForm.lastAddress;
      mstCounter.phone = counterForm.phone;
      mstCounter.footer_text = counterForm.footerText;
      mstCounter.timezone = counterForm.timezone;
      mstCounter.created_by = this.userProfile.username;

      this.counterService.addCounter(mstCounter).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      })
    });
  }

  async showConfirmInput() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/counter']);;
          }
        }
      ]
    });

    await alert.present();
  }

}
