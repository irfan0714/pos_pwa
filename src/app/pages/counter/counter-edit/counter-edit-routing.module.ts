import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CounterEditPage } from './counter-edit.page';

const routes: Routes = [
  {
    path: '',
    component: CounterEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CounterEditPageRoutingModule {}
