import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CounterEditPage } from './counter-edit.page';

describe('CounterEditPage', () => {
  let component: CounterEditPage;
  let fixture: ComponentFixture<CounterEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CounterEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
