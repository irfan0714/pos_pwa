import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { CounterService } from '../counter.service';
import { MstCounter } from '../../../models/mst-counter.model';

@Component({
  selector: 'app-counter-edit',
  templateUrl: './counter-edit.page.html',
  styleUrls: ['./counter-edit.page.scss'],
})
export class CounterEditPage implements OnInit {

  formCounterEdit: FormGroup;
  counterId: any;
  counterData: any;
  token: any;
  branchList: any[] = [];
  statusCounter: any[] = ['Not Active', 'Active'];
  userProfile: UserProfile = new UserProfile();

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private counterService: CounterService
  ) { }

  ngOnInit() {
    this.buildFormCounterEdit();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getData();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  buildFormCounterEdit() {
    this.formCounterEdit = this.fb.group({
      branchId: [],
      counterCode: [],
      counterName: [],
      transDate: [],
      stockPeriod: [],
      firstAddress: [],
      lastAddress: [],
      phone: [],
      footerText: [],
      timezone: [],
      activeStatus: []
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.counterId = snapshot.counterId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.counterService.getCounterforEdit(this.counterId, { "token": this.token }).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.counterData = response.results.data;
          this.branchList = response.results.branch_data;

          this.formCounterEdit = this.fb.group({
            branchId: [this.counterData.branch_id, Validators.required],
            counterCode: [this.counterData.counter_code, Validators.required],
            counterName: [this.counterData.counter_name, Validators.required],
            transDate: [this.counterData.trans_date, Validators.required],
            stockPeriod: [this.counterData.stock_period, Validators.required],
            firstAddress: [this.counterData.first_address],
            lastAddress: [this.counterData.last_address],
            phone: [this.counterData.phone],
            footerText: [this.counterData.footer_text],
            timezone: [this.counterData.timezone],
            activeStatus: [parseInt(this.counterData.active), Validators.required]
          });
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  updateData() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const counterForm = this.formCounterEdit.getRawValue();
      let dateConvert = this.utilService.convertDate(counterForm.transDate);
      let newTransDate = dateConvert.years + '-' + dateConvert.months + '-' + dateConvert.dates;
      let dateConvert2 = this.utilService.convertDate(counterForm.stockPeriod);
      let newStockPeriod = dateConvert2.years + '-' + dateConvert2.months + '-' + dateConvert2.dates;


      const mstCounter = new MstCounter();
      mstCounter.branch_id = counterForm.branchId;
      mstCounter.counter_code = counterForm.counterCode;
      mstCounter.counter_name = counterForm.counterName;
      mstCounter.trans_date = newTransDate;
      mstCounter.stock_period = newStockPeriod;
      mstCounter.first_address = counterForm.firstAddress;
      mstCounter.last_address = counterForm.lastAddress;
      mstCounter.phone = counterForm.phone;
      mstCounter.footer_text = counterForm.footerText;
      mstCounter.timezone = counterForm.timezone;
      mstCounter.active = counterForm.activeStatus;
      mstCounter.updated_by = this.userProfile.username;

      this.counterService.updateCounter(this.counterId, mstCounter).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 200) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/counter']);;
          }
        }
      ]
    });

    await alert.present();
  }

}
