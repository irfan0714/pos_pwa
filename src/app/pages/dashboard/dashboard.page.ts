import {  AfterViewInit, Component, ElementRef, ViewChild  } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { Chart } from 'chart.js';
import { UserData } from '../../providers/user-data';
import { UtilService } from '../../service/util.service';
import { DashboardService } from './dashboard.service';
import { Storage } from '@ionic/storage';
import { UserProfile } from '../../models/user-profile.model';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements AfterViewInit {

  @ViewChild('lineCanvas', {static: true}) private lineCanvas: ElementRef;
  lineChart: any;
  listCounter: any = [];
  chartData: any = [];
  chartDataReseller: any = [];
  totalData: number;
  token: any;
  userProfile: UserProfile = new UserProfile();

  constructor(
    private dashboardService: DashboardService,
    private utilService: UtilService,
    private toastCtrl: ToastController,
    private userData: UserData,
    private navCtrl: NavController,
    private storage: Storage
  ) { }
  
  ionViewDidEnter() {
    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  ngAfterViewInit() {
    this.getCounter();
  }

  getCounter() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let options = {
        "token": this.token
      };
      this.dashboardService.getCounter(options).subscribe((response) => {
        this.utilService.loadingDismiss();
        this.listCounter = response.results.data_percounter;
        this.chartData = response.results.data_perbulan;
        this.chartDataReseller = response.results.data_perbulan_reseller;

        this.lineChartMethod();
        this.totalData = 0;
        for (let i = 0; i < this.listCounter.length; i++) {
            this.totalData += parseInt(this.listCounter[i].total);
        }
        
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }
  lineChartMethod() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      // options: {
      //   scales: {
      //     xAxes: [{
      //       ticks: {}
      //     }],
      //     yAxes: [{
      //       ticks: {
      //         beginAtZero: true,
      //         stepSize: 500000,
      //           // Return an empty string to draw the tick line but hide the tick label
      //           // Return `null` or `undefined` to hide the tick line entirely
      //           userCallback: function(value, index, values) {
      //             // Convert the number to a string and splite the string every 3 charaters from the end
      //             value = value.toString();
      //             value = value.split(/(?=(?:...)*$)/);
      //             // Convert the array to a string and format the output
      //             value = value.join(',');
      //             return '' + value;
      //            }
      //       }
      //     }]
      //   },
      // },
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'],
        datasets: [
          {
            label: 'Penjualan Reguler 2022',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#3880ff',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            data: this.chartData,
            spanGaps: false,
          },
          
          {
            label: 'Penjualan Reseller 2022',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#ffc409',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            data: this.chartDataReseller,
            spanGaps: false,
          }
        ]
      }
    });
  }


}
