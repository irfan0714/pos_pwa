import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GudangCreatePage } from './gudang-create.page';

const routes: Routes = [
  {
    path: '',
    component: GudangCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GudangCreatePageRoutingModule {}
