import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GudangCreatePage } from './gudang-create.page';

describe('GudangCreatePage', () => {
  let component: GudangCreatePage;
  let fixture: ComponentFixture<GudangCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GudangCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GudangCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
