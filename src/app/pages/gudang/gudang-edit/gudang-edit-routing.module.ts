import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GudangEditPage } from './gudang-edit.page';

const routes: Routes = [
  {
    path: '',
    component: GudangEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GudangEditPageRoutingModule {}
