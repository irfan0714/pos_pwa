import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GudangEditPageRoutingModule } from './gudang-edit-routing.module';

import { GudangEditPage } from './gudang-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    GudangEditPageRoutingModule
  ],
  declarations: [GudangEditPage]
})
export class GudangEditPageModule {}
