import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GudangEditPage } from './gudang-edit.page';

describe('GudangEditPage', () => {
  let component: GudangEditPage;
  let fixture: ComponentFixture<GudangEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GudangEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GudangEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
