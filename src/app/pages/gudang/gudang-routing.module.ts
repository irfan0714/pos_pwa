import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GudangPage } from './gudang.page';

const routes: Routes = [
  {
    path: '',
    component: GudangPage
  },
  {
    path: 'gudang-create',
    loadChildren: () => import('./gudang-create/gudang-create.module').then( m => m.GudangCreatePageModule)
  },
  {
    path: 'gudang-edit',
    loadChildren: () => import('./gudang-edit/gudang-edit.module').then( m => m.GudangEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GudangPageRoutingModule {}
