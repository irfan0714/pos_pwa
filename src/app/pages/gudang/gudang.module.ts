import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GudangPageRoutingModule } from './gudang-routing.module';

import { GudangPage } from './gudang.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GudangPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [GudangPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class GudangPageModule {}
