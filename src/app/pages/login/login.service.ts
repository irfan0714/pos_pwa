import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl: any;

  constructor(
    private http: HttpClient
  ) {
    this.baseUrl = environment.apiUrl;
  }

  userLogin(username: string, password: string): Observable<any> {
    const reqOpts = new HttpHeaders()
      .set('Accept', 'application/json'),
    formData = new FormData();
    formData.append('username', username);
    formData.append('password', password);

    return this.http.post(this.baseUrl + 'signin', formData, { headers: reqOpts })
  }
}
