import { Component, ViewChild, OnInit } from '@angular/core';
import { IonInput, ToastController, NavController, AlertController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import { UtilService } from '../../service/util.service';
import { Storage } from '@ionic/storage';
import { LoginService } from './login.service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('username', {static: false}) userUsernameInput: IonInput;
  @ViewChild('password', {static: false}) userPasswordInput: IonInput;

  login: UserOptions = { username: '', password: '' };
  submitted = false;

  constructor(
    public userData: UserData,
    public router: Router,
    private toastCtrl: ToastController,
    private utilService: UtilService,
    private storage: Storage,
    private loginService: LoginService,
    private navCtrl: NavController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.checkLogin();
  }

  ionViewDidEnter() {
    this.checkLogin();
  }

  checkLogin() {
    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.navCtrl.navigateForward(['/home']);
      }
    });
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      
      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.loginService.userLogin(this.login.username, this.login.password)
        .subscribe((response: any) => {
          this.utilService.loadingDismiss();
          if(response.token) {
            this.storage.set('hasLoggedIn', true);
            this.storage.set('username', this.login.username);
            this.storage.set('user_password', this.login.password);
            this.storage.set('user_token', response.token);
            this.storage.set('user_profile', response.user);
            window.dispatchEvent(new CustomEvent('user:login'));
            this.router.navigateByUrl('/home');
          } else {
            this.utilService.loadingDismiss();
            this.toastCtrl.create({ duration: 2000, message: 'Email atau Password salah!' }).then(t => t.present());
          }
        }, (err) => {
          let errMessage: any = err.error.message ? err.error.message : 'Terdapat error koneksi!';
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: errMessage }).then(t => t.present());
        });
      });
    }
  }

  moveField(code: any) {
    switch(code.name) {

        case "email":
          setTimeout(() => {
            this.userUsernameInput.setFocus();
          }, 300);
        break;

        case "password":
          setTimeout(() => {
            this.userPasswordInput.setFocus();
          }, 300);
        break;

        default:
          break;
    }
  }

  clearCache() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      caches.keys().then(function(names) {
        for(let name of names) {
          console.log(name);
          caches.delete(name);
        }
      });

      this.utilService.loadingDismiss();
      this.showNotifAfterUpdates("Berhasil lakukan update!");
    });
  }

  async showNotifAfterUpdates(messageValue: any) {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: messageValue,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            window.location.href = "./";
          }
        }
      ]
    });

    await alert.present();
  }
}
