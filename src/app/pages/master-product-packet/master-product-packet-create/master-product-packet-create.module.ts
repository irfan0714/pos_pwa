import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MasterProductPacketCreatePageRoutingModule } from './master-product-packet-create-routing.module';

import { MasterProductPacketCreatePage } from './master-product-packet-create.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    MasterProductPacketCreatePageRoutingModule
  ],
  declarations: [MasterProductPacketCreatePage]
})
export class MasterProductPacketCreatePageModule {}
