import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MasterProductPacketCreatePage } from './master-product-packet-create.page';

describe('MasterProductPacketCreatePage', () => {
  let component: MasterProductPacketCreatePage;
  let fixture: ComponentFixture<MasterProductPacketCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterProductPacketCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MasterProductPacketCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
