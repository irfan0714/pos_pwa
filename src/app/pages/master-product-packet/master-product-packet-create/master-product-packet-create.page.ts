import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { MasterProductPacketService } from '../master-product-packet.service';
import { MstProductPackageBundle } from '../../../models/mst-product-package-bundle.model';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-master-product-packet-create',
  templateUrl: './master-product-packet-create.page.html',
  styleUrls: ['./master-product-packet-create.page.scss'],
})
export class MasterProductPacketCreatePage implements OnInit {

  token: any;
  productSearch: Product[];
  productList: Product[];
  userProfile: UserProfile = new UserProfile();
  counterList: any[] = [];
  warehouseListMaster: any[] = [];
  warehouseList: any[] = [];
  brandList: any[] = [];
  subBrandListMaster: any[] = [];
  subBrandList: any[] = [];
  typeList: any[] = [];
  subTypeListMaster: any[] = [];
  subTypeList: any[] = [];
  categoryList: any[] = [];
  subCategoryListMaster: any[] = [];
  subCategoryList: any[] = [];
  marketingTypeList: any[] = [];

  formProductPackageCreate: FormGroup;

  db: any;

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private userData: UserData,
    private packageService: MasterProductPacketService
  ) { }

  ngOnInit() {
    this.buildFormProductPackage();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getProductPackageforCreate();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  buildFormProductPackage() {
    this.formProductPackageCreate = this.fb.group({
      counterId: [null, Validators.required],
      warehouseId: [null, Validators.required],
      productPackageName: [null, Validators.required],
      productPackageInitialName: [null, Validators.required],
      brandId: [null, Validators.required],
      subBrandId: [null, Validators.required],
      typeId: [null, Validators.required],
      subTypeId: [null, Validators.required],
      categoryId: [null, Validators.required],
      subCategoryId: [null, Validators.required],
      marketingTypeId: [null, Validators.required],
      barcode: [0, Validators.required],
      price: [0, Validators.required],
      length: [0, Validators.required],
      width: [0, Validators.required],
      height: [0, Validators.required],
      allocationQty: [0, Validators.required],
      detail: this.fb.array([])
    });
  }

  addDetail() {
    const detail = this.fb.group({
      product: [null, Validators.required],
      price: [0, Validators.required],
      qty: [0, Validators.required]
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formProductPackageCreate.get('detail'));
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    event.component.endSearch();
  }

  deleteDetail(i: any) {
    this.getDetailArray.removeAt(i);
  }

  getProductPackageforCreate() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let options = { 
        "token": this.token,
        "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
      };

      this.packageService.getProductPackageforCreate(options).subscribe((response) => {
        this.utilService.loadingDismiss();
        if (response.status.code === 200) {
          this.counterList = response.results.mst_counter_data;
          this.warehouseListMaster = response.results.mst_warehouse_data;
          this.productList = response.results.mst_product_data;
          this.brandList = response.results.mst_product_brand_data;
          this.subBrandListMaster = response.results.mst_product_sub_brand_data;
          this.typeList = response.results.mst_product_type_data;
          this.subTypeListMaster = response.results.mst_product_sub_type_data;
          this.categoryList = response.results.mst_product_category_data;
          this.subCategoryListMaster = response.results.mst_product_sub_category_data;
          this.marketingTypeList = response.results.mst_product_marketing_type_data;
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  getWarehouseList(event) {
    let counterId = event.target.value;
    if(this.warehouseListMaster.length > 0) {
      this.warehouseList = [];
      this.warehouseList = this.warehouseListMaster.filter(x => x.counter_id === counterId);
    } else {
      this.toastCtrl.create({ duration: 2000, message: 'Data Gudang kosong!' }).then(t => t.present());
    }

    this.refreshForm('warehouseId');
  }

  getSubBrandList(event) {
    let brandId = event.target.value;
    if(this.subBrandListMaster.length > 0) {
      this.subBrandList = [];
      this.subBrandList = this.subBrandListMaster.filter(x => x.product_brand_id === brandId);
    } else {
      this.toastCtrl.create({ duration: 2000, message: 'Data Product Sub Brand kosong!' }).then(t => t.present());
    }

    this.refreshForm('subBrandId');
  }

  getSubTypeList(event) {
    let typeId = event.target.value;
    if(this.subTypeListMaster.length > 0) {
      this.subTypeList = [];
      this.subTypeList = this.subTypeListMaster.filter(x => x.product_type_id === typeId);
    } else {
      this.toastCtrl.create({ duration: 2000, message: 'Data Product Sub Type kosong!' }).then(t => t.present());
    }

    this.refreshForm('subTypeId');
  }

  getSubCategoryList(event) {
    let categoryId = event.target.value;
    if(this.subCategoryListMaster.length > 0) {
      this.subCategoryList = [];
      this.subCategoryList = this.subCategoryListMaster.filter(x => x.product_category_id === categoryId);
    } else {
      this.toastCtrl.create({ duration: 2000, message: 'Data Product Sub Category kosong!' }).then(t => t.present());
    }

    this.refreshForm('subCategoryId');
  }

  refreshForm(action: any) {
    const form = this.formProductPackageCreate.value;
    let detailArray: any[] = form.detail;
    this.formProductPackageCreate = this.fb.group({
      counterId: [form.counterId, Validators.required],
      warehouseId: [action === 'warehouseId' ? null : form.warehouseId, Validators.required],
      productPackageName: [form.productPackageName, Validators.required],
      productPackageInitialName: [form.productPackageInitialName, Validators.required],
      brandId: [form.brandId, Validators.required],
      subBrandId: [action === 'subBrandId' ? null : form.subBrandId, Validators.required],
      typeId: [form.typeId, Validators.required],
      subTypeId: [action === 'subTypeId' ? null : form.subTypeId, Validators.required],
      categoryId: [form.categoryId, Validators.required],
      subCategoryId: [action === 'subCategoryId' ? null : form.subCategoryId, Validators.required],
      marketingTypeId: [form.marketingTypeId, Validators.required],
      barcode: [form.barcode, Validators.required],
      price: [form.price, Validators.required],
      length: [form.length, Validators.required],
      width: [form.width, Validators.required],
      height: [form.height, Validators.required],
      allocationQty: [form.allocationQty, Validators.required],
      detail: this.fb.array([])
    });
    
    if(detailArray.length > 0) {
      for(let i = 0; i < detailArray.length; i++) {
        let productId: string = detailArray[i].product.id.trim().toLowerCase();
        let filterProduct = this.filterProducts(this.productList, productId);
        const detail = this.fb.group({
          product: [filterProduct[0], Validators.required],
          price: [detailArray[i].price, Validators.required],
          qty: [detailArray[i].qty, Validators.required]
        });
        
        this.getDetailArray.push(detail);
      }
    }
  }

  inputData() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let convertDate = this.utilService.convertDate(new Date());
      let transDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;
      const formProductPackage = this.formProductPackageCreate.value;
      let arrProduct: any = [];
      let arrPrice: any = [];
      let arrQty: any = [];
      let arrCreatedBy: any = [];

      let arrStockMutationTypeId: any = [];
      let arrValue: any = [];
      let arrStockMove: any = [];
      let arrTransDate: any = [];

      for(let x = 0; x < formProductPackage.detail.length; x++) {
        arrProduct[x] = formProductPackage.detail[x].product.id;
        arrPrice[x] = parseInt(formProductPackage.detail[x].price);
        arrQty[x] = parseInt(formProductPackage.detail[x].qty);
        arrCreatedBy[x] = this.userProfile.username;

        arrStockMutationTypeId[x] = 'MP'; // MP = MUTASI PAKET
        arrValue[x] = 0;
        arrStockMove[x] = 'O';
        arrTransDate[x] = transDate;
      }

      const mstProductPackageBundle = new MstProductPackageBundle();
      mstProductPackageBundle.allocationQty = formProductPackage.allocationQty;
      mstProductPackageBundle.mstProduct.product_brand_id = formProductPackage.brandId;
      mstProductPackageBundle.mstProduct.product_sub_brand_id = parseInt(formProductPackage.subBrandId) < 1000 ? "0" + formProductPackage.subBrandId : formProductPackage.subBrandId;
      mstProductPackageBundle.mstProduct.product_type_id = formProductPackage.typeId;
      mstProductPackageBundle.mstProduct.product_sub_type_id = parseInt(formProductPackage.subTypeId) < 1000 ? "0" + formProductPackage.subTypeId : formProductPackage.subTypeId;
      mstProductPackageBundle.mstProduct.product_category_id = formProductPackage.categoryId;
      mstProductPackageBundle.mstProduct.product_sub_category_id = parseInt(formProductPackage.subCategoryId) < 1000 ? "0" + formProductPackage.subCategoryId : formProductPackage.subCategoryId;
      mstProductPackageBundle.mstProduct.product_marketing_type_id = formProductPackage.marketingTypeId;
      mstProductPackageBundle.mstProduct.barcode = formProductPackage.barcode;
      mstProductPackageBundle.mstProduct.product_name = formProductPackage.productPackageName;
      mstProductPackageBundle.mstProduct.initial_name = formProductPackage.productPackageInitialName;
      mstProductPackageBundle.mstProduct.unit = 'PKT';
      mstProductPackageBundle.mstProduct.price = formProductPackage.price;
      mstProductPackageBundle.mstProduct.length_size = formProductPackage.length;
      mstProductPackageBundle.mstProduct.width_size = formProductPackage.width;
      mstProductPackageBundle.mstProduct.height_size = formProductPackage.height;
      mstProductPackageBundle.mstProduct.created_by = this.userProfile.username;

      mstProductPackageBundle.mstProductPackage.product_id = arrProduct;
      mstProductPackageBundle.mstProductPackage.price = arrPrice;
      mstProductPackageBundle.mstProductPackage.qty = arrQty;
      mstProductPackageBundle.mstProductPackage.created_by = arrCreatedBy;

      mstProductPackageBundle.stockMutation.stock_mutation_type_id = arrStockMutationTypeId;
      mstProductPackageBundle.stockMutation.product_id = arrProduct;
      mstProductPackageBundle.stockMutation.qty = arrQty;
      mstProductPackageBundle.stockMutation.value = arrValue;
      mstProductPackageBundle.stockMutation.stock_move = arrStockMove;
      mstProductPackageBundle.stockMutation.trans_date = arrTransDate;

      mstProductPackageBundle.mstCounter.id = formProductPackage.counterId;
      
      mstProductPackageBundle.mstWarehouse.id = formProductPackage.warehouseId;

      this.packageService.addProductPackage(mstProductPackageBundle).subscribe((response: any) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmInput() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/master-product-packet']);;
          }
        }
      ]
    });

    await alert.present();
  }

}
