import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasterProductPacketEditPage } from './master-product-packet-edit.page';

const routes: Routes = [
  {
    path: '',
    component: MasterProductPacketEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterProductPacketEditPageRoutingModule {}
