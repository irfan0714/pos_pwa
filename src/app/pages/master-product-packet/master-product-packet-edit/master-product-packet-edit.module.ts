import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MasterProductPacketEditPageRoutingModule } from './master-product-packet-edit-routing.module';

import { MasterProductPacketEditPage } from './master-product-packet-edit.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    MasterProductPacketEditPageRoutingModule
  ],
  declarations: [MasterProductPacketEditPage]
})
export class MasterProductPacketEditPageModule {}
