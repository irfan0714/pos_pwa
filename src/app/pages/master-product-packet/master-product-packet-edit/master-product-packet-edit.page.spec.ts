import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MasterProductPacketEditPage } from './master-product-packet-edit.page';

describe('MasterProductPacketEditPage', () => {
  let component: MasterProductPacketEditPage;
  let fixture: ComponentFixture<MasterProductPacketEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterProductPacketEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MasterProductPacketEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
