import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../../service/util.service';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { IonicSelectableComponent } from 'ionic-selectable';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MasterProductPacketService } from '../master-product-packet.service';
import { MstProductPackageBundle } from '../../../models/mst-product-package-bundle.model';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-master-product-packet-edit',
  templateUrl: './master-product-packet-edit.page.html',
  styleUrls: ['./master-product-packet-edit.page.scss'],
})
export class MasterProductPacketEditPage implements OnInit {

  token: any;
  userProfile: UserProfile = new UserProfile();
  productPackageId: any;
  productList: Product[];
  brandList: any[] = [];
  subBrandList: any[] = [];
  typeList: any[] = [];
  subTypeList: any[] = [];
  categoryList: any[] = [];
  subCategoryList: any[] = [];
  marketingTypeList: any[] = [];
  mstProductData: any;
  productPackageData: any[] = [];
  createdBy: any;
  createdAt: any;
  updatedBy: any;
  updatedAt: any;

  formProductPackageEdit: FormGroup;
  db: any;

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private utilService: UtilService,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private alertController: AlertController,
    private storage: Storage,
    private packageService: MasterProductPacketService
  ) { }

  ngOnInit() {
    this.buildFormProductPackageEdit();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getData();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.productPackageId = snapshot.productPackageId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.packageService.getProductPackageforEdit(this.productPackageId, { "token": this.token }).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.productPackageData = response.results.mst_product_package_data;
          this.mstProductData = response.results.mst_product_data;
          this.productList = response.results.mst_product_all_data;
          this.brandList = response.results.mst_product_brand_data;
          this.subBrandList = response.results.mst_product_sub_brand_data;
          this.typeList = response.results.mst_product_type_data;
          this.subTypeList = response.results.mst_product_sub_type_data;
          this.categoryList = response.results.mst_product_category_data;
          this.subCategoryList = response.results.mst_product_sub_category_data;
          this.marketingTypeList = response.results.mst_product_marketing_type_data;

          let userCreate: any = response.results.user_create_data;
          this.createdBy = userCreate ? userCreate[0].name : null;
          let userUpdate: any = response.results.user_update_data;
          this.updatedBy = userUpdate ? userUpdate[0].name : null;
          if(this.mstProductData.length > 0) {
            this.createdAt = this.mstProductData[0].created_at;
            this.updatedAt = this.mstProductData[0].updated_at;

            this.formProductPackageEdit = this.fb.group({
              productPackageId: [this.mstProductData[0].id],
              productPackageName: [this.mstProductData[0].product_name],
              productPackageInitialName: [this.mstProductData[0].initial_name],
              brandId: [this.mstProductData[0].product_brand_id],
              subBrandId: [parseInt(this.mstProductData[0].product_sub_brand_id)],
              typeId: [this.mstProductData[0].product_type_id],
              subTypeId: [parseInt(this.mstProductData[0].product_sub_type_id)],
              categoryId: [this.mstProductData[0].product_category_id],
              subCategoryId: [parseInt(this.mstProductData[0].product_sub_category_id)],
              marketingTypeId: [parseInt(this.mstProductData[0].product_marketing_type_id)],
              barcode: [this.mstProductData[0].barcode],
              price: [this.mstProductData[0].price],
              length: [this.mstProductData[0].length_size],
              width: [this.mstProductData[0].width_size],
              height: [this.mstProductData[0].height_size],
              detail: this.fb.array([])
            });
          }

          if(this.productPackageData.length > 0) {
            for(let i = 0; i < this.productPackageData.length; i++) {
              const detail = this.fb.group({
                productId: [this.productPackageData[i].product_id],
                product: [this.productPackageData[i].product_name],
                price: [this.productPackageData[i].price],
                qty: [this.productPackageData[i].qty]
              });
              this.getDetailArray.push(detail);
            }
          } else {
            this.getDetailArray.clear();
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  buildFormProductPackageEdit() {
    this.formProductPackageEdit = this.fb.group({
      productPackageId: [null],
      productPackageName: [null],
      productPackageInitialName: [null],
      brandId: [null],
      subBrandId: [null],
      typeId: [null],
      subTypeId: [null],
      categoryId: [null],
      subCategoryId: [null],
      marketingTypeId: [null],
      barcode: [0],
      price: [0],
      length: [0],
      width: [0],
      height: [0],
      detail: this.fb.array([])
    });
  }

  addDetail() {
    const detail = this.fb.group({
      product: [null],
      price: [0],
      qty: [0]
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formProductPackageEdit.get('detail'));
  }

}
