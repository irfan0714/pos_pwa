import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasterProductPacketPage } from './master-product-packet.page';

const routes: Routes = [
  {
    path: '',
    component: MasterProductPacketPage
  },
  {
    path: 'master-product-packet-create',
    loadChildren: () => import('./master-product-packet-create/master-product-packet-create.module').then( m => m.MasterProductPacketCreatePageModule)
  },
  {
    path: 'master-product-packet-edit',
    loadChildren: () => import('./master-product-packet-edit/master-product-packet-edit.module').then( m => m.MasterProductPacketEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterProductPacketPageRoutingModule {}
