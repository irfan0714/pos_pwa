import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MasterProductPacketPageRoutingModule } from './master-product-packet-routing.module';

import { MasterProductPacketPage } from './master-product-packet.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MasterProductPacketPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [MasterProductPacketPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MasterProductPacketPageModule {}
