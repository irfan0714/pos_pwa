import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MasterProductPacketPage } from './master-product-packet.page';

describe('MasterProductPacketPage', () => {
  let component: MasterProductPacketPage;
  let fixture: ComponentFixture<MasterProductPacketPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterProductPacketPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MasterProductPacketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
