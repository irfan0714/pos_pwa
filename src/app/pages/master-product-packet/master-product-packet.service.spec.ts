import { TestBed } from '@angular/core/testing';

import { MasterProductPacketService } from './master-product-packet.service';

describe('MasterProductPacketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MasterProductPacketService = TestBed.get(MasterProductPacketService);
    expect(service).toBeTruthy();
  });
});
