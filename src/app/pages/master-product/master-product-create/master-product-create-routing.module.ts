import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasterProductCreatePage } from './master-product-create.page';

const routes: Routes = [
  {
    path: '',
    component: MasterProductCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterProductCreatePageRoutingModule {}
