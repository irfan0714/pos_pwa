import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MasterProductCreatePageRoutingModule } from './master-product-create-routing.module';

import { MasterProductCreatePage } from './master-product-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MasterProductCreatePageRoutingModule
  ],
  declarations: [MasterProductCreatePage]
})
export class MasterProductCreatePageModule {}
