import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MasterProductCreatePage } from './master-product-create.page';

describe('MasterProductCreatePage', () => {
  let component: MasterProductCreatePage;
  let fixture: ComponentFixture<MasterProductCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterProductCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MasterProductCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
