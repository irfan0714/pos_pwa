import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { MasterProductService } from '../master-product.service';
import { MstProduct } from '../../../models/mst-product.model';

@Component({
  selector: 'app-master-product-create',
  templateUrl: './master-product-create.page.html',
  styleUrls: ['./master-product-create.page.scss'],
})
export class MasterProductCreatePage implements OnInit {

  token: any;
  userProfile: UserProfile = new UserProfile();
  brandList: any[] = [];
  subBrandList: any[] = [];
  subBrandListServer: any[] = [];
  typeList: any[] = [];
  subTypeList: any[] = [];
  subTypeListServer: any[] = [];
  categoryList: any[] = [];
  subCategoryList: any[] = [];
  subCategoryListServer: any[] = [];
  marketingTypeList: any[] = [];

  formMstProductCreate: FormGroup;

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private userData: UserData,
    private mstProductService: MasterProductService
  ) { }

  ngOnInit() {
    this.buildFormMstProductCreate();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getMstProductforCreate();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  buildFormMstProductCreate() {
    this.formMstProductCreate = this.fb.group({
      productName: [null, Validators.required],
      initialName: [null, Validators.required],
      barcode: [null, Validators.required],
      marketingTypeId: [null, Validators.required],
      brandId: [null, Validators.required],
      subBrandId: [null, Validators.required],
      typeId: [null, Validators.required],
      subTypeId: [null, Validators.required],
      categoryId: [null, Validators.required],
      subCategoryId: [null, Validators.required],
      price: [0, Validators.required]
    });
  }

  getMstProductforCreate() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      this.mstProductService.getProductForCreate({ "token": this.token }).subscribe((response) => {
        this.utilService.loadingDismiss();
        if (response.status.code === 200) {
          this.brandList = response.results.mst_product_brand_data;
          this.subBrandList = response.results.mst_product_sub_brand_data;
          this.subBrandListServer = response.results.mst_product_sub_brand_data;
          this.typeList = response.results.mst_product_type_data;
          this.subTypeList = response.results.mst_product_sub_type_data;
          this.subTypeListServer = response.results.mst_product_sub_type_data;
          this.categoryList = response.results.mst_product_category_data;
          this.subCategoryList = response.results.mst_product_sub_category_data;
          this.subCategoryListServer = response.results.mst_product_sub_category_data;
          this.marketingTypeList = response.results.mst_product_marketing_type_data;
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  getSubTypeList(event) {
    let productTypeId = event.target.value;
    let subTypeList = this.subTypeListServer.filter(x => x.product_type_id === productTypeId);
    this.subTypeList = subTypeList ? subTypeList : this.subTypeListServer;
  }

  getSubCategoryList(event) {
    let productCategoryId = event.target.value;
    let subCategoryList = this.subCategoryListServer.filter(x => x.product_category_id === productCategoryId);
    this.subCategoryList = subCategoryList ? subCategoryList : this.subCategoryListServer;
  }

  getSubBrandList(event) {
    let productBrandId = event.target.value;
    let subBrandList = this.subBrandListServer.filter(x => x.product_brand_id === productBrandId);
    this.subBrandList = subBrandList ? subBrandList : this.subBrandListServer;
  }

  inputProduct() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formMstProduct = this.formMstProductCreate.value;
      const mstProduct = new MstProduct();
      mstProduct.product_name = formMstProduct.productName;
      mstProduct.initial_name = formMstProduct.initialName;
      mstProduct.barcode = formMstProduct.barcode;
      mstProduct.product_brand_id = formMstProduct.brandId;
      mstProduct.product_sub_brand_id = parseInt(formMstProduct.subBrandId) < 1000 ? "0" + formMstProduct.subBrandId : formMstProduct.subBrandId;
      mstProduct.product_type_id = formMstProduct.typeId;
      mstProduct.product_sub_type_id = parseInt(formMstProduct.subTypeId) < 1000 ? "0" + formMstProduct.subTypeId : formMstProduct.subTypeId;
      mstProduct.product_category_id = formMstProduct.categoryId;
      mstProduct.product_sub_category_id = parseInt(formMstProduct.subCategoryId) < 1000 ? "0" + formMstProduct.subCategoryId : formMstProduct.subCategoryId;
      mstProduct.product_marketing_type_id = formMstProduct.marketingTypeId;
      mstProduct.price = formMstProduct.price;
      mstProduct.created_by = this.userProfile.username;

      this.mstProductService.addProduct(mstProduct).subscribe((response: any) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmInput() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/master-product']);;
          }
        }
      ]
    });

    await alert.present();
  }

}
