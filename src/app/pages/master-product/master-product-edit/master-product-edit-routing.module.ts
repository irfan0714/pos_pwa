import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasterProductEditPage } from './master-product-edit.page';

const routes: Routes = [
  {
    path: '',
    component: MasterProductEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterProductEditPageRoutingModule {}
