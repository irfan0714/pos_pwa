import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MasterProductEditPageRoutingModule } from './master-product-edit-routing.module';

import { MasterProductEditPage } from './master-product-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MasterProductEditPageRoutingModule
  ],
  declarations: [MasterProductEditPage]
})
export class MasterProductEditPageModule {}
