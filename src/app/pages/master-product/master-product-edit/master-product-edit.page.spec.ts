import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MasterProductEditPage } from './master-product-edit.page';

describe('MasterProductEditPage', () => {
  let component: MasterProductEditPage;
  let fixture: ComponentFixture<MasterProductEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterProductEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MasterProductEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
