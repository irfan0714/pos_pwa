import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MasterProductPage } from './master-product.page';

describe('MasterProductPage', () => {
  let component: MasterProductPage;
  let fixture: ComponentFixture<MasterProductPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterProductPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MasterProductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
