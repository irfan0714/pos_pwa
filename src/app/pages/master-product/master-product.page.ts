import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, AlertController, ToastController, ModalController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { UtilService } from '../../service/util.service';
import { environment } from '../../../environments/environment';
import { UserProfile } from '../../models/user-profile.model';
import { UserData } from '../../providers/user-data';
import { MasterProductComponent } from '../../component/advanced-filter/master-product/master-product.component';
import { MasterProductService } from './master-product.service';
import { MstProduct } from '../../models/mst-product.model';

@Component({
  selector: 'app-master-product',
  templateUrl: './master-product.page.html',
  styleUrls: ['./master-product.page.scss'],
})
export class MasterProductPage implements OnInit {

  page = {
    limit: 10,
    count: 0,
    offset: 0,
    orderBy: 'Menu_Name',
    orderDir: 'desc'
  };

  baseUrl: any;
  rows: any;
  token: any;
  userProfile: UserProfile = new UserProfile();
  productList: any;
  filteredData: any;

  brandList: any[] = [];
  typeList: any[] = [];
  categoryList: any[] = [];

  brandId: any;
  typeId: any;
  categoryId: any;

  keywordSearch: any;

  constructor(
    private storage: Storage,
    private navCtrl: NavController,
    private alertController: AlertController,
    private utilService: UtilService,
    private toastCtrl: ToastController,
    private userData: UserData,
    private modalController: ModalController,
    private mstProductService: MasterProductService
    ) {
      this.baseUrl = environment.apiUrl;
    }

  ngOnInit() {
  }

  ionViewDidEnter() {
    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.utilService.loadingPresent('Harap tunggu...')
        .then(() => {
          this.mstProductService.getDataforAdvancedFilter({"token": this.token})
          .subscribe((response) => {
            this.utilService.loadingDismiss();
            this.brandList = response.results.brand_data;
            this.typeList = response.results.type_data;
            this.categoryList = response.results.category_data;

            this.pageCallback({ offset: this.page.offset });
          }, () => {
            this.utilService.loadingDismiss();
            this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
            this.pageCallback({ offset: this.page.offset });
          });
        });
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  pageCallback(pageInfo: { count?: number, pageSize?: number, limit?: number, offset?: number }) {
    this.page.offset = pageInfo.offset;
    this.reloadTable();
  }
  
  sortCallback(sortInfo: { sorts: { dir: string, prop: string }[], column: {}, prevValue: string, newValue: string }) {
    this.page.orderDir = sortInfo.sorts[0].dir;
    this.page.orderBy = sortInfo.sorts[0].prop;
    this.reloadTable();
  }

  reloadTable() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let offset = this.page.offset + 1;
      let options = {
        "token": this.token,
        "page": offset.toString(),
        "keywords": this.keywordSearch ? this.keywordSearch : 'null',
        "product_brand_id": this.brandId ? this.brandId : 'null',
        "product_type_id": this.typeId ? this.typeId : 'null',
        "product_category_id": this.categoryId ? this.categoryId : 'null'
      };

      this.mstProductService.getProduct(options)
      .subscribe((response) => {
        this.utilService.loadingDismiss();
        this.page.count = response.results.total;
        this.rows = response.results.data;
        this.filteredData = response.results.data;
        this.productList = response.results.data;
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }

  pullProduct() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {

      const mstProduct = new MstProduct();
      mstProduct.created_by = this.userProfile.username;

      this.mstProductService.pullProduct(mstProduct).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 200) {
          this.showPullSuccess();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      })
    });
  }

  async showConfirmPull() {
    const alert = await this.alertController.create({
      header: 'Pull Confirmation',
      cssClass:'custom-alert-class',
      message: 'Apakah anda yakin untuk menarik data Product?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {}
        },
        {
          text: 'OK',
          handler: () => {
            this.pullProduct();
          }
        }
      ]
    });

    await alert.present();
  }

  async showPullSuccess() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Berhasil tarik data Product!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.pageCallback({ offset: this.page.offset });
          }
        }
      ]
    });

    await alert.present();
  }

  filterDatatable(event) {
    let val = event.target.value.toLowerCase();
    let columnLength = 9;
    let keys = Object.keys(this.productList[0]);
    this.rows = this.filteredData.filter(function(item){
      for (let i=0; i < columnLength; i++){
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val){
          return true;
        }
      }
    });

    this.page.offset = 0;
  }

  async openAdvancedFilters() {
    const modal = await this.modalController.create({
      component: MasterProductComponent,
      componentProps: {
        brandList: this.brandList,
        typeList: this.typeList,
        categoryList: this.categoryList,
        brandId: this.brandId,
        typeId: this.typeId,
        categoryId: this.categoryId
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data: any = modelData.data;

      if (data.length > 0) {
        this.keywordSearch = undefined;
        this.brandId = data[0].brand_id;
        this.typeId = data[0].type_id;
        this.categoryId = data[0].category_id;

        this.pageCallback({ offset: this.page.offset });
      }
    });

    return await modal.present();
  }

  searchWithKeywords() {
    this.brandId = undefined;
    this.typeId = undefined;
    this.categoryId = undefined;

    this.pageCallback({ offset: this.page.offset });
  }

  clearFilters() {
    this.keywordSearch = undefined;
    this.brandId = undefined;
    this.typeId = undefined;
    this.categoryId = undefined;

    this.pageCallback({ offset: this.page.offset });
  }

  goToMstProductCreate() {
    this.navCtrl.navigateForward(['/master-product/master-product-create']);
  }

  goToUpdatePage(id: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        PCode: id
      }
    };
    this.navCtrl.navigateForward(['/master-product/master-product-edit'], navigationExtras);
  }

  /*relog() {
    Promise.all([
      this.storage.get('user_email'),
      this.storage.get('user_password')
    ])
    .then(([email, password]) => {
      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.loginService.userLogin(email, password)
        .subscribe((response: any) => {
          this.utilService.loadingDismiss();
          if(response.token) {
            this.storage.set('user_token', response.token);
            this.toastCtrl.create({ duration: 2000, message: 'Refresh kembali laman ini.' }).then(t => t.present());
          } else {
            this.utilService.loadingDismiss();
            this.toastCtrl.create({ duration: 2000, message: 'Email atau Password salah!' }).then(t => t.present());
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }*/

}
