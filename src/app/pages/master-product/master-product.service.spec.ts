import { TestBed } from '@angular/core/testing';

import { MasterProductService } from './master-product.service';

describe('MasterProductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MasterProductService = TestBed.get(MasterProductService);
    expect(service).toBeTruthy();
  });
});
