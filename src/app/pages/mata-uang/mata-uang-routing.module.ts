import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MataUangPage } from './mata-uang.page';

const routes: Routes = [
  {
    path: '',
    component: MataUangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MataUangPageRoutingModule {}
