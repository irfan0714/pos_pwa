import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MataUangPage } from './mata-uang.page';

describe('MataUangPage', () => {
  let component: MataUangPage;
  let fixture: ComponentFixture<MataUangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MataUangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MataUangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
