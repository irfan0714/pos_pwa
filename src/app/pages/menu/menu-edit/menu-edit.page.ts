import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { Menu } from '../../../models/menu.model';
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-menu-edit',
  templateUrl: './menu-edit.page.html',
  styleUrls: ['./menu-edit.page.scss'],
})
export class MenuEditPage implements OnInit {

  token: any;
  userProfile: UserProfile = new UserProfile();
  formMenuEdit: FormGroup;
  menuId: any;
  menuData: any;
  parentMenuData: any[] = [];
  statusMenu: any[] = ['Not Active', 'Active'];

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private menuService: MenuService
  ) { }

  ngOnInit() {
    this.buildFormMenuEdit();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getData();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.menuId = snapshot.menuId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.menuService.getMenuforEdit(this.menuId, { "token": this.token }).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.menuData = response.results.data;
          this.parentMenuData = response.results.data_parent_menu;

          this.formMenuEdit = this.fb.group({
            parentId: [this.menuData.parent_id, Validators.required],
            menuName: [this.menuData.menu_name, Validators.required],
            root: [this.menuData.root, Validators.required],
            weight: [this.menuData.weight, Validators.required],
            icon: [this.menuData.icon, Validators.required],
            activeStatus: [parseInt(this.menuData.active), Validators.required]
          });
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  buildFormMenuEdit() {
    this.formMenuEdit = this.fb.group({
      parentId: [],
      menuName: [],
      root: [],
      weight: [],
      icon: [],
      activeStatus: []
    });
  }

  updateData() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const menuForm = this.formMenuEdit.getRawValue();
      const menu = new Menu();
      menu.parent_id = menuForm.parentId;
      menu.menu_name = menuForm.menuName;
      menu.root = menuForm.root;
      menu.weight = menuForm.weight;
      menu.icon = menuForm.icon;
      menu.active = menuForm.activeStatus;
      menu.updated_by = this.userProfile.username;

      this.menuService.updateMenu(this.menuId, menu).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 200) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/menu']);;
          }
        }
      ]
    });

    await alert.present();
  }

}
