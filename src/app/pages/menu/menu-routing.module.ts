import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage
  },
  {
    path: 'menu-create',
    loadChildren: () => import('./menu-create/menu-create.module').then( m => m.MenuCreatePageModule)
  },
  {
    path: 'menu-edit',
    loadChildren: () => import('./menu-edit/menu-edit.module').then( m => m.MenuEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
