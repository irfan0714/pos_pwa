import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalKasirCreatePageRoutingModule } from './modal-kasir-create-routing.module';

import { ModalKasirCreatePage } from './modal-kasir-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalKasirCreatePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ModalKasirCreatePage]
})
export class ModalKasirCreatePageModule {}
