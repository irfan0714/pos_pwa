import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalKasirCreatePage } from './modal-kasir-create.page';

describe('ModalKasirCreatePage', () => {
  let component: ModalKasirCreatePage;
  let fixture: ComponentFixture<ModalKasirCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalKasirCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalKasirCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
