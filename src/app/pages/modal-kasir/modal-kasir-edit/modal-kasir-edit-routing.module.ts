import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalKasirEditPage } from './modal-kasir-edit.page';

const routes: Routes = [
  {
    path: '',
    component: ModalKasirEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalKasirEditPageRoutingModule {}
