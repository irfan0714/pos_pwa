import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalKasirPage } from './modal-kasir.page';

describe('ModalKasirPage', () => {
  let component: ModalKasirPage;
  let fixture: ComponentFixture<ModalKasirPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalKasirPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalKasirPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
