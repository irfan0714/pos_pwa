import { TestBed } from '@angular/core/testing';

import { ModalKasirService } from './modal-kasir.service';

describe('ModalKasirService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModalKasirService = TestBed.get(ModalKasirService);
    expect(service).toBeTruthy();
  });
});
