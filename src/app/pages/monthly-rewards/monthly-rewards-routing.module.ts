import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonthlyRewardsPage } from './monthly-rewards.page';

const routes: Routes = [
  {
    path: '',
    component: MonthlyRewardsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonthlyRewardsPageRoutingModule {}
