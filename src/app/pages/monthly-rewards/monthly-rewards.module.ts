import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MonthlyRewardsPageRoutingModule } from './monthly-rewards-routing.module';

import { MonthlyRewardsPage } from './monthly-rewards.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MonthlyRewardsPageRoutingModule
  ],
  declarations: [MonthlyRewardsPage]
})
export class MonthlyRewardsPageModule {}
