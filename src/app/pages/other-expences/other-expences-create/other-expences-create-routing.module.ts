import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtherExpencesCreatePage } from './other-expences-create.page';

const routes: Routes = [
  {
    path: '',
    component: OtherExpencesCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherExpencesCreatePageRoutingModule {}
