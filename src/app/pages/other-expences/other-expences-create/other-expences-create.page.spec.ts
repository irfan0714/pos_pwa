import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtherExpencesCreatePage } from './other-expences-create.page';

describe('OtherExpencesCreatePage', () => {
  let component: OtherExpencesCreatePage;
  let fixture: ComponentFixture<OtherExpencesCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherExpencesCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtherExpencesCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
