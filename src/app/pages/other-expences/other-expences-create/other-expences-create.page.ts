import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController, ModalController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { OtherExpencesService } from '../other-expences.service';
import { OtherExpencesBundle } from '../../../models/other-expences-bundle.model';
import { FindProductComponent } from '../../../component/find-product/find-product.component';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-other-expences-create',
  templateUrl: './other-expences-create.page.html',
  styleUrls: ['./other-expences-create.page.scss'],
})
export class OtherExpencesCreatePage implements OnInit {

  token: any;
  productSearch: Product[];
  productList: Product[];
  userProfile: UserProfile = new UserProfile();
  counterData: any[] = [];
  warehouseData: any[] = [];
  purposeData: any[] = [];
  otherExpencesDetailList: any[] = [];

  formOtherExpencesCreate: FormGroup;
  otherExpencesId: any;

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private userData: UserData,
    private modalController: ModalController,
    private expencesService: OtherExpencesService
  ) { }

  ngOnInit() {
    this.buildFormOtherExpences();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getOtherExpencesForCreate();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getOtherExpencesForCreate() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let options = {
        "token": this.token,
        "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
      };

      this.expencesService.getOtherExpencesforCreate(options).subscribe((response) => {
        this.utilService.loadingDismiss();
        if (response.status.code === 200) {
          this.productList = response.results.product_data;
          this.counterData = response.results.counter_data;
          this.warehouseData = response.results.warehouse_data;
          this.purposeData = response.results.purpose_data;

          this.buildFormOtherExpences();
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  buildFormOtherExpences() {
    let newDate = new Date();
    let convertDate = this.utilService.convertDate(newDate);
    let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

    this.formOtherExpencesCreate = this.fb.group({
      counterId: [this.counterData.length > 0 ? parseInt(this.counterData[0].id) : null, Validators.required],
      warehouseId: [this.warehouseData.length > 0 ? parseInt(this.warehouseData[0].id) : null, Validators.required],
      purposeId: [this.purposeData.length > 0 ? parseInt(this.purposeData[0].id) : null, Validators.required],
      docDate: [todayDate, Validators.required],
      description: [null, Validators.required]
    });
  }

  inputBundle() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherExpences = this.formOtherExpencesCreate.value;
      let docDateConvert = this.utilService.convertDate(formOtherExpences.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < this.otherExpencesDetailList.length; x++) {
        let htmlIdQtyReceive: any = 'qtyReceive_' + x;
        let htmlIdDesc: any = 'description_' + x;
        let qtyReceive: any = (<HTMLInputElement>document.getElementById(htmlIdQtyReceive)).value;
        let description: any = (<HTMLInputElement>document.getElementById(htmlIdDesc)).value;

        arrProduct[x] = this.otherExpencesDetailList[x].product_id;
        arrQty[x] = parseInt(qtyReceive);
        arrUnitType[x] = 'PCS';
        arrDesc[x] = description;
      }

      const otherExpencesBundle = new OtherExpencesBundle();
      otherExpencesBundle.otherExpences.counter_id = formOtherExpences.counterId;
      otherExpencesBundle.otherExpences.warehouse_id = formOtherExpences.warehouseId;
      otherExpencesBundle.otherExpences.purpose_id = formOtherExpences.purposeId;
      otherExpencesBundle.otherExpences.doc_date = documentDate;
      otherExpencesBundle.otherExpences.desc = formOtherExpences.description;
      otherExpencesBundle.otherExpences.status = '0';
      otherExpencesBundle.otherExpences.created_by = this.userProfile.username;

      otherExpencesBundle.otherExpencesDetail.product_id = arrProduct;
      otherExpencesBundle.otherExpencesDetail.qty = arrQty;
      otherExpencesBundle.otherExpencesDetail.unit = arrUnitType;
      otherExpencesBundle.otherExpencesDetail.description = arrDesc;

      this.expencesService.addOtherExpencesBundle(otherExpencesBundle)
      .subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmInput() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/other-expences']);;
          }
        }
      ]
    });

    await alert.present();
  }

  addDetail() {
    let length = this.otherExpencesDetailList.length;
    this.otherExpencesDetailList.push({
      'id' : length + 1,
      'product_id' : null,
      'product_name' : null,
      'unit': null
    });
  }

  async findProduct(index: any) {
    const modal = await this.modalController.create({
      component: FindProductComponent,
      componentProps: {
        'productList': this.productList
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data = modelData.data;
      if(data) {
        let findProduct = this.otherExpencesDetailList.indexOf(data);
        if(findProduct === -1) {
          this.otherExpencesDetailList[index].product_id = data.id;
          this.otherExpencesDetailList[index].product_name = data.product_name;
        }
      }
    });

    return await modal.present();
  }

  deleteProduct(index: any) {
    this.otherExpencesDetailList.splice(index, 1);
  }

  /*inputHeader() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let todayDate = new Date();
      let convertDate = this.utilService.convertDate(todayDate);
      let years = convertDate.years.substr(2,2);
      let month = convertDate.months;

      const formOtherExpences = this.formOtherExpencesCreate.value;
      let counterId = formOtherExpences.counterId;
      let warehouseId = formOtherExpences.warehouseId;
      let purposeId = formOtherExpences.purposeId;
      let desc = formOtherExpences.description;

      let docDateConvert = this.utilService.convertDate(formOtherExpences.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      let options = {
        "token": this.token,
        "counter_id": counterId
      };
      
      this.expencesService.getOtherExpencesforCreate(options).subscribe((response) => {
        if(response.results.length > 0) {
          let latestOtherExpencesId: any = response.results[0].id;
          let splitId: any = latestOtherExpencesId.split('-');
          let lastCounter: any = splitId[0].substr(6, 3);
          let lastMonth: any = parseInt(splitId[0].substr(4, 2));
          let newCounter: any = (parseInt(lastCounter) + 1);
          if(lastMonth === parseInt(month)) {
            if(newCounter <= 9) { newCounter = '00' + newCounter.toString(); }
            if(newCounter > 9 && newCounter <= 99) { newCounter = '0' + newCounter.toString(); }
          } else {
            newCounter = '001';
          }

          this.otherExpencesId = 'PL' + years + month + newCounter + '-' + splitId[1];
        } else {
          let newCounterId: any;
          if(parseInt(counterId) <= 9) { newCounterId = '00' + counterId.toString(); }
          if(parseInt(counterId) > 9 && parseInt(counterId) <= 99) { newCounterId = '0' + counterId.toString(); }
          this.otherExpencesId = 'PL' + years + month + '001' + '-' + newCounterId;
        }

        const otherExpences = new OtherExpences();
        otherExpences.id = this.otherExpencesId;
        otherExpences.counter_id = counterId;
        otherExpences.warehouse_id = warehouseId;
        otherExpences.purpose_id = purposeId;
        otherExpences.doc_date = documentDate;
        otherExpences.desc = desc;
        otherExpences.status = '0';
        otherExpences.created_by = this.userProfile.username;

        this.expencesService.addOtherExepences(otherExpences).subscribe((response) => {
          this.utilService.loadingDismiss();
          if(response.status.code === 201) {
            this.saveOtherExpencesDetail(this.otherExpencesId);
          } else {
            this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }

  saveOtherExpencesDetail(otherExpencesId: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherExpencesDetail = this.formOtherExpencesCreate.value;
      let arrOtherExpencesId: any = [];
      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < formOtherExpencesDetail.detail.length; x++) {
        arrOtherExpencesId[x] = otherExpencesId;
        arrProduct[x] = formOtherExpencesDetail.detail[x].product.id;
        arrQty[x] = parseInt(formOtherExpencesDetail.detail[x].qty);
        arrUnitType[x] = 'PCS';
        arrDesc[x] = formOtherExpencesDetail.detail[x].desc;
      }

      const expencesDetail = new OtherExpencesDetail();
      expencesDetail.other_expences_id = arrOtherExpencesId;
      expencesDetail.product_id = arrProduct;
      expencesDetail.qty = arrQty;
      expencesDetail.unit = arrUnitType;
      expencesDetail.description = arrDesc;

      this.expencesService.addOtherExepencesDetail(expencesDetail).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }
  
  addDetail() {
    const detail = this.fb.group({
      product: [null, Validators.required],
      qty: [0, Validators.required],
      desc: [null, Validators.required]
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formOtherExpencesCreate.get('detail'));
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    event.component.endSearch();
  }

  deleteDetail(i: any) {
    this.getDetailArray.removeAt(i);
  }

  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  getCounterList() {
    let selectQuery = 'SELECT * FROM mst_counters';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.counterData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let dataCounter = {
                'id': result.rows[x].id,
                'counter_name': result.rows[x].counter_name
              };
              this.counterData.push(dataCounter);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getWarehouseList() {
    let selectQuery = 'SELECT * FROM mst_warehouses';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.warehouseData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let dataWarehouse = {
                'id': result.rows[x].id,
                'warehouse_name': result.rows[x].warehouse_name
              };
              this.warehouseData.push(dataWarehouse);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getPurposeList() {
    let selectQuery = 'SELECT * FROM mst_purposes';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.purposeData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let purposeData = {
                'id': result.rows[x].id,
                'purpose_name': result.rows[x].purpose_name
              };
              this.purposeData.push(purposeData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }
  */
}
