import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtherExpencesEditPageRoutingModule } from './other-expences-edit-routing.module';

import { OtherExpencesEditPage } from './other-expences-edit.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    OtherExpencesEditPageRoutingModule
  ],
  declarations: [OtherExpencesEditPage]
})
export class OtherExpencesEditPageModule {}
