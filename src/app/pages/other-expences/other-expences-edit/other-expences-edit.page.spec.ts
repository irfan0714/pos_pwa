import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtherExpencesEditPage } from './other-expences-edit.page';

describe('OtherExpencesEditPage', () => {
  let component: OtherExpencesEditPage;
  let fixture: ComponentFixture<OtherExpencesEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherExpencesEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtherExpencesEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
