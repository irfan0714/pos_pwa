import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController, ModalController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { OtherExpencesService } from '../other-expences.service';
import { OtherExpencesStatus } from '../../../enum/OtherExpencesStatus';
import { RoleAccess } from '../../../models/role-access.model';
import { OtherExpencesClosedBundle } from '../../../models/other-expences-cl-bundle.model';
import { FindProductComponent } from '../../../component/find-product/find-product.component';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-other-expences-edit',
  templateUrl: './other-expences-edit.page.html',
  styleUrls: ['./other-expences-edit.page.scss'],
})
export class OtherExpencesEditPage implements OnInit {

  token: any;
  productSearch: Product[];
  productList: Product[];
  userProfile: UserProfile = new UserProfile();
  counterData: any[] = [];
  warehouseData: any[] = [];
  purposeData: any[] = [];
  otherExpencesData: any;
  otherExpencesDetailData: any[] = [];
  statusOtherExpences: any = 0;
  otherExpencesDetailList: any[] = [];

  formOtherExpencesEdit: FormGroup;
  otherExpencesId: any;

  expencesStatus = OtherExpencesStatus;
  expencesStatusList = Object.keys(OtherExpencesStatus).filter(
    expencesStatus => typeof this.expencesStatus[expencesStatus] === 'number'
  );

  approvalHistoryData: any[] = [];
  createdBy: any;
  createdAt: any;
  updatedBy: any;
  updatedAt: any;

  userAccess: any[] = [];
  roleAccess = new RoleAccess();
  roleName: any;

  timezone: any[] = ['Asia/Jakarta', 'Asia/Makassar', 'Asia/Jayapura'];

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private activeRoute: ActivatedRoute,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private expencesService: OtherExpencesService,
    private router: Router,
    private modalController: ModalController,
    private userDataProvider: UserData
  ) { }

  ngOnInit() {
    this.buildFormOtherExpences();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile'),
      this.storage.get('user_menu_access')
    ])
    .then(([token, profile, access]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.userAccess = access;
        this.roleName = this.userProfile.role_detail ? this.userProfile.role_detail.role_name : null;
        this.roleAccess = this.userDataProvider.checkAccess(this.router.url, this.userAccess, this.userProfile);
        this.getData();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getAllProduct() {
    this.expencesService.getProduct({ "token": this.token }).subscribe((response) => {
      if (response.status.code === 200) {
        this.productList = response.results;
      }
    }, () => {
      this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
    });
  }

  buildFormOtherExpences() {
    let newDate = new Date();
    let convertDate = this.utilService.convertDate(newDate);
    let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

    this.formOtherExpencesEdit = this.fb.group({
      otherExpencesId: [this.otherExpencesId],
      counterId: [null],
      warehouseId: [null],
      purposeId: [null],
      docDate: [todayDate],
      description: [null],
      status: [null]
    });
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.otherExpencesId = snapshot.otherExpencesId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        let options = {
          "token": this.token,
          "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
        };

        this.expencesService.getOtherExpencesforEdit(this.otherExpencesId, options).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.otherExpencesData = response.results.other_expences_data;
          this.otherExpencesDetailData = response.results.other_expences_detail_data;
          this.productList = response.results.product_data;
          this.counterData = response.results.counter_data;
          this.warehouseData = response.results.warehouse_data;
          this.purposeData = response.results.purpose_data;

          let userCreate: any = response.results.user_create_data;
          this.createdBy = userCreate ? userCreate[0].name : null;
          let userUpdate: any = response.results.user_update_data;
          this.updatedBy = userUpdate ? userUpdate[0].name : null;
          this.approvalHistoryData = response.results.approval_history_data;
          
          if(this.otherExpencesData.length > 0) {
            this.createdAt = this.otherExpencesData[0].created_at;
            this.updatedAt = this.otherExpencesData[0].updated_at;
            this.statusOtherExpences = this.otherExpencesData[0].status;
            let statusName = this.expencesStatusList[parseInt(this.statusOtherExpences)];

            this.formOtherExpencesEdit = this.fb.group({
              otherExpencesId: [this.otherExpencesId],
              counterId: [this.otherExpencesData[0].counter_id],
              warehouseId: [this.otherExpencesData[0].warehouse_id],
              purposeId: [this.otherExpencesData[0].purpose_id],
              docDate: [this.otherExpencesData[0].doc_date],
              description: [this.otherExpencesData[0].desc],
              status: [statusName]
            });
          }

          if(this.otherExpencesDetailData.length > 0) {
            this.otherExpencesDetailList = [];
            for(let i = 0; i < this.otherExpencesDetailData.length; i++) {
              this.otherExpencesDetailList.push({
                'id' : i + 1,
                'product_id' : this.otherExpencesDetailData[i].product_id,
                'product_name' : this.otherExpencesDetailData[i].product_name,
                'qty_receive': this.otherExpencesDetailData[i].qty,
                'desc': this.otherExpencesDetailData[i].description
              });
            }
          } else {
            this.otherExpencesDetailList = [];
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  updateBundle(action: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherExpences = this.formOtherExpencesEdit.value;
      let docDateConvert = this.utilService.convertDate(formOtherExpences.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      const otherExpencesClosedBundle = new OtherExpencesClosedBundle();
      otherExpencesClosedBundle.otherExpences.id = this.otherExpencesId;
      otherExpencesClosedBundle.otherExpences.warehouse_id = formOtherExpences.warehouseId;
      otherExpencesClosedBundle.otherExpences.purpose_id = formOtherExpences.purposeId;
      otherExpencesClosedBundle.otherExpences.doc_date = documentDate;
      otherExpencesClosedBundle.otherExpences.desc = formOtherExpences.description;
      otherExpencesClosedBundle.otherExpences.status = action === 'data' ? '0' : '1';
      otherExpencesClosedBundle.otherExpences.updated_by = this.userProfile.username;

      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < this.otherExpencesDetailList.length; x++) {
        let htmlIdQtyReceive: any = 'qtyReceive_' + x;
        let htmlIdDesc: any = 'description_' + x;
        let qtyReceive: any = (<HTMLInputElement>document.getElementById(htmlIdQtyReceive)).value;
        let description: any = (<HTMLInputElement>document.getElementById(htmlIdDesc)).value;

        arrProduct[x] = this.otherExpencesDetailList[x].product_id;
        arrQty[x] = parseInt(qtyReceive);
        arrUnitType[x] = 'PCS';
        arrDesc[x] = description;
      }

      otherExpencesClosedBundle.otherExpencesDetail.product_id = arrProduct;
      otherExpencesClosedBundle.otherExpencesDetail.qty = arrQty;
      otherExpencesClosedBundle.otherExpencesDetail.unit = arrUnitType;
      otherExpencesClosedBundle.otherExpencesDetail.description = arrDesc;

      if(action === 'status') {
        let timezoneName = this.userProfile.counter_detail.timezone !== undefined ? this.timezone[parseInt(this.userProfile.counter_detail.timezone)] : this.timezone[0];
        let convertTime = this.utilService.convertDateWithMoment(new Date(), timezoneName);
        let convertDate = this.utilService.convertDate(new Date());
        let transDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;
        let transTime = convertTime.hours + ':' + convertTime.minutes + ':' + convertTime.seconds;

        otherExpencesClosedBundle.approvalHistory.transaction_id = this.otherExpencesId;
        otherExpencesClosedBundle.approvalHistory.username = this.userProfile.username;
        otherExpencesClosedBundle.approvalHistory.status = '1';
        otherExpencesClosedBundle.approvalHistory.trans_date = transDate + ' ' + transTime;

        let arrWarehouseId: any[] = [];
        let arrTransactionId: any[] = [];
        let arrStockMutationTypeId: any[] = [];
        let arrProductId: any[] = [];
        let arrQty: any[] = [];
        let arrValue: any[] = [];
        let arrStockMove: any[] = [];
        let arrTransDate: any[] = [];
        let arrYear: any[] = [];
        let arrMonth: any[] = [];

        if(this.otherExpencesDetailList.length > 0) {
          for(let x = 0; x < this.otherExpencesDetailList.length; x++) {
            let htmlIdQtyReceive: any = 'qtyReceive_' + x;
            let qtyReceive: any = (<HTMLInputElement>document.getElementById(htmlIdQtyReceive)).value;

            arrYear[x] = convertDate.years;
            arrMonth[x] = convertDate.months;
            arrWarehouseId[x] = formOtherExpences.warehouseId;
            arrTransactionId[x] = this.otherExpencesId;
            arrStockMutationTypeId[x] = 'DL'; // DL = PENERIMAAN LAIN
            arrStockMove[x] = 'I';
            arrProductId[x] = this.otherExpencesDetailList[x].product_id;
            arrQty[x] = parseInt(qtyReceive);
            arrValue[x] = 0;
            arrTransDate[x] = transDate;
          }

          otherExpencesClosedBundle.stockMutation.warehouse_id = arrWarehouseId;
          otherExpencesClosedBundle.stockMutation.transaction_id = arrTransactionId;
          otherExpencesClosedBundle.stockMutation.stock_mutation_type_id = arrStockMutationTypeId;
          otherExpencesClosedBundle.stockMutation.product_id = arrProductId;
          otherExpencesClosedBundle.stockMutation.qty = arrQty;
          otherExpencesClosedBundle.stockMutation.value = arrValue;
          otherExpencesClosedBundle.stockMutation.stock_move = arrStockMove;
          otherExpencesClosedBundle.stockMutation.trans_date = arrTransDate;
        }
      }

      this.expencesService.updateOtherExpencesBundle(this.otherExpencesId, otherExpencesClosedBundle)
      .subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/other-expences']);;
          }
        }
      ]
    });

    await alert.present();
  }

  addDetail() {
    let length = this.otherExpencesDetailList.length;
    this.otherExpencesDetailList.push({
      'id' : length + 1,
      'product_id' : null,
      'product_name' : null,
      'unit': null
    });
  }

  async findProduct(index: any) {
    const modal = await this.modalController.create({
      component: FindProductComponent,
      componentProps: {
        'productList': this.productList
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data = modelData.data;
      if(data) {
        let findProduct = this.otherExpencesDetailList.indexOf(data);
        if(findProduct === -1) {
          this.otherExpencesDetailList[index].product_id = data.id;
          this.otherExpencesDetailList[index].product_name = data.product_name;
        }
      }
    });

    return await modal.present();
  }

  deleteProduct(index: any) {
    this.otherExpencesDetailList.splice(index, 1);
  }

  /*updateHeader(action: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherExpences = this.formOtherExpencesEdit.value;
      let warehouseId = formOtherExpences.warehouseId;
      let purposeId = formOtherExpences.purposeId;
      let desc = formOtherExpences.description;

      let docDateConvert = this.utilService.convertDate(formOtherExpences.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      const otherExpences = new OtherExpences();
      otherExpences.warehouse_id = warehouseId;
      otherExpences.purpose_id = purposeId;
      otherExpences.doc_date = documentDate;
      otherExpences.desc = desc;
      otherExpences.updated_by = this.userProfile.username;

      if(action === 'data') { otherExpences.status = '0'; }
      if(action === 'status') { otherExpences.status = '1'; }

      this.expencesService.updateOtherExepences(this.otherExpencesId, otherExpences).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          if(action === 'data') { this.saveOtherExpencesDetail(this.otherExpencesId); }
          if(action === 'status') { this.saveApprovalHistory(otherExpences.status); }
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  saveOtherExpencesDetail(otherExpencesId: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherExpencesDetail = this.formOtherExpencesEdit.value;
      let arrOtherExpencesId: any = [];
      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < formOtherExpencesDetail.detail.length; x++) {
        arrOtherExpencesId[x] = otherExpencesId;
        arrProduct[x] = formOtherExpencesDetail.detail[x].product.id;
        arrQty[x] = parseInt(formOtherExpencesDetail.detail[x].qty);
        arrUnitType[x] = 'PCS';
        arrDesc[x] = formOtherExpencesDetail.detail[x].desc;
      }

      const expencesDetail = new OtherExpencesDetail();
      expencesDetail.other_expences_id = arrOtherExpencesId;
      expencesDetail.product_id = arrProduct;
      expencesDetail.qty = arrQty;
      expencesDetail.unit = arrUnitType;
      expencesDetail.description = arrDesc;

      this.expencesService.updateOtherExepencesDetail(this.otherExpencesId, expencesDetail).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  saveApprovalHistory(status: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let convertTime = this.utilService.convertDateWithMoment(new Date(), 'Asia/Jakarta');
      let convertDate = this.utilService.convertDate(new Date());
      let transDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;
      let transTime = convertTime.hours + ':' + convertTime.minutes + ':' + convertTime.seconds;

      const approvalHistory = new ApprovalHistory();
      approvalHistory.transaction_id = this.otherExpencesId;
      approvalHistory.username = this.userProfile.username;
      approvalHistory.status = status;
      approvalHistory.trans_date = transDate + ' ' + transTime;

      this.expencesService.addApprovalHistory(approvalHistory).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.saveMutationStock();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  saveMutationStock() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherExpences = this.formOtherExpencesEdit.value;
      const formOtherExpencesDetail = this.formOtherExpencesEdit.value;

      let newDate = new Date();
      let convertDate = this.utilService.convertDate(newDate);
      let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

      let arrWarehouseId: any[] = [];
      let arrTransactionId: any[] = [];
      let arrStockMutationTypeId: any[] = [];
      let arrProductId: any[] = [];
      let arrQty: any[] = [];
      let arrValue: any[] = [];
      let arrStockMove: any[] = [];
      let arrTransDate: any[] = [];
      let arrYear: any[] = [];
      let arrMonth: any[] = [];

      if(formOtherExpencesDetail.detail.length > 0) {
        for(let x = 0; x < formOtherExpencesDetail.detail.length; x++) {
          arrYear[x] = convertDate.years;
          arrMonth[x] = convertDate.months;
          arrWarehouseId[x] = formOtherExpences.warehouseId;
          arrTransactionId[x] = this.otherExpencesId;
          arrStockMutationTypeId[x] = 'PL'; // PL = PENGELUARAN LAIN
          arrStockMove[x] = 'O';
          arrProductId[x] = formOtherExpencesDetail.detail[x].product.id;
          arrQty[x] = parseInt(formOtherExpencesDetail.detail[x].qty);
          arrValue[x] = 0;
          arrTransDate[x] = todayDate;
        }

        const stockMutationData = new StockMutation();
        stockMutationData.warehouse_id = arrWarehouseId;
        stockMutationData.transaction_id = arrTransactionId;
        stockMutationData.stock_mutation_type_id = arrStockMutationTypeId;
        stockMutationData.product_id = arrProductId;
        stockMutationData.qty = arrQty;
        stockMutationData.value = arrValue;
        stockMutationData.stock_move = arrStockMove;
        stockMutationData.trans_date = arrTransDate;

        this.expencesService.addStockMutation(stockMutationData).subscribe((response) => {
          if(response.status.code === 201) {

            let options = {
              "token": this.token,
              "year": arrYear,
              "month": arrMonth,
              "warehouse_id": arrWarehouseId,
              "product_id": arrProductId,
              "mutation_type": arrStockMove,
              "qty": arrQty,
              "value": arrValue
            };

            this.expencesService.manageStock(options).subscribe((response) => {
              this.utilService.loadingDismiss();
              if(response.status.code === 201) {
                this.showConfirmUpdate();
              } else {
                this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
              }
            }, () => {
              this.utilService.loadingDismiss();
              this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
            });
          } else {
            this.utilService.loadingDismiss();
            this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      } else {
        this.utilService.loadingDismiss();
        this.showConfirmUpdate();
      }
    });
  }
  
  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  getCounterList() {
    let selectQuery = 'SELECT * FROM mst_counters';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.counterData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let dataCounter = {
                'id': result.rows[x].id,
                'counter_name': result.rows[x].counter_name
              };
              this.counterData.push(dataCounter);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getWarehouseList() {
    let selectQuery = 'SELECT * FROM mst_warehouses';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.warehouseData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let dataWarehouse = {
                'id': result.rows[x].id,
                'warehouse_name': result.rows[x].warehouse_name
              };
              this.warehouseData.push(dataWarehouse);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getPurposeList() {
    let selectQuery = 'SELECT * FROM mst_purposes';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.purposeData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let purposeData = {
                'id': result.rows[x].id,
                'purpose_name': result.rows[x].purpose_name
              };
              this.purposeData.push(purposeData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  addDetail() {
    const detail = this.fb.group({
      product: [null],
      qty: [0]
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formOtherExpencesEdit.get('detail'));
  }

  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    event.component.endSearch();
  }

  deleteDetail(i: any) {
    this.getDetailArray.removeAt(i);
  }
  */
}
