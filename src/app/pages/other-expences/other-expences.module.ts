import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtherExpencesPageRoutingModule } from './other-expences-routing.module';

import { OtherExpencesPage } from './other-expences.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OtherExpencesPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [OtherExpencesPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class OtherExpencesPageModule {}
