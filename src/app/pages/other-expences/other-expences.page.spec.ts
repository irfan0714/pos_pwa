import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtherExpencesPage } from './other-expences.page';

describe('OtherExpencesPage', () => {
  let component: OtherExpencesPage;
  let fixture: ComponentFixture<OtherExpencesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherExpencesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtherExpencesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
