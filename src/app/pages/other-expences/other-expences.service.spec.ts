import { TestBed } from '@angular/core/testing';

import { OtherExpencesService } from './other-expences.service';

describe('OtherExpencesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OtherExpencesService = TestBed.get(OtherExpencesService);
    expect(service).toBeTruthy();
  });
});
