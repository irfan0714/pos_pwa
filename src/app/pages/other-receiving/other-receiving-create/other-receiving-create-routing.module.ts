import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtherReceivingCreatePage } from './other-receiving-create.page';

const routes: Routes = [
  {
    path: '',
    component: OtherReceivingCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherReceivingCreatePageRoutingModule {}
