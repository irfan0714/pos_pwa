import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtherReceivingCreatePageRoutingModule } from './other-receiving-create-routing.module';

import { OtherReceivingCreatePage } from './other-receiving-create.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    OtherReceivingCreatePageRoutingModule
  ],
  declarations: [OtherReceivingCreatePage]
})
export class OtherReceivingCreatePageModule {}
