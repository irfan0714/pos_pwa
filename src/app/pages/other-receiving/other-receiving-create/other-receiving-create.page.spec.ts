import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtherReceivingCreatePage } from './other-receiving-create.page';

describe('OtherReceivingCreatePage', () => {
  let component: OtherReceivingCreatePage;
  let fixture: ComponentFixture<OtherReceivingCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherReceivingCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtherReceivingCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
