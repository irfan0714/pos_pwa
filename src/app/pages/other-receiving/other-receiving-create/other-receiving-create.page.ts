import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController, ModalController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { OtherReceivingService } from '../other-receiving.service';
import { OtherReceivingBundle } from '../../../models/other-receiving-bundle.model';
import { FindProductComponent } from '../../../component/find-product/find-product.component';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-other-receiving-create',
  templateUrl: './other-receiving-create.page.html',
  styleUrls: ['./other-receiving-create.page.scss'],
})
export class OtherReceivingCreatePage implements OnInit {

  token: any;
  productSearch: Product[];
  productList: Product[];
  userProfile: UserProfile = new UserProfile();
  counterData: any[] = [];
  warehouseData: any[] = [];
  purposeData: any[] = [];
  otherReceivingDetailList: any[] = [];

  formOtherReceivingCreate: FormGroup;
  otherReceivingId: any;

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private userData: UserData,
    private modalController: ModalController,
    private receivingService: OtherReceivingService
  ) { }

  ngOnInit() {
    this.buildFormOtherReceiving();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getOtherReceivingForCreate();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getOtherReceivingForCreate() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let options = {
        "token": this.token,
        "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
      };

      this.receivingService.getOtherReceivingforCreate(options).subscribe((response) => {
        this.utilService.loadingDismiss();
        if (response.status.code === 200) {
          this.productList = response.results.product_data;
          this.counterData = response.results.counter_data;
          this.warehouseData = response.results.warehouse_data;
          this.purposeData = response.results.purpose_data;

          this.buildFormOtherReceiving();
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  buildFormOtherReceiving() {
    let newDate = new Date();
    let convertDate = this.utilService.convertDate(newDate);
    let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

    this.formOtherReceivingCreate = this.fb.group({
      counterId: [this.counterData.length > 0 ? parseInt(this.counterData[0].id) : null, Validators.required],
      warehouseId: [this.warehouseData.length > 0 ? parseInt(this.warehouseData[0].id) : null, Validators.required],
      purposeId: [this.purposeData.length > 0 ? parseInt(this.purposeData[0].id) : null, Validators.required],
      docDate: [todayDate, Validators.required],
      description: [null, Validators.required]
    });
  }

  inputBundle() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherReceiving = this.formOtherReceivingCreate.value;
      let docDateConvert = this.utilService.convertDate(formOtherReceiving.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < this.otherReceivingDetailList.length; x++) {
        let htmlIdQtyReceive: any = 'qtyReceive_' + x;
        let htmlIdDesc: any = 'description_' + x;
        let qtyReceive: any = (<HTMLInputElement>document.getElementById(htmlIdQtyReceive)).value;
        let description: any = (<HTMLInputElement>document.getElementById(htmlIdDesc)).value;

        arrProduct[x] = this.otherReceivingDetailList[x].product_id;
        arrQty[x] = parseInt(qtyReceive);
        arrUnitType[x] = 'PCS';
        arrDesc[x] = description;
      }

      const otherReceivingBundle = new OtherReceivingBundle();
      otherReceivingBundle.otherReceiving.counter_id = formOtherReceiving.counterId;
      otherReceivingBundle.otherReceiving.warehouse_id = formOtherReceiving.warehouseId;
      otherReceivingBundle.otherReceiving.purpose_id = formOtherReceiving.purposeId;
      otherReceivingBundle.otherReceiving.doc_date = documentDate;
      otherReceivingBundle.otherReceiving.desc = formOtherReceiving.description;
      otherReceivingBundle.otherReceiving.status = '0';
      otherReceivingBundle.otherReceiving.created_by = this.userProfile.username;

      otherReceivingBundle.otherReceivingDetail.product_id = arrProduct;
      otherReceivingBundle.otherReceivingDetail.qty = arrQty;
      otherReceivingBundle.otherReceivingDetail.unit = arrUnitType;
      otherReceivingBundle.otherReceivingDetail.description = arrDesc;

      this.receivingService.addOtherReceivingBundle(otherReceivingBundle)
      .subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmInput() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/other-receiving']);;
          }
        }
      ]
    });

    await alert.present();
  }

  addDetail() {
    let length = this.otherReceivingDetailList.length;
    this.otherReceivingDetailList.push({
      'id' : length + 1,
      'product_id' : null,
      'product_name' : null,
      'unit': null
    });
  }

  async findProduct(index: any) {
    const modal = await this.modalController.create({
      component: FindProductComponent,
      componentProps: {
        'productList': this.productList
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data = modelData.data;
      if(data) {
        let findProduct = this.otherReceivingDetailList.indexOf(data);
        if(findProduct === -1) {
          this.otherReceivingDetailList[index].product_id = data.id;
          this.otherReceivingDetailList[index].product_name = data.product_name;
        }
      }
    });

    return await modal.present();
  }

  deleteProduct(index: any) {
    this.otherReceivingDetailList.splice(index, 1);
  }

  /*inputHeader() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let todayDate = new Date();
      let convertDate = this.utilService.convertDate(todayDate);
      let years = convertDate.years.substr(2,2);
      let month = convertDate.months;

      const formOtherReceiving = this.formOtherReceivingCreate.value;
      let counterId = formOtherReceiving.counterId;
      let warehouseId = formOtherReceiving.warehouseId;
      let purposeId = formOtherReceiving.purposeId;
      let desc = formOtherReceiving.description;

      let docDateConvert = this.utilService.convertDate(formOtherReceiving.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      let options = {
        "token": this.token,
        "counter_id": counterId
      };
      
      this.receivingService.getOtherReceivingforCreate(options).subscribe((response) => {
        if(response.results.length > 0) {
          let latestOtherReceivingId: any = response.results[0].id;
          let splitId: any = latestOtherReceivingId.split('-');
          let lastCounter: any = splitId[0].substr(6, 3);
          let lastMonth: any = parseInt(splitId[0].substr(4, 2));
          let newCounter: any = (parseInt(lastCounter) + 1);
          if(lastMonth === parseInt(month)) {
            if(newCounter <= 9) { newCounter = '00' + newCounter.toString(); }
            if(newCounter > 9 && newCounter <= 99) { newCounter = '0' + newCounter.toString(); }
          } else {
            newCounter = '001';
          }

          this.otherReceivingId = 'DL' + years + month + newCounter + '-' + splitId[1];
        } else {
          let newCounterId: any;
          if(parseInt(counterId) <= 9) { newCounterId = '00' + counterId.toString(); }
          if(parseInt(counterId) > 9 && parseInt(counterId) <= 99) { newCounterId = '0' + counterId.toString(); }
          this.otherReceivingId = 'DL' + years + month + '001' + '-' + newCounterId;
        }

        const otherReceiving = new OtherReceiving();
        otherReceiving.id = this.otherReceivingId;
        otherReceiving.counter_id = counterId;
        otherReceiving.warehouse_id = warehouseId;
        otherReceiving.purpose_id = purposeId;
        otherReceiving.doc_date = documentDate;
        otherReceiving.desc = desc;
        otherReceiving.status = '0';
        otherReceiving.created_by = this.userProfile.username;

        this.receivingService.addOtherReceiving(otherReceiving).subscribe((response) => {
          this.utilService.loadingDismiss();
          if(response.status.code === 201) {
            this.saveOtherReceivingDetail(this.otherReceivingId);
          } else {
            this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }

  saveOtherReceivingDetail(otherReceivingId: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherReceivingDetail = this.formOtherReceivingCreate.value;
      let arrOtherReceivingId: any = [];
      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < formOtherReceivingDetail.detail.length; x++) {
        arrOtherReceivingId[x] = otherReceivingId;
        arrProduct[x] = formOtherReceivingDetail.detail[x].product.id;
        arrQty[x] = parseInt(formOtherReceivingDetail.detail[x].qty);
        arrUnitType[x] = 'PCS';
        arrDesc[x] = formOtherReceivingDetail.detail[x].desc;
      }

      const receivingDetail = new OtherReceivingDetail();
      receivingDetail.other_receiving_id = arrOtherReceivingId;
      receivingDetail.product_id = arrProduct;
      receivingDetail.qty = arrQty;
      receivingDetail.unit = arrUnitType;
      receivingDetail.description = arrDesc;

      this.receivingService.addOtherReceivingDetail(receivingDetail).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }
  
  addDetail() {
    const detail = this.fb.group({
      product: [null, Validators.required],
      qty: [0, Validators.required],
      desc: [null, Validators.required]
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formOtherReceivingCreate.get('detail'));
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    event.component.endSearch();
  }

  deleteDetail(i: any) {
    this.getDetailArray.removeAt(i);
  }
  
  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  getCounterList() {
    let selectQuery = 'SELECT * FROM mst_counters';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.counterData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let dataCounter = {
                'id': result.rows[x].id,
                'counter_name': result.rows[x].counter_name
              };
              this.counterData.push(dataCounter);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getWarehouseList() {
    let selectQuery = 'SELECT * FROM mst_warehouses';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.warehouseData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let dataWarehouse = {
                'id': result.rows[x].id,
                'warehouse_name': result.rows[x].warehouse_name
              };
              this.warehouseData.push(dataWarehouse);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getPurposeList() {
    let selectQuery = 'SELECT * FROM mst_purposes';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.purposeData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let purposeData = {
                'id': result.rows[x].id,
                'purpose_name': result.rows[x].purpose_name
              };
              this.purposeData.push(purposeData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }
  */
}
