import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtherReceivingEditPage } from './other-receiving-edit.page';

const routes: Routes = [
  {
    path: '',
    component: OtherReceivingEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherReceivingEditPageRoutingModule {}
