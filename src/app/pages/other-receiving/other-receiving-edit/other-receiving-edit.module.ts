import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtherReceivingEditPageRoutingModule } from './other-receiving-edit-routing.module';

import { OtherReceivingEditPage } from './other-receiving-edit.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    OtherReceivingEditPageRoutingModule
  ],
  declarations: [OtherReceivingEditPage]
})
export class OtherReceivingEditPageModule {}
