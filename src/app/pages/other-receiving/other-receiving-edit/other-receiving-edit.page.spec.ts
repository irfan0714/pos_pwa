import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtherReceivingEditPage } from './other-receiving-edit.page';

describe('OtherReceivingEditPage', () => {
  let component: OtherReceivingEditPage;
  let fixture: ComponentFixture<OtherReceivingEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherReceivingEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtherReceivingEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
