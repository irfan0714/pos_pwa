import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController, ModalController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { OtherReceivingService } from '../other-receiving.service';
import { OtherReceivingStatus } from '../../../enum/OtherReceivingStatus';
import { RoleAccess } from '../../../models/role-access.model';
import { OtherReceivingClosedBundle } from '../../../models/other-receiving-cl-bundle.model';
import { FindProductComponent } from '../../../component/find-product/find-product.component';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-other-receiving-edit',
  templateUrl: './other-receiving-edit.page.html',
  styleUrls: ['./other-receiving-edit.page.scss'],
})
export class OtherReceivingEditPage implements OnInit {

  token: any;
  productSearch: Product[];
  productList: Product[];
  userProfile: UserProfile = new UserProfile();
  counterData: any[] = [];
  warehouseData: any[] = [];
  purposeData: any[] = [];
  otherReceivingData: any;
  otherReceivingDetailData: any[] = [];
  statusOtherReceiving: any = 0;
  otherReceivingDetailList: any[] = [];

  formOtherReceivingEdit: FormGroup;
  otherReceivingId: any;

  receivingStatus = OtherReceivingStatus;
  receivingStatusList = Object.keys(OtherReceivingStatus).filter(
    receivingStatus => typeof this.receivingStatus[receivingStatus] === 'number'
  );

  approvalHistoryData: any[] = [];
  createdBy: any;
  createdAt: any;
  updatedBy: any;
  updatedAt: any;

  userAccess: any[] = [];
  roleAccess = new RoleAccess();
  roleName: any;

  timezone: any[] = ['Asia/Jakarta', 'Asia/Makassar', 'Asia/Jayapura'];

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private activeRoute: ActivatedRoute,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private receivingService: OtherReceivingService,
    private router: Router,
    private modalController: ModalController,
    private userDataProvider: UserData
  ) { }

  ngOnInit() {
    this.buildFormOtherReceiving();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile'),
      this.storage.get('user_menu_access')
    ])
    .then(([token, profile, access]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.userAccess = access;
        this.roleName = this.userProfile.role_detail ? this.userProfile.role_detail.role_name : null;
        this.roleAccess = this.userDataProvider.checkAccess(this.router.url, this.userAccess, this.userProfile);
        this.getData();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getAllProduct() {
    this.receivingService.getProduct({ "token": this.token }).subscribe((response) => {
      if (response.status.code === 200) {
        this.productList = response.results;
      }
    }, () => {
      this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
    });
  }

  buildFormOtherReceiving() {
    let newDate = new Date();
    let convertDate = this.utilService.convertDate(newDate);
    let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

    this.formOtherReceivingEdit = this.fb.group({
      otherReceivingId: [this.otherReceivingId],
      counterId: [null],
      warehouseId: [null],
      purposeId: [null],
      docDate: [todayDate],
      description: [null],
      status: [null]
    });
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.otherReceivingId = snapshot.otherReceivingId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        let options = {
          "token": this.token,
          "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
        };

        this.receivingService.getOtherReceivingforEdit(this.otherReceivingId, options).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.otherReceivingData = response.results.other_receiving_data;
          this.otherReceivingDetailData = response.results.other_receiving_detail_data;
          this.productList = response.results.product_data;
          this.counterData = response.results.counter_data;
          this.warehouseData = response.results.warehouse_data;
          this.purposeData = response.results.purpose_data;

          let userCreate: any = response.results.user_create_data;
          this.createdBy = userCreate ? userCreate[0].name : null;
          let userUpdate: any = response.results.user_update_data;
          this.updatedBy = userUpdate ? userUpdate[0].name : null;
          this.approvalHistoryData = response.results.approval_history_data;
          
          if(this.otherReceivingData.length > 0) {
            this.createdAt = this.otherReceivingData[0].created_at;
            this.updatedAt = this.otherReceivingData[0].updated_at;
            this.statusOtherReceiving = this.otherReceivingData[0].status;
            let statusName = this.receivingStatusList[parseInt(this.statusOtherReceiving)];

            this.formOtherReceivingEdit = this.fb.group({
              otherReceivingId: [this.otherReceivingId],
              counterId: [this.otherReceivingData[0].counter_id],
              warehouseId: [this.otherReceivingData[0].warehouse_id],
              purposeId: [this.otherReceivingData[0].purpose_id],
              docDate: [this.otherReceivingData[0].doc_date],
              description: [this.otherReceivingData[0].desc],
              status: [statusName]
            });
          }

          if(this.otherReceivingDetailData.length > 0) {
            this.otherReceivingDetailList = [];
            for(let i = 0; i < this.otherReceivingDetailData.length; i++) {
              this.otherReceivingDetailList.push({
                'id' : i + 1,
                'product_id' : this.otherReceivingDetailData[i].product_id,
                'product_name' : this.otherReceivingDetailData[i].product_name,
                'qty_receive': this.otherReceivingDetailData[i].qty,
                'desc': this.otherReceivingDetailData[i].description
              });
            }
          } else {
            this.otherReceivingDetailList = [];
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  updateBundle(action: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherReceiving = this.formOtherReceivingEdit.value;
      let docDateConvert = this.utilService.convertDate(formOtherReceiving.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      const otherReceivingClosedBundle = new OtherReceivingClosedBundle();
      otherReceivingClosedBundle.otherReceiving.id = this.otherReceivingId;
      otherReceivingClosedBundle.otherReceiving.warehouse_id = formOtherReceiving.warehouseId;
      otherReceivingClosedBundle.otherReceiving.purpose_id = formOtherReceiving.purposeId;
      otherReceivingClosedBundle.otherReceiving.doc_date = documentDate;
      otherReceivingClosedBundle.otherReceiving.desc = formOtherReceiving.description;
      otherReceivingClosedBundle.otherReceiving.status = action === 'data' ? '0' : '1';
      otherReceivingClosedBundle.otherReceiving.updated_by = this.userProfile.username;

      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < this.otherReceivingDetailList.length; x++) {
        let htmlIdQtyReceive: any = 'qtyReceive_' + x;
        let htmlIdDesc: any = 'description_' + x;
        let qtyReceive: any = (<HTMLInputElement>document.getElementById(htmlIdQtyReceive)).value;
        let description: any = (<HTMLInputElement>document.getElementById(htmlIdDesc)).value;

        arrProduct[x] = this.otherReceivingDetailList[x].product_id;
        arrQty[x] = parseInt(qtyReceive);
        arrUnitType[x] = 'PCS';
        arrDesc[x] = description;
      }

      otherReceivingClosedBundle.otherReceivingDetail.product_id = arrProduct;
      otherReceivingClosedBundle.otherReceivingDetail.qty = arrQty;
      otherReceivingClosedBundle.otherReceivingDetail.unit = arrUnitType;
      otherReceivingClosedBundle.otherReceivingDetail.description = arrDesc;

      if(action === 'status') {
        let timezoneName = this.userProfile.counter_detail.timezone !== undefined ? this.timezone[parseInt(this.userProfile.counter_detail.timezone)] : this.timezone[0];
        let convertTime = this.utilService.convertDateWithMoment(new Date(), timezoneName);
        let convertDate = this.utilService.convertDate(new Date());
        let transDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;
        let transTime = convertTime.hours + ':' + convertTime.minutes + ':' + convertTime.seconds;

        otherReceivingClosedBundle.approvalHistory.transaction_id = this.otherReceivingId;
        otherReceivingClosedBundle.approvalHistory.username = this.userProfile.username;
        otherReceivingClosedBundle.approvalHistory.status = '1';
        otherReceivingClosedBundle.approvalHistory.trans_date = transDate + ' ' + transTime;

        let arrWarehouseId: any[] = [];
        let arrTransactionId: any[] = [];
        let arrStockMutationTypeId: any[] = [];
        let arrProductId: any[] = [];
        let arrQty: any[] = [];
        let arrValue: any[] = [];
        let arrStockMove: any[] = [];
        let arrTransDate: any[] = [];
        let arrYear: any[] = [];
        let arrMonth: any[] = [];

        if(this.otherReceivingDetailList.length > 0) {
          for(let x = 0; x < this.otherReceivingDetailList.length; x++) {
            let htmlIdQtyReceive: any = 'qtyReceive_' + x;
            let qtyReceive: any = (<HTMLInputElement>document.getElementById(htmlIdQtyReceive)).value;

            arrYear[x] = convertDate.years;
            arrMonth[x] = convertDate.months;
            arrWarehouseId[x] = formOtherReceiving.warehouseId;
            arrTransactionId[x] = this.otherReceivingId;
            arrStockMutationTypeId[x] = 'DL'; // DL = PENERIMAAN LAIN
            arrStockMove[x] = 'I';
            arrProductId[x] = this.otherReceivingDetailList[x].product_id;
            arrQty[x] = parseInt(qtyReceive);
            arrValue[x] = 0;
            arrTransDate[x] = transDate;
          }

          otherReceivingClosedBundle.stockMutation.warehouse_id = arrWarehouseId;
          otherReceivingClosedBundle.stockMutation.transaction_id = arrTransactionId;
          otherReceivingClosedBundle.stockMutation.stock_mutation_type_id = arrStockMutationTypeId;
          otherReceivingClosedBundle.stockMutation.product_id = arrProductId;
          otherReceivingClosedBundle.stockMutation.qty = arrQty;
          otherReceivingClosedBundle.stockMutation.value = arrValue;
          otherReceivingClosedBundle.stockMutation.stock_move = arrStockMove;
          otherReceivingClosedBundle.stockMutation.trans_date = arrTransDate;
        }
      }

      this.receivingService.updateOtherReceivingBundle(this.otherReceivingId, otherReceivingClosedBundle)
      .subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/other-receiving']);;
          }
        }
      ]
    });

    await alert.present();
  }

  addDetail() {
    let length = this.otherReceivingDetailList.length;
    this.otherReceivingDetailList.push({
      'id' : length + 1,
      'product_id' : null,
      'product_name' : null,
      'unit': null
    });
  }

  async findProduct(index: any) {
    const modal = await this.modalController.create({
      component: FindProductComponent,
      componentProps: {
        'productList': this.productList
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data = modelData.data;
      if(data) {
        let findProduct = this.otherReceivingDetailList.indexOf(data);
        if(findProduct === -1) {
          this.otherReceivingDetailList[index].product_id = data.id;
          this.otherReceivingDetailList[index].product_name = data.product_name;
        }
      }
    });

    return await modal.present();
  }

  deleteProduct(index: any) {
    this.otherReceivingDetailList.splice(index, 1);
  }

  /*addDetail() {
    const detail = this.fb.group({
      product: [null],
      qty: [0]
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formOtherReceivingEdit.get('detail'));
  }

  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    event.component.endSearch();
  }

  deleteDetail(i: any) {
    this.getDetailArray.removeAt(i);
  }
  
  updateHeader(action: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherReceiving = this.formOtherReceivingEdit.value;
      let warehouseId = formOtherReceiving.warehouseId;
      let purposeId = formOtherReceiving.purposeId;
      let desc = formOtherReceiving.description;

      let docDateConvert = this.utilService.convertDate(formOtherReceiving.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      const otherReceiving = new OtherReceiving();
      otherReceiving.warehouse_id = warehouseId;
      otherReceiving.purpose_id = purposeId;
      otherReceiving.doc_date = documentDate;
      otherReceiving.desc = desc;
      otherReceiving.updated_by = this.userProfile.username;

      if(action === 'data') { otherReceiving.status = '0'; }
      if(action === 'status') { otherReceiving.status = '1'; }

      this.receivingService.updateOtherReceiving(this.otherReceivingId, otherReceiving).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          if(action === 'data') { this.saveOtherReceivingDetail(this.otherReceivingId); }
          if(action === 'status') { this.saveApprovalHistory(otherReceiving.status); }
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  saveOtherReceivingDetail(otherReceivingId: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherReceivingDetail = this.formOtherReceivingEdit.value;
      let arrOtherReceivingId: any = [];
      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < formOtherReceivingDetail.detail.length; x++) {
        arrOtherReceivingId[x] = otherReceivingId;
        arrProduct[x] = formOtherReceivingDetail.detail[x].product.id;
        arrQty[x] = parseInt(formOtherReceivingDetail.detail[x].qty);
        arrUnitType[x] = 'PCS';
        arrDesc[x] = formOtherReceivingDetail.detail[x].desc;
      }

      const receivingDetail = new OtherReceivingDetail();
      receivingDetail.other_receiving_id = arrOtherReceivingId;
      receivingDetail.product_id = arrProduct;
      receivingDetail.qty = arrQty;
      receivingDetail.unit = arrUnitType;
      receivingDetail.description = arrDesc;

      this.receivingService.updateOtherReceivingDetail(this.otherReceivingId, receivingDetail).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  saveApprovalHistory(status: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let convertTime = this.utilService.convertDateWithMoment(new Date(), 'Asia/Jakarta');
      let convertDate = this.utilService.convertDate(new Date());
      let transDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;
      let transTime = convertTime.hours + ':' + convertTime.minutes + ':' + convertTime.seconds;

      const approvalHistory = new ApprovalHistory();
      approvalHistory.transaction_id = this.otherReceivingId;
      approvalHistory.username = this.userProfile.username;
      approvalHistory.status = status;
      approvalHistory.trans_date = transDate + ' ' + transTime;

      this.receivingService.addApprovalHistory(approvalHistory).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.saveMutationStock();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  saveMutationStock() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formOtherReceiving = this.formOtherReceivingEdit.value;
      const formOtherReceivingDetail = this.formOtherReceivingEdit.value;

      let newDate = new Date();
      let convertDate = this.utilService.convertDate(newDate);
      let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

      let arrWarehouseId: any[] = [];
      let arrTransactionId: any[] = [];
      let arrStockMutationTypeId: any[] = [];
      let arrProductId: any[] = [];
      let arrQty: any[] = [];
      let arrValue: any[] = [];
      let arrStockMove: any[] = [];
      let arrTransDate: any[] = [];
      let arrYear: any[] = [];
      let arrMonth: any[] = [];

      if(formOtherReceivingDetail.detail.length > 0) {
        for(let x = 0; x < formOtherReceivingDetail.detail.length; x++) {
          arrYear[x] = convertDate.years;
          arrMonth[x] = convertDate.months;
          arrWarehouseId[x] = formOtherReceiving.warehouseId;
          arrTransactionId[x] = this.otherReceivingId;
          arrStockMutationTypeId[x] = 'DL'; // DL = PENERIMAAN LAIN
          arrStockMove[x] = 'I';
          arrProductId[x] = formOtherReceivingDetail.detail[x].product.id;
          arrQty[x] = parseInt(formOtherReceivingDetail.detail[x].qty);
          arrValue[x] = 0;
          arrTransDate[x] = todayDate;
        }

        const stockMutationData = new StockMutation();
        stockMutationData.warehouse_id = arrWarehouseId;
        stockMutationData.transaction_id = arrTransactionId;
        stockMutationData.stock_mutation_type_id = arrStockMutationTypeId;
        stockMutationData.product_id = arrProductId;
        stockMutationData.qty = arrQty;
        stockMutationData.value = arrValue;
        stockMutationData.stock_move = arrStockMove;
        stockMutationData.trans_date = arrTransDate;

        this.receivingService.addStockMutation(stockMutationData).subscribe((response) => {
          if(response.status.code === 201) {

            let options = {
              "token": this.token,
              "year": arrYear,
              "month": arrMonth,
              "warehouse_id": arrWarehouseId,
              "product_id": arrProductId,
              "mutation_type": arrStockMove,
              "qty": arrQty,
              "value": arrValue
            };

            this.receivingService.manageStock(options).subscribe((response) => {
              this.utilService.loadingDismiss();
              if(response.status.code === 201) {
                this.showConfirmUpdate();
              } else {
                this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
              }
            }, () => {
              this.utilService.loadingDismiss();
              this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
            });
          } else {
            this.utilService.loadingDismiss();
            this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      } else {
        this.utilService.loadingDismiss();
        this.showConfirmUpdate();
      }
    });
  }
  
  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  getCounterList() {
    let selectQuery = 'SELECT * FROM mst_counters';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.counterData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let dataCounter = {
                'id': result.rows[x].id,
                'counter_name': result.rows[x].counter_name
              };
              this.counterData.push(dataCounter);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getWarehouseList() {
    let selectQuery = 'SELECT * FROM mst_warehouses';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.warehouseData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let dataWarehouse = {
                'id': result.rows[x].id,
                'warehouse_name': result.rows[x].warehouse_name
              };
              this.warehouseData.push(dataWarehouse);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getPurposeList() {
    let selectQuery = 'SELECT * FROM mst_purposes';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.purposeData = [];
            for(let x = 0; x < result.rows.length; x++) {
              let purposeData = {
                'id': result.rows[x].id,
                'purpose_name': result.rows[x].purpose_name
              };
              this.purposeData.push(purposeData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }
  */
}
