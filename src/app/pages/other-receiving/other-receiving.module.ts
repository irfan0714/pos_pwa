import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtherReceivingPageRoutingModule } from './other-receiving-routing.module';

import { OtherReceivingPage } from './other-receiving.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OtherReceivingPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [OtherReceivingPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class OtherReceivingPageModule {}
