import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtherReceivingPage } from './other-receiving.page';

describe('OtherReceivingPage', () => {
  let component: OtherReceivingPage;
  let fixture: ComponentFixture<OtherReceivingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherReceivingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtherReceivingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
