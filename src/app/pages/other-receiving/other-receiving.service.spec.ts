import { TestBed } from '@angular/core/testing';

import { OtherReceivingService } from './other-receiving.service';

describe('OtherReceivingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OtherReceivingService = TestBed.get(OtherReceivingService);
    expect(service).toBeTruthy();
  });
});
