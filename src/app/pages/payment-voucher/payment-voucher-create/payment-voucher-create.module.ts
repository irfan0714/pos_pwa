import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentVoucherCreatePageRoutingModule } from './payment-voucher-create-routing.module';

import { PaymentVoucherCreatePage } from './payment-voucher-create.page';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentVoucherCreatePageRoutingModule,
    ReactiveFormsModule,
    IonicSelectableModule
  ],
  declarations: [PaymentVoucherCreatePage]
})
export class PaymentVoucherCreatePageModule {}
