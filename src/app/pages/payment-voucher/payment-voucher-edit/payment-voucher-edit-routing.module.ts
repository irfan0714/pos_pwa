import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentVoucherEditPage } from './payment-voucher-edit.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentVoucherEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentVoucherEditPageRoutingModule {}
