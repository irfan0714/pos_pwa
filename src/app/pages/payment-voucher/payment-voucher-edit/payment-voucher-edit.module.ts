import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentVoucherEditPageRoutingModule } from './payment-voucher-edit-routing.module';

import { PaymentVoucherEditPage } from './payment-voucher-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentVoucherEditPageRoutingModule
  ],
  declarations: [PaymentVoucherEditPage]
})
export class PaymentVoucherEditPageModule {}
