import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentVoucherPageRoutingModule } from './payment-voucher-routing.module';

import { PaymentVoucherPage } from './payment-voucher.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentVoucherPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [PaymentVoucherPage]
})
export class PaymentVoucherPageModule {}
