import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaymentVoucherPage } from './payment-voucher.page';

describe('PaymentVoucherPage', () => {
  let component: PaymentVoucherPage;
  let fixture: ComponentFixture<PaymentVoucherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentVoucherPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaymentVoucherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
