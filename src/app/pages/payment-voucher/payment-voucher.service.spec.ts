import { TestBed } from '@angular/core/testing';

import { PaymentVoucherService } from './payment-voucher.service';

describe('PaymentVoucherService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentVoucherService = TestBed.get(PaymentVoucherService);
    expect(service).toBeTruthy();
  });
});
