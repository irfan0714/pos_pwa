import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { IonicModule } from '@ionic/angular';

import { PettyCashCategoryCreatePageRoutingModule } from './petty-cash-category-create-routing.module';

import { PettyCashCategoryCreatePage } from './petty-cash-category-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PettyCashCategoryCreatePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PettyCashCategoryCreatePage]
})
export class PettyCashCategoryCreatePageModule {}
