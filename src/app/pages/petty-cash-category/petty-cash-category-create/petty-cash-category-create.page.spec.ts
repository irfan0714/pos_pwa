import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PettyCashCategoryCreatePage } from './petty-cash-category-create.page';

describe('PettyCashCategoryCreatePage', () => {
  let component: PettyCashCategoryCreatePage;
  let fixture: ComponentFixture<PettyCashCategoryCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PettyCashCategoryCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PettyCashCategoryCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
