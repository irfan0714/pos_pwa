import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PettyCashCategoryEditPage } from './petty-cash-category-edit.page';

const routes: Routes = [
  {
    path: '',
    component: PettyCashCategoryEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PettyCashCategoryEditPageRoutingModule {}
