import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PettyCashCategoryEditPageRoutingModule } from './petty-cash-category-edit-routing.module';

import { PettyCashCategoryEditPage } from './petty-cash-category-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PettyCashCategoryEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PettyCashCategoryEditPage]
})
export class PettyCashCategoryEditPageModule {}
