import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PettyCashCategoryEditPage } from './petty-cash-category-edit.page';

describe('PettyCashCategoryEditPage', () => {
  let component: PettyCashCategoryEditPage;
  let fixture: ComponentFixture<PettyCashCategoryEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PettyCashCategoryEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PettyCashCategoryEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
