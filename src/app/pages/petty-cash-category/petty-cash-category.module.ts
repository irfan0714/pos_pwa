import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PettyCashCategoryPageRoutingModule } from './petty-cash-category-routing.module';

import { PettyCashCategoryPage } from './petty-cash-category.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PettyCashCategoryPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [PettyCashCategoryPage]
})
export class PettyCashCategoryPageModule {}
