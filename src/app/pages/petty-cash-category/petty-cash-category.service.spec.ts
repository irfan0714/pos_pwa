import { TestBed } from '@angular/core/testing';

import { PettyCashCategoryService } from './petty-cash-category.service';

describe('PettyCashCategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PettyCashCategoryService = TestBed.get(PettyCashCategoryService);
    expect(service).toBeTruthy();
  });
});
