import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PettyCashCreatePage } from './petty-cash-create.page';

const routes: Routes = [
  {
    path: '',
    component: PettyCashCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PettyCashCreatePageRoutingModule {}
