import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PettyCashCreatePage } from './petty-cash-create.page';

describe('PettyCashCreatePage', () => {
  let component: PettyCashCreatePage;
  let fixture: ComponentFixture<PettyCashCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PettyCashCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PettyCashCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
