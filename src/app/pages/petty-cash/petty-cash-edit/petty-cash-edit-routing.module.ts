import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PettyCashEditPage } from './petty-cash-edit.page';

const routes: Routes = [
  {
    path: '',
    component: PettyCashEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PettyCashEditPageRoutingModule {}
