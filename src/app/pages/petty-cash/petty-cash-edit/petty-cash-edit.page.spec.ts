import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PettyCashEditPage } from './petty-cash-edit.page';

describe('PettyCashEditPage', () => {
  let component: PettyCashEditPage;
  let fixture: ComponentFixture<PettyCashEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PettyCashEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PettyCashEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
