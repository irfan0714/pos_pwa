import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PettyCashPage } from './petty-cash.page';

const routes: Routes = [
  {
    path: '',
    component: PettyCashPage
  },
  {
    path: 'petty-cash-create',
    loadChildren: () => import('./petty-cash-create/petty-cash-create.module').then( m => m.PettyCashCreatePageModule)
  },
  {
    path: 'petty-cash-edit',
    loadChildren: () => import('./petty-cash-edit/petty-cash-edit.module').then( m => m.PettyCashEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PettyCashPageRoutingModule {}
