import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PettyCashPageRoutingModule } from './petty-cash-routing.module';

import { PettyCashPage } from './petty-cash.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PettyCashPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [PettyCashPage]
})
export class PettyCashPageModule {}
