import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PosResellerPage } from './pos-reseller.page';

const routes: Routes = [
  {
    path: '',
    component: PosResellerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PosResellerPageRoutingModule {}
