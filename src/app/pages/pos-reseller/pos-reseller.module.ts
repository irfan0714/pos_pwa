import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PosResellerPageRoutingModule } from './pos-reseller-routing.module';

import { PosResellerPage } from './pos-reseller.page';
import { AutoCompleteModule } from 'ionic4-auto-complete';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PosResellerPageRoutingModule,
    AutoCompleteModule
  ],
  declarations: [PosResellerPage]
})
export class PosResellerPageModule {}
