import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PosResellerPage } from './pos-reseller.page';

describe('PosResellerPage', () => {
  let component: PosResellerPage;
  let fixture: ComponentFixture<PosResellerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosResellerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PosResellerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
