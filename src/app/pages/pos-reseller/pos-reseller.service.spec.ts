import { TestBed } from '@angular/core/testing';

import { PosResellerService } from './pos-reseller.service';

describe('PosResellerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PosResellerService = TestBed.get(PosResellerService);
    expect(service).toBeTruthy();
  });
});
