import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PriceGroupCreatePage } from './price-group-create.page';

const routes: Routes = [
  {
    path: '',
    component: PriceGroupCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PriceGroupCreatePageRoutingModule {}
