import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PriceGroupCreatePage } from './price-group-create.page';

describe('PriceGroupCreatePage', () => {
  let component: PriceGroupCreatePage;
  let fixture: ComponentFixture<PriceGroupCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceGroupCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PriceGroupCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
