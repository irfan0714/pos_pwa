import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PriceGroupEditPage } from './price-group-edit.page';

const routes: Routes = [
  {
    path: '',
    component: PriceGroupEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PriceGroupEditPageRoutingModule {}
