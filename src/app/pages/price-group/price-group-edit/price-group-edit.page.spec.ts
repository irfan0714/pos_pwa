import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PriceGroupEditPage } from './price-group-edit.page';

describe('PriceGroupEditPage', () => {
  let component: PriceGroupEditPage;
  let fixture: ComponentFixture<PriceGroupEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceGroupEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PriceGroupEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
