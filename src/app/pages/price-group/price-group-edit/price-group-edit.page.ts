import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController, ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { PriceGroupService } from '../price-group.service';
import { MstPriceGroupBundle } from '../../../models/mst-price-group-bundle.model';
import { PriceGroupComponent } from '../../../component/price-group/price-group.component';

@Component({
  selector: 'app-price-group-edit',
  templateUrl: './price-group-edit.page.html',
  styleUrls: ['./price-group-edit.page.scss'],
})
export class PriceGroupEditPage implements OnInit {

  token: any;
  userProfile: UserProfile = new UserProfile();
  priceGroupId: any;
  priceGroupDetailData: any[];
  rows: any[];

  formPriceGroupEdit: FormGroup;

  priceGroupData: any;

  createdBy: any;
  createdAt: any;
  updatedBy: any;
  updatedAt: any;
  
  statusPriceGroup: any[] = ['Not Active', 'Active'];
  priceGroupTypeList: any[] = [{id: 'RG', type_name: 'REGULER'}, {id: 'RS', type_name: 'RESELLER'}];

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private utilService: UtilService,
    private alertController: AlertController,
    private modalController: ModalController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private userData: UserData,
    private priceGroupService: PriceGroupService
  ) { }

  ngOnInit() {
    this.buildFormPriceGroupEdit();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getPriceGroupforEdit();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  buildFormPriceGroupEdit() {
    this.formPriceGroupEdit = this.fb.group({
      counterName: [null],
      priceGroupType: [null],
      description: [null],
      status: [null],
      keywordSearch: [null]
    });
  }

  getPriceGroupforEdit() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.priceGroupId = snapshot.priceGroupId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.priceGroupService.getPriceGroupforEdit(this.priceGroupId, { "token": this.token }).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.priceGroupData = response.results.price_group_data;
          this.rows = response.results.price_group_detail_data;
          this.priceGroupDetailData = response.results.price_group_detail_data;
          let userCreate: any = response.results.user_create_data;
          this.createdBy = userCreate ? userCreate[0].name : null;
          let userUpdate: any = response.results.user_update_data;
          this.updatedBy = userUpdate ? userUpdate[0].name : null;
          if(this.priceGroupData.length > 0) {
            this.createdAt = this.priceGroupData[0].created_at;
            this.updatedAt = this.priceGroupData[0].updated_at;

            this.formPriceGroupEdit = this.fb.group({
              counterName: [this.priceGroupData[0].counter_name],
              priceGroupType: [this.priceGroupData[0].price_group_types, Validators.required],
              description: [this.priceGroupData[0].description, Validators.required],
              status: [parseInt(this.priceGroupData[0].active)],
              keywordSearch: [null]
            });
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  searchProduct() {
    const formPriceGroup = this.formPriceGroupEdit.value;
    this.rows = this.priceGroupDetailData.filter(item => {
      return item.product_name.toLowerCase().indexOf(formPriceGroup.keywordSearch) !== -1 ||
        item.product_id.toString().toLowerCase().indexOf(formPriceGroup.keywordSearch) !== -1;
    });
  }

  async changePriceProduct(productData: any) {
    const modal = await this.modalController.create({
      component: PriceGroupComponent,
      componentProps: {
        'productId': productData.product_id,
        'productName': productData.product_name,
        'productPrice': productData.price,
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data = modelData.data;
      let checkProduct: any[] = this.priceGroupDetailData.find(x => x.product_id === data[0].product_id);
      if (checkProduct !== undefined) {
        let index: number = this.priceGroupDetailData.indexOf(checkProduct);
        this.priceGroupDetailData[index]['price'] = data[0].price;
        this.rows[this.rows.indexOf(this.rows.find(x => x.product_id === data[0].product_id))]['price'] = data[0].price;
      }
    });

    return await modal.present();
  }

  update() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formPriceGroup = this.formPriceGroupEdit.value;
      let arrProduct: any = [];
      let arrPrice: any = [];

      for(let x = 0; x < this.priceGroupDetailData.length; x++) {
        arrProduct[x] = this.priceGroupDetailData[x].product_id;
        arrPrice[x] = parseInt(this.priceGroupDetailData[x].price);
      }

      const mstPriceGroupBundle = new MstPriceGroupBundle();
      mstPriceGroupBundle.mstPriceGroup.price_group_types = formPriceGroup.priceGroupType;
      mstPriceGroupBundle.mstPriceGroup.description = formPriceGroup.description;
      mstPriceGroupBundle.mstPriceGroup.active = formPriceGroup.status;
      mstPriceGroupBundle.mstPriceGroup.updated_by = this.userProfile.username;

      mstPriceGroupBundle.mstPriceGroupDetail.product_id = arrProduct;
      mstPriceGroupBundle.mstPriceGroupDetail.price = arrPrice;

      this.priceGroupService.updatePriceGroup(this.priceGroupId, mstPriceGroupBundle).subscribe((response: any) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/price-group']);;
          }
        }
      ]
    });

    await alert.present();
  }

}
