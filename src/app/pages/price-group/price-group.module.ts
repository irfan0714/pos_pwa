import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PriceGroupPageRoutingModule } from './price-group-routing.module';

import { PriceGroupPage } from './price-group.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PriceGroupPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [PriceGroupPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PriceGroupPageModule {}
