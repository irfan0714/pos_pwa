import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PriceGroupPage } from './price-group.page';

describe('PriceGroupPage', () => {
  let component: PriceGroupPage;
  let fixture: ComponentFixture<PriceGroupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceGroupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PriceGroupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
