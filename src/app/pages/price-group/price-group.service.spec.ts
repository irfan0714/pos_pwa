import { TestBed } from '@angular/core/testing';

import { PriceGroupService } from './price-group.service';

describe('PriceGroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PriceGroupService = TestBed.get(PriceGroupService);
    expect(service).toBeTruthy();
  });
});
