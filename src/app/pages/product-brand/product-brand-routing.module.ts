import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductBrandPage } from './product-brand.page';

const routes: Routes = [
  {
    path: '',
    component: ProductBrandPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductBrandPageRoutingModule {}
