import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductBrandPage } from './product-brand.page';

describe('ProductBrandPage', () => {
  let component: ProductBrandPage;
  let fixture: ComponentFixture<ProductBrandPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductBrandPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductBrandPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
