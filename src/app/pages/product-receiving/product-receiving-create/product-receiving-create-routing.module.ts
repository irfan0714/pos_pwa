import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductReceivingCreatePage } from './product-receiving-create.page';

const routes: Routes = [
  {
    path: '',
    component: ProductReceivingCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductReceivingCreatePageRoutingModule {}
