import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductReceivingCreatePageRoutingModule } from './product-receiving-create-routing.module';

import { ProductReceivingCreatePage } from './product-receiving-create.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    ProductReceivingCreatePageRoutingModule
  ],
  declarations: [ProductReceivingCreatePage]
})
export class ProductReceivingCreatePageModule {}
