import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController, ModalController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { ProductReceivingComponent } from '../../../component/product-receiving/product-receiving.component';
import { ProductReceivingService } from '../product-receiving.service';
import { ProductReceivingBundle } from '../../../models/product-receiving-bundle.model';
import { FindProductComponent } from '../../../component/find-product/find-product.component';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-product-receiving-create',
  templateUrl: './product-receiving-create.page.html',
  styleUrls: ['./product-receiving-create.page.scss'],
})
export class ProductReceivingCreatePage implements OnInit {

  token: any;
  productSearch: Product[];
  productList: Product[];
  userProfile: UserProfile = new UserProfile();
  counterList: any[] = [];
  warehouseList: any[] = [];
  unitType: any[] = ['PIECES'];
  productRequestDetailList: any[] = [];

  formProductReceivingCreate: FormGroup;
  productReceivingId: any;
  productRequestDetails: any[] = [];

  productRequestNumber: any;

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private userData: UserData,
    private modalController: ModalController,
    private productReceivingService: ProductReceivingService
  ) { }

  ngOnInit() {
    this.buildFormProductReceiving();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getProductReceivingForCreate();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getProductReceivingForCreate() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let options = {
        "token": this.token,
        "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
      };

      this.productReceivingService.getProductReceivingforCreate(options)
      .subscribe((res: any) => {
        this.utilService.loadingDismiss();
        if (res.status.code === 200) {
          this.productList = res.results.product_data;
          this.counterList = res.results.counter_data;
          this.warehouseList = res.results.warehouse_data;

          this.buildFormProductReceiving();
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  buildFormProductReceiving() {
    let newDate = new Date();
    let convertDate = this.utilService.convertDate(newDate);
    let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

    this.formProductReceivingCreate = this.fb.group({
      docDate: [todayDate, Validators.required],
      productRequestId: [this.productRequestNumber ? this.productRequestNumber : null, Validators.required],
      counterId: [this.counterList.length > 0 ? parseInt(this.counterList[0].id) : null, Validators.required],
      warehouseId: [this.warehouseList.length > 0 ? parseInt(this.warehouseList[0].id) : null, Validators.required],
      courier: [null, Validators.required],
      description: [null, Validators.required]
    });
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  inputBundle() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formProductReceive = this.formProductReceivingCreate.value;

      let docDateConvert = this.utilService.convertDate(formProductReceive.docDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;

      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrQtyReceive: any = [];
      let arrDesc: any = [];

      for(let x = 0; x < this.productRequestDetailList.length; x++) {
        let htmlIdQtyReceive: any = 'qtyReceive_' + x;
        let htmlIdDescription: any = 'description_' + x;
        let qtyReceive: any = (<HTMLInputElement>document.getElementById(htmlIdQtyReceive)).value;
        let description: any = (<HTMLInputElement>document.getElementById(htmlIdDescription)).value;

        arrProduct[x] = this.productRequestDetailList[x].product_id;
        arrQty[x] = parseInt(this.productRequestDetailList[x].qty_request);
        arrUnitType[x] = 0;
        arrQtyReceive[x] = parseInt(qtyReceive);
        arrDesc[x] = description;
      }

      const productReceivingBundle = new ProductReceivingBundle();
      productReceivingBundle.productReceiving.product_request_id = formProductReceive.productRequestId;
      productReceivingBundle.productReceiving.counter_id = formProductReceive.counterId;
      productReceivingBundle.productReceiving.warehouse_id = formProductReceive.warehouseId;
      productReceivingBundle.productReceiving.doc_date = documentDate;
      productReceivingBundle.productReceiving.receipt_no = '0';
      productReceivingBundle.productReceiving.courier = formProductReceive.courier;
      productReceivingBundle.productReceiving.desc = formProductReceive.description; 
      productReceivingBundle.productReceiving.created_by = this.userProfile.username;

      productReceivingBundle.productReceivingDetail.product_id = arrProduct;
      productReceivingBundle.productReceivingDetail.qty = arrQtyReceive;
      productReceivingBundle.productReceivingDetail.unit = arrUnitType;
      productReceivingBundle.productReceivingDetail.qty_request = arrQty;
      productReceivingBundle.productReceivingDetail.descriptions = arrDesc;

      this.productReceivingService.addReceivingBundle(productReceivingBundle)
      .subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmInput() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/product-receiving']);;
          }
        }
      ]
    });

    await alert.present();
  }

  async openAdvancedFilters() {
    const formProductReceiving = this.formProductReceivingCreate.value;
    let counterId = formProductReceiving.counterId;
    let warehouseId = formProductReceiving.warehouseId;

    const modal = await this.modalController.create({
      component: ProductReceivingComponent,
      componentProps: {
        token: this.token,
        idCounter: counterId,
        idWarehouse: warehouseId
      }
    });

    modal.onDidDismiss().then((modelData) => {
      if (modelData.data !== 'null') {
        this.productRequestNumber = modelData.data;
        this.getProductRequestDetail(modelData.data);
      } else {
        // 
      }
    });

    return await modal.present();
  }

  getProductRequestDetail(productRequestId: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {

      let options = {
        "token": this.token,
        "product_request_id": productRequestId
      }

      this.productReceivingService.getProductRequestDetail(options).subscribe((response) => {
        this.utilService.loadingDismiss();
        this.productRequestDetails = response.results;
        if(this.productRequestDetails.length > 0) {
          this.productRequestDetailList = [];
          for(let i = 0; i < this.productRequestDetails.length; i++) {
            let filterProduct = this.filterProducts(this.productList, this.productRequestDetails[i].product_id);
            this.productRequestDetailList.push({
              'id' : i + 1,
              'product_id' : this.productRequestDetails[i].product_id,
              'product_name' : filterProduct[0].product_name,
              'qty_request': this.productRequestDetails[i].qty,
              'qty_receive': this.productRequestDetails[i].qty,
              'description': null,
              'unit': 'Pcs'
            });
          }
        } else {
          this.productRequestDetailList = [];
        }
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }

  addDetail() {
    let length = this.productRequestDetailList.length;
    this.productRequestDetailList.push({
      'id' : length + 1,
      'product_id' : null,
      'product_name' : null,
      'unit': null
    });
  }

  async findProduct(index: any) {
    const modal = await this.modalController.create({
      component: FindProductComponent,
      componentProps: {
        'productList': this.productList
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data = modelData.data;
      if(data) {
        let findProduct = this.productRequestDetailList.indexOf(data);
        if(findProduct === -1) {
          this.productRequestDetailList[index].product_id = data.id;
          this.productRequestDetailList[index].product_name = data.product_name;
        }
      }
    });

    return await modal.present();
  }

  deleteProduct(index: any) {
    this.productRequestDetailList.splice(index, 1);
  }

  /*addDetail() {
    const detail = this.fb.group({
      product: [null, Validators.required],
      qty: [0, Validators.required],
      typeUnit: [0],
      qtyReceive: [0, Validators.required],
      desc: [null]
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formProductReceivingCreate.get('detail'));
  }
  
  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    event.component.endSearch();
  }

  deleteDetail(i: any) {
    this.getDetailArray.removeAt(i);
  }
  
  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }
  
  getCounterList() {
    let selectQuery = 'SELECT * FROM mst_counters';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.counterList = [];
            for(let x = 0; x < result.rows.length; x++) {
              let counterData = {
                'id': result.rows[x].id,
                'counter_name': result.rows[x].counter_name
              };
              this.counterList.push(counterData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getWarehouseList() {
    let selectQuery = 'SELECT * FROM mst_warehouses';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.warehouseList = [];
            for(let x = 0; x < result.rows.length; x++) {
              let warehouseData = {
                'id': result.rows[x].id,
                'warehouse_name': result.rows[x].warehouse_name
              };
              this.warehouseList.push(warehouseData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  inputData() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let latestProductReceiveId: string;
      let lastCounter: any;
      let lastMonth: any;
      let newCounter: any;
      let todayDate = new Date();
      let convertDate = this.utilService.convertDate(todayDate);
      let years = convertDate.years.substr(2,2);
      let month = convertDate.months;

      const formProductReceive = this.formProductReceivingCreate.value;
      let counterId = formProductReceive.counterId;

      let options = {
        "token": this.token,
        "counter_id": counterId
      };

      this.productReceivingService.getLatestProductReceiveId(options).subscribe((response) => {
        if(response.results.length > 0) {
          latestProductReceiveId = response.results[0].id;
          let splitId = latestProductReceiveId.split('-');
          lastCounter = splitId[0].substr(6, 3);
          lastMonth = parseInt(splitId[0].substr(4, 2));
          newCounter = (parseInt(lastCounter) + 1);
          if(lastMonth === parseInt(month)) {
            if(newCounter <= 9) { newCounter = '00' + newCounter.toString(); }
            if(newCounter > 9 && newCounter <= 99) { newCounter = '0' + newCounter.toString(); }
          } else {
            newCounter = '001';
          }

          this.productReceivingId = 'RG' + years + month + newCounter + '-' + splitId[1];
        } else {
          let newCounterId: any;
          if(parseInt(counterId) <= 9) { newCounterId = '00' + counterId.toString(); }
          if(parseInt(counterId) > 9 && parseInt(counterId) <= 99) { newCounterId = '0' + counterId.toString(); }
          this.productReceivingId = 'RG' + years + month + '001' + '-' + newCounterId;
        }

        const productReceivingData = new ProductReceiving();
        productReceivingData.id = this.productReceivingId;
        productReceivingData.product_request_id = formProductReceive.productRequestId;
        productReceivingData.counter_id = formProductReceive.counterId;
        productReceivingData.warehouse_id = formProductReceive.warehouseId;
        productReceivingData.doc_date = formProductReceive.docDate;
        productReceivingData.receipt_no = '0';
        productReceivingData.courier = formProductReceive.courier;
        productReceivingData.desc = formProductReceive.description; 
        productReceivingData.created_by = this.userProfile.username;

        this.productReceivingService.addProductReceiving(productReceivingData).subscribe((response) => {
          this.utilService.loadingDismiss();
          if(response.status.code === 201) {
            this.saveProductReceivingDetail(this.productReceivingId);
          } else {
            this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }

  saveProductReceivingDetail(productReceivingId: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formProductReceivingDetail = this.formProductReceivingCreate.value;
      let arrProductReceivingId: any = [];
      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];
      let arrQtyReceive: any = [];

      for(let x = 0; x < formProductReceivingDetail.detail.length; x++) {
        arrProductReceivingId[x] = productReceivingId;
        arrProduct[x] = formProductReceivingDetail.detail[x].product.id;
        arrQty[x] = parseInt(formProductReceivingDetail.detail[x].qty);
        arrUnitType[x] = formProductReceivingDetail.detail[x].typeUnit;
        arrQtyReceive[x] = parseInt(formProductReceivingDetail.detail[x].qtyReceive);
      }

      const receivingDetail = new ProductReceivingDetail();
      receivingDetail.product_receiving_id = arrProductReceivingId;
      receivingDetail.product_id = arrProduct;
      receivingDetail.qty = arrQtyReceive;
      receivingDetail.unit = arrUnitType;
      receivingDetail.qty_request = arrQty;

      this.productReceivingService.addProductReceivingDetail(receivingDetail).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.saveMutationStock(arrProductReceivingId, formProductReceivingDetail.warehouseId, arrProduct, arrQtyReceive);
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  saveMutationStock(arrProductReceivingId: any, warehouseId: any, arrProduct: any[], arrQtyReceive: any[]) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let newDate = new Date();
      let convertDate = this.utilService.convertDate(newDate);
      let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

      let arrWarehouseId: any[] = [];
      let arrTransactionId: any[] = [];
      let arrStockMutationTypeId: any[] = [];
      let arrProductId: any[] = [];
      let arrQty: any[] = [];
      let arrValue: any[] = [];
      let arrStockMove: any[] = [];
      let arrTransDate: any[] = [];

      for(let x = 0; x < arrProductReceivingId.length; x++) {
        arrWarehouseId[x] = warehouseId;
        arrTransactionId[x] = arrProductReceivingId[x];
        arrStockMutationTypeId[x] = 'RG';
        arrProductId[x] = arrProduct[x];
        arrQty[x] = arrQtyReceive[x];
        arrValue[x] = 0;
        arrStockMove[x] = 'I';
        arrTransDate[x] = todayDate;
      }

      const stockMutationData = new StockMutation();
      stockMutationData.warehouse_id = arrWarehouseId;
      stockMutationData.transaction_id = arrTransactionId;
      stockMutationData.stock_mutation_type_id = arrStockMutationTypeId;
      stockMutationData.product_id = arrProductId;
      stockMutationData.qty = arrQty;
      stockMutationData.value = arrValue;
      stockMutationData.stock_move = arrStockMove;
      stockMutationData.trans_date = arrTransDate;

      this.productReceivingService.addStockMutation(stockMutationData).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.updateProductRequestStatus();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  updateProductRequestStatus() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {

      const formProductReceive = this.formProductReceivingCreate.value;

      const productRequestData = new ProductRequest();
      productRequestData.status = '1';
      productRequestData.updated_by = this.userProfile.username;

      this.productReceivingService.updateProductRequest(formProductReceive.productRequestId, productRequestData).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }*/

}
