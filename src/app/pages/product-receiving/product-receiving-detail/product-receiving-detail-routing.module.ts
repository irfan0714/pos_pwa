import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductReceivingDetailPage } from './product-receiving-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ProductReceivingDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductReceivingDetailPageRoutingModule {}
