import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductReceivingDetailPageRoutingModule } from './product-receiving-detail-routing.module';

import { ProductReceivingDetailPage } from './product-receiving-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ProductReceivingDetailPageRoutingModule
  ],
  declarations: [ProductReceivingDetailPage]
})
export class ProductReceivingDetailPageModule {}
