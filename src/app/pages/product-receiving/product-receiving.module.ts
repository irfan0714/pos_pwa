import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductReceivingPageRoutingModule } from './product-receiving-routing.module';

import { ProductReceivingPage } from './product-receiving.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductReceivingPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ProductReceivingPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ProductReceivingPageModule {}
