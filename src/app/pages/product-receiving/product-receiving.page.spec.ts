import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductReceivingPage } from './product-receiving.page';

describe('ProductReceivingPage', () => {
  let component: ProductReceivingPage;
  let fixture: ComponentFixture<ProductReceivingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductReceivingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductReceivingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
