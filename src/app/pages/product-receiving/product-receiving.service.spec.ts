import { TestBed } from '@angular/core/testing';

import { ProductReceivingService } from './product-receiving.service';

describe('ProductReceivingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductReceivingService = TestBed.get(ProductReceivingService);
    expect(service).toBeTruthy();
  });
});
