import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductRequestCreatePage } from './product-request-create.page';

const routes: Routes = [
  {
    path: '',
    component: ProductRequestCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductRequestCreatePageRoutingModule {}
