import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductRequestCreatePageRoutingModule } from './product-request-create-routing.module';

import { ProductRequestCreatePage } from './product-request-create.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    ProductRequestCreatePageRoutingModule,
  ],
  declarations: [ProductRequestCreatePage]
})
export class ProductRequestCreatePageModule {}
