import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ModalController, ToastController } from '@ionic/angular';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserData } from '../../../providers/user-data';
import { ProductRequestService } from '../product-request.service';
import { ProductRequestBundle } from '../../../models/product-request-bundle.model';
import { FindProductComponent } from '../../../component/find-product/find-product.component';

class Product {
  public id: string;
  public product_name: string;
}

@Component({
  selector: 'app-product-request-create',
  templateUrl: './product-request-create.page.html',
  styleUrls: ['./product-request-create.page.scss'],
})
export class ProductRequestCreatePage implements OnInit {

  token: any;
  productSearch: Product[];
  productList: Product[];
  userProfile: UserProfile = new UserProfile();
  counterList: any[] = [];
  warehouseList: any[] = [];
  unitType: any[] = ['PIECES'];

  formProductRequestCreate: FormGroup;
  productRequestId: any;

  productRequestDetailList: any[] = [];

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private userData: UserData,
    private modalController: ModalController,
    private requestService: ProductRequestService
  ) { }

  ngOnInit() {
    this.buildFormProductRequest();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getAllProduct();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getAllProduct() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      this.requestService.getProduct({ "token": this.token }).subscribe((response) => {
        if (response.status.code === 200) {
          this.productList = response.results;
        }

        this.getProductRequestCreateData();
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }
  
  getProductRequestCreateData() {
    let options = { 
      "token": this.token,
      "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
    };

    this.requestService.getRequestforCreate(options).subscribe((response) => {
      this.utilService.loadingDismiss();
      if (response.status.code === 200) {
        this.counterList = response.results.counter_data;
        this.warehouseList = response.results.warehouse_data;

        this.buildFormProductRequest();
      }
    }, () => {
      this.utilService.loadingDismiss();
      this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
    });
  }

  buildFormProductRequest() {
    let newDate = new Date();
    let convertDate = this.utilService.convertDate(newDate);
    let todayDate = convertDate.years + '-' + convertDate.months + '-' + convertDate.dates;

    this.formProductRequestCreate = this.fb.group({
      docDate: [todayDate, Validators.required],
      needDate: [todayDate, Validators.required],
      counterId: [this.counterList.length > 0 ? parseInt(this.counterList[0].id) : null, Validators.required],
      warehouseId: [this.warehouseList.length > 0 ? parseInt(this.warehouseList[0].id) : null, Validators.required],
      description: [null, Validators.required]
    });
  }

  inputBundle() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formProductRequest = this.formProductRequestCreate.value;
      let counterId = formProductRequest.counterId;
      let warehouseId = formProductRequest.warehouseId;
      let desc = formProductRequest.description;
      let docDateConvert = this.utilService.convertDate(formProductRequest.docDate);
      let needDateConvert = this.utilService.convertDate(formProductRequest.needDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;
      let dateOfNeed = needDateConvert.years + '-' + needDateConvert.months + '-' + needDateConvert.dates;

      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];

      for(let x = 0; x < this.productRequestDetailList.length; x++) {
        let htmlIdQtyRequest: any = 'qtyRequest_' + x;
        let qtyRequest: any = (<HTMLInputElement>document.getElementById(htmlIdQtyRequest)).value;

        arrProduct[x] = this.productRequestDetailList[x].product_id;
        arrQty[x] = parseInt(qtyRequest);
        arrUnitType[x] = 0;
      }

      const productRequestBundle = new ProductRequestBundle();
      productRequestBundle.productRequest.counter_id = counterId;
      productRequestBundle.productRequest.warehouse_id = warehouseId;
      productRequestBundle.productRequest.doc_date = documentDate;
      productRequestBundle.productRequest.need_date = dateOfNeed;
      productRequestBundle.productRequest.desc = desc;
      productRequestBundle.productRequest.status = '0';
      productRequestBundle.productRequest.created_by = this.userProfile.username;

      productRequestBundle.productRequestDetail.product_id = arrProduct;
      productRequestBundle.productRequestDetail.qty = arrQty;
      productRequestBundle.productRequestDetail.unit = arrUnitType;

      this.requestService.addRequestBundle(productRequestBundle)
      .subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmInput() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/product-request']);;
          }
        }
      ]
    });

    await alert.present();
  }

  addDetail() {
    let length = this.productRequestDetailList.length;
    this.productRequestDetailList.push({
      'id' : length + 1,
      'product_id' : null,
      'product_name' : null,
      'unit': null
    });
  }

  async findProduct(index: any) {
    const modal = await this.modalController.create({
      component: FindProductComponent,
      componentProps: {
        'productList': this.productList
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data = modelData.data;
      if(data) {
        let findProduct = this.productRequestDetailList.indexOf(data);
        if(findProduct === -1) {
          this.productRequestDetailList[index].product_id = data.id;
          this.productRequestDetailList[index].product_name = data.product_name;
        }
      }
    });

    return await modal.present();
  }

  deleteProduct(index: any) {
    this.productRequestDetailList.splice(index, 1);
  }

  /*addDetail() {
    const detail = this.fb.group({
      product: [null, Validators.required],
      qty: [0, Validators.required],
      typeUnit: [0]
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formProductRequestCreate.get('detail'));
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    event.component.endSearch();
  }

  deleteDetail(i: any) {
    this.getDetailArray.removeAt(i);
  }
  
  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }
  
  getCounterList() {
    let selectQuery = 'SELECT * FROM mst_counters';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.counterList = [];
            for(let x = 0; x < result.rows.length; x++) {
              let counterData = {
                'id': result.rows[x].id,
                'counter_name': result.rows[x].counter_name
              };
              this.counterList.push(counterData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getWarehouseList() {
    let selectQuery = 'SELECT * FROM mst_warehouses';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.warehouseList = [];
            for(let x = 0; x < result.rows.length; x++) {
              let warehouseData = {
                'id': result.rows[x].id,
                'warehouse_name': result.rows[x].warehouse_name
              };
              this.warehouseList.push(warehouseData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  inputData() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let todayDate = new Date();
      let convertDate = this.utilService.convertDate(todayDate);
      let years = convertDate.years.substr(2,2);
      let month = convertDate.months;

      const formProductRequest = this.formProductRequestCreate.value;
      let counterId = formProductRequest.counterId;
      let warehouseId = formProductRequest.warehouseId;
      let desc = formProductRequest.description;

      let docDateConvert = this.utilService.convertDate(formProductRequest.docDate);
      let needDateConvert = this.utilService.convertDate(formProductRequest.needDate);
      let documentDate = docDateConvert.years + '-' + docDateConvert.months + '-' + docDateConvert.dates;
      let dateOfNeed = needDateConvert.years + '-' + needDateConvert.months + '-' + needDateConvert.dates;

      let options = {
        "token": this.token,
        "counter_id": counterId
      };
      
      //salah api
      this.requestService.getRequestforCreate(options).subscribe((response) => {
        if(response.results.length > 0) {
          let latestProductRequestId: any = response.results[0].id;
          let splitId: any = latestProductRequestId.split('-');
          let lastCounter: any = splitId[0].substr(6, 3);
          let lastMonth: any = parseInt(splitId[0].substr(4, 2));
          let newCounter: any = (parseInt(lastCounter) + 1);
          if(lastMonth === parseInt(month)) {
            if(newCounter <= 9) { newCounter = '00' + newCounter.toString(); }
            if(newCounter > 9 && newCounter <= 99) { newCounter = '0' + newCounter.toString(); }
          } else {
            newCounter = '001';
          }

          this.productRequestId = 'PR' + years + month + newCounter + '-' + splitId[1];
        } else {
          let newCounterId: any;
          if(parseInt(counterId) <= 9) { newCounterId = '00' + counterId.toString(); }
          if(parseInt(counterId) > 9 && parseInt(counterId) <= 99) { newCounterId = '0' + counterId.toString(); }
          this.productRequestId = 'PR' + years + month + '001' + '-' + newCounterId;
        }

        const productRequest = new ProductRequest();
        productRequest.id = this.productRequestId;
        productRequest.counter_id = counterId;
        productRequest.warehouse_id = warehouseId;
        productRequest.doc_date = documentDate;
        productRequest.need_date = dateOfNeed;
        productRequest.desc = desc;
        productRequest.status = '0';
        productRequest.created_by = this.userProfile.username;

        this.requestService.addRequest(productRequest).subscribe((response) => {
          this.utilService.loadingDismiss();
          if(response.status.code === 201) {
            this.saveProductRequestDetail(this.productRequestId);
          } else {
            this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }

  saveProductRequestDetail(productRequestId: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formProductRequestDetail = this.formProductRequestCreate.value;
      let arrProductRequestId: any = [];
      let arrProduct: any = [];
      let arrQty: any = [];
      let arrUnitType: any = [];

      for(let x = 0; x < formProductRequestDetail.detail.length; x++) {
        arrProductRequestId[x] = productRequestId;
        arrProduct[x] = formProductRequestDetail.detail[x].product.id;
        arrQty[x] = parseInt(formProductRequestDetail.detail[x].qty);
        arrUnitType[x] = formProductRequestDetail.detail[x].typeUnit;
      }

      const requestDetail = new ProductRequestDetail();
      requestDetail.product_request_id = arrProductRequestId;
      requestDetail.product_id = arrProduct;
      requestDetail.qty = arrQty;
      requestDetail.unit = arrUnitType;

      this.requestService.addRequestDetail(requestDetail).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmInput();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }*/
}
