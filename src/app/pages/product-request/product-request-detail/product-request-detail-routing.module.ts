import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductRequestDetailPage } from './product-request-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ProductRequestDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductRequestDetailPageRoutingModule {}
