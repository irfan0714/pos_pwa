import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductRequestDetailPageRoutingModule } from './product-request-detail-routing.module';

import { ProductRequestDetailPage } from './product-request-detail.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    ProductRequestDetailPageRoutingModule
  ],
  declarations: [ProductRequestDetailPage]
})
export class ProductRequestDetailPageModule {}
