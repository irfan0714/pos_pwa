import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductRequestPage } from './product-request.page';

const routes: Routes = [
  {
    path: '',
    component: ProductRequestPage
  },
  {
    path: 'product-request-create',
    loadChildren: () => import('./product-request-create/product-request-create.module').then( m => m.ProductRequestCreatePageModule)
  },
  {
    path: 'product-request-detail',
    loadChildren: () => import('./product-request-detail/product-request-detail.module').then( m => m.ProductRequestDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductRequestPageRoutingModule {}
