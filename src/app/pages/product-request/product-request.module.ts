import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductRequestPageRoutingModule } from './product-request-routing.module';

import { ProductRequestPage } from './product-request.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductRequestPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ProductRequestPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ProductRequestPageModule {}
