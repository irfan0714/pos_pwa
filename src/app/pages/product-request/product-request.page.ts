import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ToastController, ModalController } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { UtilService } from '../../service/util.service';
import { UserProfile } from '../../models/user-profile.model';
import { UserData } from '../../providers/user-data';
import { ProductRequestComponent } from '../../component/advanced-filter/product-request/product-request.component';
import { ProductRequestService } from './product-request.service';
import { ProductRequestStatus } from '../../enum/ProductRequestStatus';
import { RoleAccess } from '../../models/role-access.model';

@Component({
  selector: 'app-product-request',
  templateUrl: './product-request.page.html',
  styleUrls: ['./product-request.page.scss'],
})
export class ProductRequestPage implements OnInit {

  page = {
    limit: 10,
    count: 0,
    offset: 0,
    orderBy: '',
    orderDir: 'desc'
  };

  rows: any;
  productRequestList: any;
  filteredData: any;
  token: any;
  userProfile: UserProfile = new UserProfile();
  counterId: any;
  statusId: any;
  counterList: any[] = [];
  docDateStart: any;
  docDateEnd: any;

  requestStatus = ProductRequestStatus;
  requestStatusList = Object.keys(ProductRequestStatus).filter(
    requestStatus => typeof this.requestStatus[requestStatus] === 'number'
  );

  keywordSearch: any;
  
  userAccess: any[] = [];
  roleAccess = new RoleAccess();
  roleName: any;

  constructor(
    private storage: Storage,
    private navCtrl: NavController,
    private utilService: UtilService,
    private toastCtrl: ToastController,
    private userData: UserData,
    private modalController: ModalController,
    private router: Router,
    private requestService: ProductRequestService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile'),
      this.storage.get('user_menu_access')
    ])
    .then(([token, profile, access]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.userAccess = access;
        this.roleName = this.userProfile.role_detail ? this.userProfile.role_detail.role_name : null;
        this.roleAccess = this.userData.checkAccess(this.router.url, this.userAccess, this.userProfile);
        this.getDataForAdvancedFilter();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  getDataForAdvancedFilter() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let options = {
        "token": this.token,
        "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list,
      };

      this.requestService.getDataforAdvancedFilter(options)
      .subscribe((response) => {
        this.utilService.loadingDismiss();
        this.counterList = response.results.counter_data;

        this.pageCallback({ offset: this.page.offset });
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        this.pageCallback({ offset: this.page.offset });
      });
    });
  }

  pageCallback(pageInfo: { count?: number, pageSize?: number, limit?: number, offset?: number }) {
    this.page.offset = pageInfo.offset;
    this.reloadTable();
  }
  
  sortCallback(sortInfo: { sorts: { dir: string, prop: string }[], column: {}, prevValue: string, newValue: string }) {
    this.page.orderDir = sortInfo.sorts[0].dir;
    this.page.orderBy = sortInfo.sorts[0].prop;
    this.reloadTable();
  }

  reloadTable() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let offset = this.page.offset + 1;
      let counterIds: any;
      if(this.counterId) { counterIds = this.counterId; }
      else { counterIds = this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list; }

      let options = {
        "token": this.token,
        "page": offset.toString(),
        "counter_id": counterIds,
        "doc_date_start": this.docDateStart ? this.docDateStart : '2000-01-01',
        "doc_date_end": this.docDateEnd ? this.docDateEnd : '4712-12-31',
        "status_id": this.statusId !== undefined ? this.statusId : 'null',
        "keywords": this.keywordSearch ? this.keywordSearch : 'null'
      };

      this.requestService.getProductRequest(options).subscribe((response) => {
        this.utilService.loadingDismiss();
        this.page.count = response.results.total;
        this.rows = response.results.data;
        this.filteredData = response.results.data;
        this.productRequestList = response.results.data;
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }

  goToProductRequestCreate() {
    this.navCtrl.navigateForward(['/product-request/product-request-create']);
  }

  goToUpdatePage(id: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        productRequestId: id
      }
    };
    this.navCtrl.navigateForward(['/product-request/product-request-detail'], navigationExtras);
  }

  async openAdvancedFilters() {
    const modal = await this.modalController.create({
      component: ProductRequestComponent,
      componentProps: {
        counterList: this.counterList,
        counterId: this.counterId,
        docDateStart: this.docDateStart,
        docDateEnd: this.docDateEnd,
        statusId: this.statusId
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      let data: any = modelData.data;

      if (data.length > 0) {
        this.keywordSearch = undefined;
        if(data[0].doc_date_start) {
          let convertDateStart = this.utilService.convertDate(data[0].doc_date_start);
          this.docDateStart = convertDateStart.years + '-' + convertDateStart.months + '-' + convertDateStart.dates;
        }
        if(data[0].doc_date_end) {
          let convertDateEnd = this.utilService.convertDate(data[0].doc_date_end);
          this.docDateEnd = convertDateEnd.years + '-' + convertDateEnd.months + '-' + convertDateEnd.dates;
        }
        
        this.counterId = data[0].counter_id;
        this.statusId = data[0].status_id;

        this.pageCallback({ offset: this.page.offset });
      }
    });

    return await modal.present();
  }

  filterDatatable(event) {
    let val = event.target.value.toLowerCase();
    let columnLength = 5;
    let keys = Object.keys(this.productRequestList[0]);
    this.rows = this.filteredData.filter(function(item){
      for (let i=0; i < columnLength; i++){
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val){
          return true;
        }
      }
    });

    this.page.offset = 0;
  }

  checkStatus(status: any) {
    let cssClass: any = '';
    if(status === '0') { cssClass = 'status pending'}
    if(status === '1') { cssClass = 'status sent'}
    if(status === '2') { cssClass = 'status void'}
    if(status === '3') { cssClass = 'status void'}
    if(status === '4') { cssClass = 'status pending'}
    if(status === '5') { cssClass = 'status pending'}
    if(status === '6') { cssClass = 'status sent'}
    return cssClass;
  }

  searchWithKeywords() {
    this.counterId = undefined;
    this.docDateStart = undefined;
    this.docDateEnd = undefined;
    this.statusId = undefined;

    this.pageCallback({ offset: this.page.offset });
  }

  clearFilters() {
    this.keywordSearch = undefined;
    this.counterId = undefined;
    this.docDateStart = undefined;
    this.docDateEnd = undefined;
    this.statusId = undefined;

    this.pageCallback({ offset: this.page.offset });
  }

  /*openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  getLocalData(table: any, column: any, name: any) {
    this.requestService.getLocalData(name, { "token": this.token }).subscribe((response) => {
      if (response.status.code == 200) {
        if(response.results.length > 0) {
          let tableName = table;
          let columnList = column;
          
          this.createTable(tableName, columnList).then((res) => {
            if(tableName === 'mst_counters') { this.inputCounterLocalData(response); }
            if(tableName === 'mst_warehouses') { this.inputWarehouseLocalData(response); }
            
          }, (err) => {
            console.log(err);
          });
        }
      }
    }, () => {
      this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
    });
  }

  createTable(tableName: any, columnList: any) {
    let sqlQuery: string = 'CREATE TABLE IF NOT EXISTS ' + tableName + columnList;

    return new Promise((resolve, reject) => {
      this.db.transaction((tx) => {
        tx.executeSql(sqlQuery, [],
        (tx, result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
      });
    });
  }

  execQuery(sqlQuery: any) {
    this.db.transaction((tx) => {
      tx.executeSql(sqlQuery, [],
      (tx, result) => {
      }, (error) => {
        console.log(error);
      });
    });
  }

  inputCounterLocalData(response: any) {
    for (let i = 0; i < response.results.length; i++) {

      let insertQuery = 'INSERT INTO ' + this.counterTableName + this.counterColumnList + ' VALUES (' +
      response.results[i].id + ', ' + response.results[i].branch_id + ', "' +
      response.results[i].counter_name + '", "' +
      response.results[i].trans_date + '", "' + response.results[i].first_address + '", "' +
      response.results[i].last_address + '", "' + response.results[i].phone + '", "' + 
      response.results[i].footer_text + '", "' + response.results[i].latitude + '", "' + 
      response.results[i].longitude + '", "' + response.results[i].active + '", "' +
      response.results[i].created_by + '", "' + response.results[i].updated_by + '", "' +
      response.results[i].created_at + '", "' + response.results[i].updated_at + '")';

      let selectQuery = 'SELECT * FROM ' + this.counterTableName + ' WHERE id = ' + response.results[i].id;
      let deleteQuery = 'DELETE FROM ' + this.counterTableName + ' WHERE id = ' + response.results[i].id;

      this.db.transaction((tx) => {
        tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length === 0) {
            this.execQuery(insertQuery);
          } else {
            this.execQuery(deleteQuery);
            this.execQuery(insertQuery);
          }
        }, (error) => {
          console.log(error);
        });
      });
    }
  }

  inputWarehouseLocalData(response: any) {
    for (let i = 0; i < response.results.length; i++) {

      let insertQuery = 'INSERT INTO ' + this.warehouseTableName + this.warehouseColumnList + ' VALUES (' +
      response.results[i].id + ', ' + response.results[i].counter_id + ', "' +
      response.results[i].warehouse_name + '", ' + response.results[i].warehouse_type_id + ', "' +
      response.results[i].unit_size + '", ' + response.results[i].length_size + ', ' +
      response.results[i].width_size + ', ' + response.results[i].height_size + ', "' +
      response.results[i].active + '", "' + response.results[i].created_by + '", "' +
      response.results[i].updated_by + '", "' + response.results[i].created_at + '", "' +
      response.results[i].updated_at + '")';

      let selectQuery = 'SELECT * FROM ' + this.warehouseTableName + ' WHERE id = ' + response.results[i].id;
      let deleteQuery = 'DELETE FROM ' + this.warehouseTableName + ' WHERE id = ' + response.results[i].id;

      this.db.transaction((tx) => {
        tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length === 0) {
            this.execQuery(insertQuery);
          } else {
            this.execQuery(deleteQuery);
            this.execQuery(insertQuery);
          }
        }, (error) => {
          console.log(error);
        });
      });
    }
  }*/

}
