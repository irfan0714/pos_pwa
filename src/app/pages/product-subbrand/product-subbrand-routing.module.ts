import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductSubbrandPage } from './product-subbrand.page';

const routes: Routes = [
  {
    path: '',
    component: ProductSubbrandPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductSubbrandPageRoutingModule {}
