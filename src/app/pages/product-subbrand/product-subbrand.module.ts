import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductSubbrandPageRoutingModule } from './product-subbrand-routing.module';

import { ProductSubbrandPage } from './product-subbrand.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductSubbrandPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ProductSubbrandPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ProductSubbrandPageModule {}
