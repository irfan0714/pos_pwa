import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductSubcategoryPage } from './product-subcategory.page';

const routes: Routes = [
  {
    path: '',
    component: ProductSubcategoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductSubcategoryPageRoutingModule {}
