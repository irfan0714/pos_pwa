import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductSubcategoryPageRoutingModule } from './product-subcategory-routing.module';

import { ProductSubcategoryPage } from './product-subcategory.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductSubcategoryPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ProductSubcategoryPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ProductSubcategoryPageModule {}
