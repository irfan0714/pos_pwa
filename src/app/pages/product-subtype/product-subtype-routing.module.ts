import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductSubtypePage } from './product-subtype.page';

const routes: Routes = [
  {
    path: '',
    component: ProductSubtypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductSubtypePageRoutingModule {}
