import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductSubtypePage } from './product-subtype.page';

describe('ProductSubtypePage', () => {
  let component: ProductSubtypePage;
  let fixture: ComponentFixture<ProductSubtypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSubtypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductSubtypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
