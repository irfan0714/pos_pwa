import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductTypePageRoutingModule } from './product-type-routing.module';

import { ProductTypePage } from './product-type.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductTypePageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ProductTypePage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ProductTypePageModule {}
