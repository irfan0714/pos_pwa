import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromotionDetailPageRoutingModule } from './promotion-detail-routing.module';

import { PromotionDetailPage } from './promotion-detail.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    PromotionDetailPageRoutingModule
  ],
  declarations: [PromotionDetailPage]
})
export class PromotionDetailPageModule {}
