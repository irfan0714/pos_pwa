import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController, ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../service/util.service';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../models/user-profile.model';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Storage } from '@ionic/storage';
import { PromotionsService } from '../promotions/promotions.service';
import { MstPromotionDetail } from '../../models/mst-promotion.model';
import { PromotionDetailComponent } from '../../component/promotion-detail/promotion-detail.component';

class Product {
  public id: string;
  public product_name: string;
  public barcode: string;
}

@Component({
  selector: 'app-promotion-detail',
  templateUrl: './promotion-detail.page.html',
  styleUrls: ['./promotion-detail.page.scss'],
})
export class PromotionDetailPage implements OnInit {

  token: any;
  promotionId: any;
  promotionData: any[] = [];
  promotionDetailData: any;
  promotionFreeItemData: any[] = [];
  promotionTypeId: any;
  promotionTypeName: any;
  productSearch: Product[];
  productList: Product[];
  userProfile: UserProfile = new UserProfile();
  discountType: any[] = ['Persentase', 'Nominal', 'Bonus Item'];
  stockData: any[] = [];
  searchValue: any[] = [];
  productScan: any[] = [];
  productScanDisplay: any[] = [];
  productFreeItem: any[] = [];
  productFreeItemDisplay: any[] = [];

  formPromotionDetailEdit: FormGroup;
  
  mstBarcodeList: Array<any>;
  mstProductList: Array<any>;

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private promotionService: PromotionsService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.buildFormPromotionDetailEdit();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getData();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  buildFormPromotionDetailEdit() {
    this.formPromotionDetailEdit = this.fb.group({
      counterName: [],
      warehouseName: [],
      promotionName: [],
      promotionType: [],
      detail: this.fb.array([])
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.promotionId = snapshot.promotionId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.promotionService.getPromotionDetailforEdit(this.promotionId, { "token": this.token }).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.promotionData = response.results.promotion_data;
          this.promotionDetailData = response.results.promotion_details_data;
          this.promotionFreeItemData = response.results.promotion_free_items;
          this.productList = response.results.product_data;
          this.stockData = response.results.stock_data;

          if(this.promotionData.length > 0) {
            this.promotionTypeId = this.promotionData[0].promotion_type_id;
            this.promotionTypeName = this.promotionData[0].promotion_type_name;
            this.formPromotionDetailEdit = this.fb.group({
              counterName: [this.promotionData[0].counter_name],
              warehouseName: [this.promotionData[0].warehouse_name],
              promotionName: [this.promotionData[0].promotion_name],
              promotionType: [this.promotionData[0].promotion_type_name],
              detail: this.fb.array([])
            });
          }

          if(this.promotionDetailData) {
            for(let i = 0; i < this.promotionDetailData.length; i++) {
              if(this.promotionTypeName === 'FREE ITEM (ITEM)' || this.promotionTypeName === 'FREE ITEM (NOMINAL)') {
                if(this.promotionTypeName === 'FREE ITEM (ITEM)') {
                  let str = this.promotionDetailData[i].product_id;
                  let re = /;/gi;
                  let newstr = str.replace(re, ", ");

                  this.productScan[i] = this.promotionDetailData[i].product_id;
                  this.productScanDisplay[i] = newstr;
                }

                let productList: string = '';
                let productListDisplay: string = '';
                if(this.promotionFreeItemData.length > 0) {
                  for(let x = 0; x < this.promotionFreeItemData.length; x++) {
                    if(this.promotionFreeItemData[x].promotion_detail_id === this.promotionDetailData[i].id) {
                      productList = productList + this.promotionFreeItemData[x].product_id;
                      productListDisplay = productListDisplay + this.promotionFreeItemData[x].product_id;
                      if((x+1) !== this.promotionFreeItemData.length) {
                        if(this.promotionFreeItemData[x+1].promotion_detail_id === this.promotionDetailData[i].id) {
                          productList = productList + ';';
                          productListDisplay = productListDisplay + ', ';
                        }
                      }
                    }
                  }
                }

                this.productFreeItem[i] = productList;
                this.productFreeItemDisplay[i] = productListDisplay;
                
              }

              let filterProduct;
              if(this.promotionTypeName === 'PERSENTASE' || this.promotionTypeName === 'NOMINAL') {
                let productId: string = this.promotionDetailData[i].product_id.trim().toLowerCase();
                filterProduct = this.filterProducts(this.productList, productId);
              }
              
              // let checkStock: any = this.stockData.find(x => x.product_id === productId);
              // let stockProduct: number = checkStock ? checkStock.end_stock : 0;
              // let freePromoUsed: number = this.promotionDetailData[i].free_item_used;

              const detail = this.fb.group({
                detailId: [this.promotionDetailData[i].id],
                product: [filterProduct ? filterProduct[0] : null , this.promotionTypeName !== 'FREE ITEM (ITEM)' && this.promotionTypeName !== 'FREE ITEM (NOMINAL)' ? Validators.required : null],
                minTotalTrans: [parseInt(this.promotionDetailData[i].minimum_transaction)],
                value: [parseInt(this.promotionDetailData[i].value), Validators.required],
                freeQty: [this.promotionDetailData[i].free_qty],
                //stock: [{value: stockProduct, disabled: true}],
                minBuy: [this.promotionDetailData[i].minimum_buy],
                // promoUsed: [{value: freePromoUsed, disabled: true}],
                multipleFlag: [this.promotionDetailData[i].multiple_flag]
              });
              this.getDetailArray.push(detail);
              // this.searchValue.push(filterProduct[0].product_name);
            }
          }

          this.mstBarcodeList = [];
          this.mstProductList = [];
          for (let i = 0; i < this.productList.length; i++) {
            
            let checkStock: any = this.stockData.find(x => x.product_id === this.productList[i].id);
            let stockProduct: number = checkStock ? checkStock.end_stock : 0;

            this.mstBarcodeList[this.productList[i].barcode] = {
              id: String(this.productList[i].id),
              barcode: this.productList[i].barcode,
              product_name: this.productList[i].product_name,
              stock: stockProduct,
              terpakai: 0
            }
            
            let id = String(this.productList[i].id);
            let index: number = parseInt(id);
            if(Number.isNaN(index)) {
              //
            } else {
              this.mstProductList[index] = {
                id: String(this.productList[i].id),
                barcode: this.productList[i].barcode,
                product_name: this.productList[i].product_name,
                stock: stockProduct,
                terpakai: 0
              }
            }
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  addDetail() {
    const detail = this.fb.group({
      detailId: [0],
      product: [this.promotionTypeName !== 'FREE ITEM (ITEM)' && this.promotionTypeName !== 'FREE ITEM (NOMINAL)' ? Validators.required : null],
      minTotalTrans: [0],
      value: [0],
      freeQty: [0],
      // stock: [{value: 0, disabled: true}],
      minBuy: [0],
      // promoUsed: [{value: 0, disabled: true}],
      multipleFlag: ['0']
    });
    this.getDetailArray.push(detail);
  }

  get getDetailArray() {
    return (<FormArray>this.formPromotionDetailEdit.get('detail'));
  }

  filterProducts(products: Product[], text: string) {
    return products.filter(product => {
      return product.product_name.toLowerCase().indexOf(text) !== -1 ||
        product.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  searchProducts(event: {
    component: IonicSelectableComponent,
    text: string
  }, index: any) {

    let text: string;

    if(this.searchValue.length > 0) {
      text = this.searchValue[index] ? this.searchValue[index] : '';
    }

    text = event.text !== '' ? event.text.trim().toLowerCase() : text.trim().toLowerCase();
    event.component.startSearch();

    if (!text) {
      event.component.items = [];
      event.component.endSearch();
      return;
    }

    event.component.items = this.filterProducts(this.productList, text);
    this.searchValue[index] = text;
    event.component.endSearch();
  }

  productChange(event: {
    component: IonicSelectableComponent,
    value: any
  }, index: any) {
    let product: any = event.value;
    if(product) {
      let checkStock: any = this.stockData.find(x => x.product_id === product.id);
      let stock: any = checkStock ? checkStock.end_stock : 0;
      (this.getDetailArray.at(index) as FormGroup).get('stock').patchValue(stock);
    }
  }

  deleteDetail(i: any) {
    this.getDetailArray.removeAt(i);
    if(this.promotionTypeName === 'FREE ITEM (ITEM)') {
      this.productScan.splice(i, 1);
      this.productScanDisplay.splice(i, 1);
      this.productFreeItem.splice(i, 1);
      this.productFreeItemDisplay.splice(i, 1);
    }

    if(this.promotionTypeName === 'FREE ITEM (NOMINAL)') {
      this.productFreeItem.splice(i, 1);
      this.productFreeItemDisplay.splice(i, 1);
    }
  }

  validateInput() {
    const formPromotionDetail = this.formPromotionDetailEdit.value;
    let result = false;

    if(this.promotionTypeName === 'FREE ITEM (ITEM)') {
      let x = 0;
      while(x < formPromotionDetail.detail.length) {
        if(!this.productScan[x]) {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Product Scan yang masih kosong, silahkan diisi terlebih dahulu!' }).then(t => t.present());
          result = true;
          break;
        }

        if(!this.productFreeItem[x]) {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Product Bonus yang masih kosong, silahkan diisi terlebih dahulu!' }).then(t => t.present());
          result = true;
          break;
        }

        x++;
      }
    }

    if(this.promotionTypeName === 'FREE ITEM (NOMINAL)') {
      let x = 0;
      while(x < formPromotionDetail.detail.length) {

        if(!this.productFreeItem[x]) {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Product Bonus yang masih kosong, silahkan diisi terlebih dahulu!' }).then(t => t.present());
          result = true;
          break;
        }

        x++;
      }
    }

    if(this.promotionTypeName === 'PERSENTASE' || this.promotionTypeName === 'NOMINAL') {
      let x = 0;
      while(x < formPromotionDetail.detail.length) {

        if(!formPromotionDetail.detail[x].product.id) {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Product Promo yang masih kosong, silahkan diisi terlebih dahulu!' }).then(t => t.present());
          result = true;
          break;
        }

        x++;
      }
    }

    if(result === false) {
      this.updateData();
    }
  }

  updateData() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formPromotionDetail = this.formPromotionDetailEdit.value;
      let arrPromotionId: any = [];
      let arrProduct: any = [];
      let arrMinimumTransaction: any = [];
      let arrType: any = [];
      let arrValue: any = [];
      let arrFreeQty: any = [];
      let arrMinimumBuy: any = [];
      let arrFreeItemUsed: any = [];
      let arrMultipleFlag: any = [];
      let arrFreeItem: any = [];

      for(let x = 0; x < formPromotionDetail.detail.length; x++) {
        arrPromotionId[x] = parseInt(this.promotionId);
        arrProduct[x] = this.promotionTypeName === 'FREE ITEM (ITEM)' ? this.productScan[x] : this.promotionTypeName === 'NOMINAL' || this.promotionTypeName === 'PERSENTASE' ? formPromotionDetail.detail[x].product.id : null;
        arrMinimumTransaction[x] = parseInt(formPromotionDetail.detail[x].minTotalTrans);
        arrType[x] = this.promotionTypeId;
        arrValue[x] = parseInt(formPromotionDetail.detail[x].value);
        arrFreeQty[x] = parseInt(formPromotionDetail.detail[x].freeQty);
        arrMinimumBuy[x] = parseInt(formPromotionDetail.detail[x].minBuy);
        arrFreeItemUsed[x] = 0;
        arrMultipleFlag[x] = formPromotionDetail.detail[x].multipleFlag;
        this.promotionTypeName === 'FREE ITEM (ITEM)' || this.promotionTypeName === 'FREE ITEM (NOMINAL)' ? arrFreeItem[x] = this.productFreeItem[x] : arrFreeItem = [];
      }

      const promotionDetail = new MstPromotionDetail();
      promotionDetail.promotion_id = arrPromotionId;
      promotionDetail.product_id = arrProduct;
      promotionDetail.type = arrType;
      promotionDetail.value = arrValue;
      promotionDetail.free_qty = arrFreeQty;
      promotionDetail.minimum_buy = arrMinimumBuy;
      promotionDetail.minimum_transaction = arrMinimumTransaction;
      promotionDetail.free_item_used = arrFreeItemUsed;
      promotionDetail.multiple_flag = arrMultipleFlag;
      promotionDetail.free_item = arrFreeItem;

      this.promotionService.addPromotionDetail(promotionDetail).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    })
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/promotions']);;
          }
        }
      ]
    });

    await alert.present();
  }

  checkPromo(field: any) {
    let cssClass: any = '';

    if(field === 'value') {
      if(this.promotionTypeName === 'FREE ITEM (ITEM)' || this.promotionTypeName === 'FREE ITEM (NOMINAL)') {
        cssClass = 'display-none';
      }
    }

    if(field === 'free_item_item') {
      if(this.promotionTypeName !== 'FREE ITEM (ITEM)') {
        cssClass = 'display-none';
      }
    }

    if(field === 'free_item_nominal') {
      if(this.promotionTypeName !== 'FREE ITEM (NOMINAL)') {
        cssClass = 'display-none';
      }
    }

    if(field === 'free_item_item_nominal') {
      if(this.promotionTypeName !== 'FREE ITEM (ITEM)' && this.promotionTypeName !== 'FREE ITEM (NOMINAL)') {
        cssClass = 'display-none';
      }
    }

    if(field === 'value_free_item_item') {
      if(this.promotionTypeName === 'FREE ITEM (NOMINAL)') {
        cssClass = 'display-none';
      }
    }
    return cssClass;
  }

  async openPromotionDetail(index: any, action: any) {
    
    const modal = await this.modalController.create({
      component: PromotionDetailComponent,
      componentProps: {
        'mstBarcodeList': this.mstBarcodeList,
        'mstProductList': this.mstProductList,
        'productScan': this.productScan[index] ? this.productScan[index] : null,
        'productFreeItem': this.productFreeItem[index] ? this.productFreeItem[index] : null,
        'action': action
      },
      backdropDismiss: false
    });

    modal.onDidDismiss().then((modelData) => {
      if (modelData.data !== '') {
        let dataProduct: any[] = modelData.data;
        let productList: string = '';
        let productListDisplay: string = '';
        if(dataProduct.length > 0) {
          for(let i = 0; i < dataProduct.length; i++) {
            productList = productList + dataProduct[i].id;
            productListDisplay = productListDisplay + dataProduct[i].id;
            if((i+1) !== dataProduct.length) {
              productList = productList + ';';
              productListDisplay = productListDisplay + ', ';
            }
          }

          if(action === 'product') {
            this.productScan[index] = productList;
            this.productScanDisplay[index] = productListDisplay;
          }

          if(action === 'bonus') {
            this.productFreeItem[index] = productList;
            this.productFreeItemDisplay[index] = productListDisplay;
          }
        } else {
          if(action === 'product') {
            this.productScan[index] = null;
            this.productScanDisplay[index] = null;
          }

          if(action === 'bonus') {
            this.productFreeItem[index] = null;
            this.productFreeItemDisplay[index] = null;
          }
        }
      } else {
        if(action === 'product') {
          this.productScan[index] = null;
          this.productScanDisplay[index] = null;
        }

        if(action === 'bonus') {
          this.productFreeItem[index] = null;
          this.productFreeItemDisplay[index] = null;
        }
      }
    });

    return await modal.present();
  }

}
