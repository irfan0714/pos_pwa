import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromotionTypeCreatePageRoutingModule } from './promotion-type-create-routing.module';

import { PromotionTypeCreatePage } from './promotion-type-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PromotionTypeCreatePageRoutingModule
  ],
  declarations: [PromotionTypeCreatePage]
})
export class PromotionTypeCreatePageModule {}
