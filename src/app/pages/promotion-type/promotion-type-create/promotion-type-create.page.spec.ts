import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromotionTypeCreatePage } from './promotion-type-create.page';

describe('PromotionTypeCreatePage', () => {
  let component: PromotionTypeCreatePage;
  let fixture: ComponentFixture<PromotionTypeCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionTypeCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromotionTypeCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
