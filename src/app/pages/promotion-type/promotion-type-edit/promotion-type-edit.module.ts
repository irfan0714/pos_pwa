import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromotionTypeEditPageRoutingModule } from './promotion-type-edit-routing.module';

import { PromotionTypeEditPage } from './promotion-type-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PromotionTypeEditPageRoutingModule
  ],
  declarations: [PromotionTypeEditPage]
})
export class PromotionTypeEditPageModule {}
