import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromotionTypeEditPage } from './promotion-type-edit.page';

describe('PromotionTypeEditPage', () => {
  let component: PromotionTypeEditPage;
  let fixture: ComponentFixture<PromotionTypeEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionTypeEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromotionTypeEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
