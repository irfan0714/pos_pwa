import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PromotionTypePage } from './promotion-type.page';

const routes: Routes = [
  {
    path: '',
    component: PromotionTypePage
  },
  {
    path: 'promotion-type-create',
    loadChildren: () => import('./promotion-type-create/promotion-type-create.module').then( m => m.PromotionTypeCreatePageModule)
  },
  {
    path: 'promotion-type-edit',
    loadChildren: () => import('./promotion-type-edit/promotion-type-edit.module').then( m => m.PromotionTypeEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromotionTypePageRoutingModule {}
