import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromotionTypePageRoutingModule } from './promotion-type-routing.module';

import { PromotionTypePage } from './promotion-type.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PromotionTypePageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [PromotionTypePage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PromotionTypePageModule {}
