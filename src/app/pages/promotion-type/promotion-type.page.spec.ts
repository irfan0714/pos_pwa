import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromotionTypePage } from './promotion-type.page';

describe('PromotionTypePage', () => {
  let component: PromotionTypePage;
  let fixture: ComponentFixture<PromotionTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionTypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromotionTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
