import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PromotionCreatePage } from './promotion-create.page';

const routes: Routes = [
  {
    path: '',
    component: PromotionCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromotionCreatePageRoutingModule {}
