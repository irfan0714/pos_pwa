import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromotionCreatePageRoutingModule } from './promotion-create-routing.module';

import { PromotionCreatePage } from './promotion-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PromotionCreatePageRoutingModule
  ],
  declarations: [PromotionCreatePage]
})
export class PromotionCreatePageModule {}
