import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromotionCreatePage } from './promotion-create.page';

describe('PromotionCreatePage', () => {
  let component: PromotionCreatePage;
  let fixture: ComponentFixture<PromotionCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromotionCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
