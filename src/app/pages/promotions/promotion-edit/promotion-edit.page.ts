import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { PromotionsService } from '../promotions.service';
import { MstPromotion } from '../../../models/mst-promotion.model';

@Component({
  selector: 'app-promotion-edit',
  templateUrl: './promotion-edit.page.html',
  styleUrls: ['./promotion-edit.page.scss'],
})
export class PromotionEditPage implements OnInit {

  formPromotionEdit: FormGroup;
  promotionId: any;
  promotionData: any;
  token: any;
  promotionTypeData: any[] = [];
  counterList: any[] = [];
  warehouseList: any[] = [];
  cashierType: any[] = ['Semua Kasir', 'Kasir Reguler', 'Kasir Reseller'];
  statusPromotion: any[] = ['Not Active', 'Active'];
  userProfile: UserProfile = new UserProfile();

  db: any;

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private promotionService: PromotionsService
  ) { }

  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  ngOnInit() {
    this.buildFormPromotionEdit();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getData();
        // this.openDB();
        // this.getCounterList();
        // this.getWarehouseList();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  buildFormPromotionEdit() {
    this.formPromotionEdit = this.fb.group({
      counterId: [null],
      warehouseId: [null],
      promotionName: [],
      earlyPeriod: [],
      endPeriod: [],
      promotionType: [],
      promotionCashierType: [],
      minimum: [],
      validOnSun: [],
      validOnMon: [],
      validOnTue: [],
      validOnWed: [],
      validOnThu: [],
      validOnFri: [],
      validOnSat: [],
      activeStatus: []
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.promotionId = snapshot.promotionId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        let options = {
          "token": this.token,
          "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
        };
        this.promotionService.getPromotionforEdit(this.promotionId, options).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.promotionData = response.results.data;
          this.promotionTypeData = response.results.promotion_type_data;
          this.counterList = response.results.counter_data;
          this.warehouseList = response.results.warehouse_data;

          let validOnSun: any = false, validOnMon: any = false, validOnTue: any = false, validOnWed: any = false,
          validOnThu: any = false, validOnFri: any = false, validOnSat: any = false;

          if(this.promotionData.valid_on_su === '1') { validOnSun = true; };
          if(this.promotionData.valid_on_mo === '1') { validOnMon = true; };
          if(this.promotionData.valid_on_tu === '1') { validOnTue = true; };
          if(this.promotionData.valid_on_we === '1') { validOnWed = true; };
          if(this.promotionData.valid_on_th === '1') { validOnThu = true; };
          if(this.promotionData.valid_on_fr === '1') { validOnFri = true; };
          if(this.promotionData.valid_on_sa === '1') { validOnSat = true; };

          this.formPromotionEdit = this.fb.group({
            counterId: [this.promotionData.counter_id],
            warehouseId: [this.promotionData.warehouse_id],
            promotionName: [this.promotionData.promotion_name, Validators.required],
            earlyPeriod: [this.promotionData.early_period, Validators.required],
            endPeriod: [this.promotionData.end_period, Validators.required],
            promotionType: [this.promotionData.promotion_type_id, Validators.required],
            promotionCashierType: [parseInt(this.promotionData.promotion_cashier_type), Validators.required],
            minimum: [this.promotionData.minimum, Validators.required],
            validOnSun: [validOnSun],
            validOnMon: [validOnMon],
            validOnTue: [validOnTue],
            validOnWed: [validOnWed],
            validOnThu: [validOnThu],
            validOnFri: [validOnFri],
            validOnSat: [validOnSat],
            activeStatus: [parseInt(this.promotionData.status), Validators.required]
          });
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  updateData() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const promotionData = this.formPromotionEdit.getRawValue();
      let earlyPeriods = this.utilService.convertDate(promotionData.earlyPeriod);
      let endPeriods = this.utilService.convertDate(promotionData.endPeriod);
      let newEarlyPeriod = earlyPeriods.years + '-' + earlyPeriods.months + '-' + earlyPeriods.dates;
      let newEndPeriod = endPeriods.years + '-' + endPeriods.months + '-' + endPeriods.dates;

      let validOnSun: any = '0', validOnMon: any = '0', validOnTue: any = '0', validOnWed: any = '0', validOnThu: any = '0',
      validOnFri: any = '0', validOnSat: any = '0';

      if(promotionData.validOnSun === true) { validOnSun = '1'; };
      if(promotionData.validOnMon === true) { validOnMon = '1'; };
      if(promotionData.validOnTue === true) { validOnTue = '1'; };
      if(promotionData.validOnWed === true) { validOnWed = '1'; };
      if(promotionData.validOnThu === true) { validOnThu = '1'; };
      if(promotionData.validOnFri === true) { validOnFri = '1'; };
      if(promotionData.validOnSat === true) { validOnSat = '1'; };

      const mstPromotion = new MstPromotion();
      mstPromotion.promotion_name = promotionData.promotionName;
      mstPromotion.early_period = newEarlyPeriod;
      mstPromotion.end_period = newEndPeriod;
      mstPromotion.promotion_type_id = promotionData.promotionType;
      mstPromotion.promotion_cashier_type = String(promotionData.promotionCashierType);
      mstPromotion.minimum = promotionData.minimum;
      mstPromotion.valid_on_su = validOnSun;
      mstPromotion.valid_on_mo = validOnMon;
      mstPromotion.valid_on_tu = validOnTue;
      mstPromotion.valid_on_we = validOnWed;
      mstPromotion.valid_on_th = validOnThu;
      mstPromotion.valid_on_fr = validOnFri;
      mstPromotion.valid_on_sa = validOnSat;
      mstPromotion.status = String(promotionData.activeStatus);
      mstPromotion.updated_by = this.userProfile.username;

      this.promotionService.updatePromotion(this.promotionId, mstPromotion).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 200) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      })
    });
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/promotions']);;
          }
        }
      ]
    });

    await alert.present();
  }

  getCounterList() {
    let selectQuery = 'SELECT * FROM mst_counters';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.counterList = [];
            for(let x = 0; x < result.rows.length; x++) {
              let counterData = {
                'id': result.rows[x].id,
                'counter_name': result.rows[x].counter_name
              };
              this.counterList.push(counterData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

  getWarehouseList() {
    let selectQuery = 'SELECT * FROM mst_warehouses';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length > 0) {
            this.warehouseList = [];
            for(let x = 0; x < result.rows.length; x++) {
              let warehouseData = {
                'id': result.rows[x].id,
                'warehouse_name': result.rows[x].warehouse_name
              };
              this.warehouseList.push(warehouseData);
            }
          }
      }, (error) => {
        console.log(error);
      });
    });
  }

}
