import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, AlertController, ToastController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { UtilService } from '../../service/util.service';
import { UserProfile } from '../../models/user-profile.model';
import { UserData } from '../../providers/user-data';
import { PromotionsService } from './promotions.service';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.page.html',
  styleUrls: ['./promotions.page.scss'],
})
export class PromotionsPage implements OnInit {

  page = {
    limit: 10,
    count: 0,
    offset: 0,
    orderBy: '',
    orderDir: 'desc'
  };

  rows: any;
  token: any;
  userProfile: UserProfile = new UserProfile();
  promotionList: any;
  filteredData: any;
  counterId: any = '';

  db: any;

  counterTableName = 'mst_counters';
  counterColumnList = '(id, branch_id, counter_name, trans_date, first_address, last_address, phone, footer_text, latitude, longitude, active, created_by, updated_by, created_at, updated_at)';
  warehouseTableName = 'mst_warehouses';
  warehouseColumnList = '(id, counter_id, warehouse_name, warehouse_type_id, unit_size, length_size, width_size, height_size, active, created_by, updated_by, created_at, updated_at)';

  constructor(
    private storage: Storage,
    private navCtrl: NavController,
    private alertController: AlertController,
    private utilService: UtilService,
    private toastCtrl: ToastController,
    private userData: UserData,
    private promotionService: PromotionsService
  ) { }

  ngOnInit() {
  }

  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  ionViewDidEnter() {
    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.pageCallback({ offset: this.page.offset });
        /*this.openDB();
        this.getLocalData(this.counterTableName, this.counterColumnList, 'counter');
        this.getLocalData(this.warehouseTableName, this.warehouseColumnList, 'warehouse');*/
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  pageCallback(pageInfo: { count?: number, pageSize?: number, limit?: number, offset?: number }) {
    this.page.offset = pageInfo.offset;
    this.reloadTable();
  }
  
  sortCallback(sortInfo: { sorts: { dir: string, prop: string }[], column: {}, prevValue: string, newValue: string }) {
    this.page.orderDir = sortInfo.sorts[0].dir;
    this.page.orderBy = sortInfo.sorts[0].prop;
    this.reloadTable();
  }

  reloadTable() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let offset = this.page.offset + 1;
      let options = {
        "token": this.token,
        "page": offset.toString(),
        "counter_id": this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list
      };

      this.promotionService.getPromotion(options).subscribe((response) => {
        this.utilService.loadingDismiss();
        this.page.count = response.results.total;
        this.rows = response.results.data;
        this.filteredData = response.results.data;
        this.promotionList = response.results.data;
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        if(err.error.error === 'token_expired') {
          this.userData.logout().then(() => {
            this.toastCtrl.create({ duration: 2000, message: 'Token telah expired. Silahkan login kembali' }).then(t => t.present());
            this.navCtrl.navigateForward(['/login']);
          });
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      });
    });
  }

  goToPromotionCreate() {
    this.navCtrl.navigateForward(['/promotions/promotion-create']);
  }

  goToUpdatePage(id: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        promotionId: id
      }
    };
    this.navCtrl.navigateForward(['/promotions/promotion-edit'], navigationExtras);
  }

  goToPromotionDetail(id: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        promotionId: id
      }
    };
    this.navCtrl.navigateForward(['/promotion-detail'], navigationExtras);
  }

  deleteData(id: any) {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      this.promotionService.deletePromotion(id).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 200) {
          this.showDeleteSuccess();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showDeleteSuccess() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Berhasil hapus data!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.pageCallback({ offset: this.page.offset });
          }
        }
      ]
    });

    await alert.present();
  }

  async showConfirmDelete(id: any) {
    const alert = await this.alertController.create({
      header: 'Delete Confirmation',
      cssClass:'custom-alert-class',
      message: 'Apakah anda yakin untuk hapus data ini?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {}
        },
        {
          text: 'OK',
          handler: () => {
            this.deleteData(id);
          }
        }
      ]
    });

    await alert.present();
  }

  filterDatatable(event) {
    let val = event.target.value.toLowerCase();
    let columnLength = 18;
    let keys = Object.keys(this.promotionList[0]);
    this.rows = this.filteredData.filter(function(item){
      for (let i=0; i < columnLength; i++){
        if(item[keys[i]] !== null) {
          if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val){
            return true;
          }
        }
      }
    });

    this.page.offset = 0;
  }

  getLocalData(table: any, column: any, name: any) {
    this.promotionService.getLocalData(name, { "token": this.token }).subscribe((response) => {
      if (response.status.code == 200) {
        if(response.results.length > 0) {
          let tableName = table;
          let columnList = column;
          
          this.createTable(tableName, columnList).then((res) => {
            if(tableName === 'mst_counters') { this.inputCounterLocalData(response); }
            if(tableName === 'mst_warehouses') { this.inputWarehouseLocalData(response); }
            
          }, (err) => {
            console.log(err);
          });
        }
      }
    }, () => {
      this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
    });
  }

  createTable(tableName: any, columnList: any) {
    let sqlQuery: string = 'CREATE TABLE IF NOT EXISTS ' + tableName + columnList;

    return new Promise((resolve, reject) => {
      this.db.transaction((tx) => {
        tx.executeSql(sqlQuery, [],
        (tx, result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
      });
    });
  }

  execQuery(sqlQuery: any) {
    this.db.transaction((tx) => {
      tx.executeSql(sqlQuery, [],
      (tx, result) => {
      }, (error) => {
        console.log(error);
      });
    });
  }

  inputCounterLocalData(response: any) {
    for (let i = 0; i < response.results.length; i++) {

      let insertQuery = 'INSERT INTO ' + this.counterTableName + this.counterColumnList + ' VALUES (' +
      response.results[i].id + ', ' + response.results[i].branch_id + ', "' + 
      response.results[i].counter_name + '", "' +
      response.results[i].trans_date + '", "' + response.results[i].first_address + '", "' +
      response.results[i].last_address + '", "' + response.results[i].phone + '", "' + 
      response.results[i].footer_text + '", "' + response.results[i].latitude + '", "' + 
      response.results[i].longitude + '", "' + response.results[i].active + '", "' +
      response.results[i].created_by + '", "' + response.results[i].updated_by + '", "' +
      response.results[i].created_at + '", "' + response.results[i].updated_at + '")';

      let selectQuery = 'SELECT * FROM ' + this.counterTableName + ' WHERE id = ' + response.results[i].id;
      let deleteQuery = 'DELETE FROM ' + this.counterTableName + ' WHERE id = ' + response.results[i].id;

      this.db.transaction((tx) => {
        tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length === 0) {
            this.execQuery(insertQuery);
          } else {
            this.execQuery(deleteQuery);
            this.execQuery(insertQuery);
          }
        }, (error) => {
          console.log(error);
        });
      });
    }
  }

  inputWarehouseLocalData(response: any) {
    for (let i = 0; i < response.results.length; i++) {

      let insertQuery = 'INSERT INTO ' + this.warehouseTableName + this.warehouseColumnList + ' VALUES (' +
      response.results[i].id + ', ' + response.results[i].counter_id + ', "' +
      response.results[i].warehouse_name + '", ' + response.results[i].warehouse_type_id + ', "' +
      response.results[i].unit_size + '", ' + response.results[i].length_size + ', ' +
      response.results[i].width_size + ', ' + response.results[i].height_size + ', "' +
      response.results[i].active + '", "' + response.results[i].created_by + '", "' +
      response.results[i].updated_by + '", "' + response.results[i].created_at + '", "' +
      response.results[i].updated_at + '")';

      let selectQuery = 'SELECT * FROM ' + this.warehouseTableName + ' WHERE id = ' + response.results[i].id;
      let deleteQuery = 'DELETE FROM ' + this.warehouseTableName + ' WHERE id = ' + response.results[i].id;

      this.db.transaction((tx) => {
        tx.executeSql(selectQuery, [],
        (tx, result) => {
          if(result.rows.length === 0) {
            this.execQuery(insertQuery);
          } else {
            this.execQuery(deleteQuery);
            this.execQuery(insertQuery);
          }
        }, (error) => {
          console.log(error);
        });
      });
    }
  }

  /*relog() {
    Promise.all([
      this.storage.get('user_email'),
      this.storage.get('user_password')
    ])
    .then(([email, password]) => {
      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.loginService.userLogin(email, password)
        .subscribe((response: any) => {
          this.utilService.loadingDismiss();
          if(response.token) {
            this.storage.set('user_token', response.token);
            this.toastCtrl.create({ duration: 2000, message: 'Refresh kembali laman ini.' }).then(t => t.present());
          } else {
            this.utilService.loadingDismiss();
            this.toastCtrl.create({ duration: 2000, message: 'Email atau Password salah!' }).then(t => t.present());
          }
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }*/

}
