import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurposeCreatePage } from './purpose-create.page';

const routes: Routes = [
  {
    path: '',
    component: PurposeCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurposeCreatePageRoutingModule {}
