import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurposeCreatePageRoutingModule } from './purpose-create-routing.module';

import { PurposeCreatePage } from './purpose-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PurposeCreatePageRoutingModule
  ],
  declarations: [PurposeCreatePage]
})
export class PurposeCreatePageModule {}
