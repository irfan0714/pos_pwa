import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PurposeCreatePage } from './purpose-create.page';

describe('PurposeCreatePage', () => {
  let component: PurposeCreatePage;
  let fixture: ComponentFixture<PurposeCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurposeCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PurposeCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
