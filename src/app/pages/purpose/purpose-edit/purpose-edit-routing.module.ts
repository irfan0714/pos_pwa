import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurposeEditPage } from './purpose-edit.page';

const routes: Routes = [
  {
    path: '',
    component: PurposeEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurposeEditPageRoutingModule {}
