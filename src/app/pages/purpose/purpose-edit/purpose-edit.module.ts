import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurposeEditPageRoutingModule } from './purpose-edit-routing.module';

import { PurposeEditPage } from './purpose-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PurposeEditPageRoutingModule
  ],
  declarations: [PurposeEditPage]
})
export class PurposeEditPageModule {}
