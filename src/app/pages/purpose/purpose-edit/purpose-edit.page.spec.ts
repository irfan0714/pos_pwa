import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PurposeEditPage } from './purpose-edit.page';

describe('PurposeEditPage', () => {
  let component: PurposeEditPage;
  let fixture: ComponentFixture<PurposeEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurposeEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PurposeEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
