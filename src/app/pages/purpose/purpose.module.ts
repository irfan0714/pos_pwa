import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurposePageRoutingModule } from './purpose-routing.module';

import { PurposePage } from './purpose.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PurposePageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [PurposePage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PurposePageModule {}
