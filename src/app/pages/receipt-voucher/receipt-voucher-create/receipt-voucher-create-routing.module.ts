import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReceiptVoucherCreatePage } from './receipt-voucher-create.page';

const routes: Routes = [
  {
    path: '',
    component: ReceiptVoucherCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReceiptVoucherCreatePageRoutingModule {}
