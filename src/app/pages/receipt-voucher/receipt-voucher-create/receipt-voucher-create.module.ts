import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReceiptVoucherCreatePageRoutingModule } from './receipt-voucher-create-routing.module';

import { ReceiptVoucherCreatePage } from './receipt-voucher-create.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    ReceiptVoucherCreatePageRoutingModule
  ],
  declarations: [ReceiptVoucherCreatePage]
})
export class ReceiptVoucherCreatePageModule {}
