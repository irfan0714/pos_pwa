import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReceiptVoucherCreatePage } from './receipt-voucher-create.page';

describe('ReceiptVoucherCreatePage', () => {
  let component: ReceiptVoucherCreatePage;
  let fixture: ComponentFixture<ReceiptVoucherCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptVoucherCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReceiptVoucherCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
