import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReceiptVoucherEditPage } from './receipt-voucher-edit.page';

describe('ReceiptVoucherEditPage', () => {
  let component: ReceiptVoucherEditPage;
  let fixture: ComponentFixture<ReceiptVoucherEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptVoucherEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReceiptVoucherEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
