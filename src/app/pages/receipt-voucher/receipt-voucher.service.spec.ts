import { TestBed } from '@angular/core/testing';

import { ReceiptVoucherService } from './receipt-voucher.service';

describe('ReceiptVoucherService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReceiptVoucherService = TestBed.get(ReceiptVoucherService);
    expect(service).toBeTruthy();
  });
});
