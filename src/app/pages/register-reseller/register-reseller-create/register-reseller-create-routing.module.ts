import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterResellerCreatePage } from './register-reseller-create.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterResellerCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterResellerCreatePageRoutingModule {}
