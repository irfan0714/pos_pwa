import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterResellerCreatePageRoutingModule } from './register-reseller-create-routing.module';

import { RegisterResellerCreatePage } from './register-reseller-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterResellerCreatePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RegisterResellerCreatePage]
})
export class RegisterResellerCreatePageModule {}
