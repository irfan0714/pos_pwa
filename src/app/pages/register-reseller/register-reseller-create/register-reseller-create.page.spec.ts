import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegisterResellerCreatePage } from './register-reseller-create.page';

describe('RegisterResellerCreatePage', () => {
  let component: RegisterResellerCreatePage;
  let fixture: ComponentFixture<RegisterResellerCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterResellerCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterResellerCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
