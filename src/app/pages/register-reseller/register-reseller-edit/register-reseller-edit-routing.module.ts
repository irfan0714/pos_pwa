import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterResellerEditPage } from './register-reseller-edit.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterResellerEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterResellerEditPageRoutingModule {}
