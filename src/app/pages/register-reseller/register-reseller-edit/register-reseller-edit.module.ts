import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterResellerEditPageRoutingModule } from './register-reseller-edit-routing.module';

import { RegisterResellerEditPage } from './register-reseller-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterResellerEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RegisterResellerEditPage]
})
export class RegisterResellerEditPageModule {}
