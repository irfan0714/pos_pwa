import { TestBed } from '@angular/core/testing';

import { RegisterResellerService } from './register-reseller.service';

describe('RegisterResellerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegisterResellerService = TestBed.get(RegisterResellerService);
    expect(service).toBeTruthy();
  });
});
