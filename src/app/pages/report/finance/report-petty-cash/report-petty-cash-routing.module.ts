import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportPettyCashPage } from './report-petty-cash.page';

const routes: Routes = [
  {
    path: '',
    component: ReportPettyCashPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportPettyCashPageRoutingModule {}
