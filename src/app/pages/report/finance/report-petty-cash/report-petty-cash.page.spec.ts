import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportPettyCashPage } from './report-petty-cash.page';

describe('ReportPettyCashPage', () => {
  let component: ReportPettyCashPage;
  let fixture: ComponentFixture<ReportPettyCashPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPettyCashPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportPettyCashPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
