import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportProductReceivingPage } from './report-product-receiving.page';

const routes: Routes = [
  {
    path: '',
    component: ReportProductReceivingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportProductReceivingPageRoutingModule {}
