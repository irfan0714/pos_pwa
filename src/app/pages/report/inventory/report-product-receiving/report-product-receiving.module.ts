import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportProductReceivingPageRoutingModule } from './report-product-receiving-routing.module';

import { ReportProductReceivingPage } from './report-product-receiving.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportProductReceivingPageRoutingModule
  ],
  declarations: [ReportProductReceivingPage]
})
export class ReportProductReceivingPageModule {}
