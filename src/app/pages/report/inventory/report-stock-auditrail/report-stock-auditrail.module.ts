import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportStockAuditrailPageRoutingModule } from './report-stock-auditrail-routing.module';

import { ReportStockAuditrailPage } from './report-stock-auditrail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportStockAuditrailPageRoutingModule
  ],
  declarations: [ReportStockAuditrailPage]
})
export class ReportStockAuditrailPageModule {}
