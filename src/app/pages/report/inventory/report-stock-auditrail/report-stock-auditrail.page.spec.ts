import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportStockAuditrailPage } from './report-stock-auditrail.page';

describe('ReportStockAuditrailPage', () => {
  let component: ReportStockAuditrailPage;
  let fixture: ComponentFixture<ReportStockAuditrailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportStockAuditrailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportStockAuditrailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
