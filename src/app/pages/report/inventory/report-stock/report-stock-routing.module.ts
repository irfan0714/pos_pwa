import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportStockPage } from './report-stock.page';

const routes: Routes = [
  {
    path: '',
    component: ReportStockPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportStockPageRoutingModule {}
