import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportStockPage } from './report-stock.page';

describe('ReportStockPage', () => {
  let component: ReportStockPage;
  let fixture: ComponentFixture<ReportStockPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportStockPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportStockPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
