import { Injectable } from '@angular/core';
import { HttpService } from '../../service/http.service';
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private httpService: HttpService) { }

  getReportCashierIndex(params: any): Observable<any> {
    return this.httpService.get('report-cashier', params);
  }

  getReportStockAuditrailIndex(params: any): Observable<any> {
    return this.httpService.get('report-stock-auditrail', params);
  }

  getReportProductRequestIndex(params: any): Observable<any> {
    return this.httpService.get('report-product-request', params);
  }

  getReportProductReceivingIndex(params: any): Observable<any> {
    return this.httpService.get('report-product-receiving', params);
  }

  getReportCashier(rekapPer: any, params: any): Observable<any> {
    let url: any;

    if(rekapPer === '0') {
      url = 'report-cashier/transaction';
    } else if(rekapPer === '1') {
      url = 'report-cashier/transaction-detail';
    } else if(rekapPer === '2') {
      url = 'report-cashier/transaction-product';
    } else if(rekapPer === '3') {
      url = 'report-cashier/transaction-brand';
    } else if(rekapPer === '4') {
      url = 'report-cashier/transaction-prize';
    } else if(rekapPer === '5') {
      url = 'report-cashier/transaction-void';
    } else if(rekapPer === '6') {
      url = 'report-cashier/transaction-sumdate';
    } else {
      url = '';
    }

    return this.httpService.get(url, params);
  }

  getReportStock(params: any): Observable<any> {
    return this.httpService.get('report-stock', params);
  }

  getReportStockAuditrail(params: any): Observable<any> {
    return this.httpService.get('generate-report-stock-auditrail', params);
  }

  getReportProductRequest(params: any): Observable<any> {
    return this.httpService.get('generate-report-product-request', params);
  }

  getReportProductReceiving(params: any): Observable<any> {
    return this.httpService.get('generate-report-product-receiving', params);
  }

  getReportProductRequestDetail(params: any): Observable<any> {
    return this.httpService.get('generate-report-product-request-detail', params);
  }

  getReportProductReceivingDetail(params: any): Observable<any> {
    return this.httpService.get('generate-report-product-receiving-detail', params);
  }

  getMutationStock(params: any): Observable<any> {
    return this.httpService.get('stock-mutation-get', params);
  }

  saveVoidTransaction(data: any): Observable<any> {
    return this.httpService.post('save-void-transaction', data);
  }

  getSalesByReceiptNumber(params: any, receiptNo: any): Observable<any> {
    return this.httpService.get(`get-sales-transaction/${receiptNo}`, params);
  }
}
