import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportCashierPage } from './report-cashier.page';

const routes: Routes = [
  {
    path: '',
    component: ReportCashierPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportCashierPageRoutingModule {}
