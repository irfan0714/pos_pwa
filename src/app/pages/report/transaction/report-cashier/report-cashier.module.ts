import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportCashierPageRoutingModule } from './report-cashier-routing.module';

import { ReportCashierPage } from './report-cashier.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ReportCashierPageRoutingModule
  ],
  declarations: [ReportCashierPage]
})
export class ReportCashierPageModule {}
