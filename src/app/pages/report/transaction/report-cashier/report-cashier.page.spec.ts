import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportCashierPage } from './report-cashier.page';

describe('ReportCashierPage', () => {
  let component: ReportCashierPage;
  let fixture: ComponentFixture<ReportCashierPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportCashierPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportCashierPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
