import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportReprintStruckPageRoutingModule } from './report-reprint-struck-routing.module';

import { ReportReprintStruckPage } from './report-reprint-struck.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ReportReprintStruckPageRoutingModule
  ],
  declarations: [ReportReprintStruckPage]
})
export class ReportReprintStruckPageModule {}
