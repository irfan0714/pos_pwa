import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportReprintStruckPage } from './report-reprint-struck.page';

describe('ReportReprintStruckPage', () => {
  let component: ReportReprintStruckPage;
  let fixture: ComponentFixture<ReportReprintStruckPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportReprintStruckPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportReprintStruckPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
