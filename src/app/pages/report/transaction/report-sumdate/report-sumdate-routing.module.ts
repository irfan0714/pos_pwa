import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportSumdatePage } from './report-sumdate.page';

const routes: Routes = [
  {
    path: '',
    component: ReportSumdatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportSumdatePageRoutingModule {}
