import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportSumdatePageRoutingModule } from './report-sumdate-routing.module';

import { ReportSumdatePage } from './report-sumdate.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportSumdatePageRoutingModule
  ],
  declarations: [ReportSumdatePage]
})
export class ReportSumdatePageModule {}
