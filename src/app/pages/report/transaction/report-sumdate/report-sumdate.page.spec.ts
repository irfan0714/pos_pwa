import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportSumdatePage } from './report-sumdate.page';

describe('ReportSumdatePage', () => {
  let component: ReportSumdatePage;
  let fixture: ComponentFixture<ReportSumdatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSumdatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportSumdatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
