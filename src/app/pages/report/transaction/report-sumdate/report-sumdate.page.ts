import { Component, OnInit } from '@angular/core';
import { ReportService } from '../../report.service';
import { Storage } from '@ionic/storage';
import { UtilService } from '../../../../service/util.service';
import { ToastController } from '@ionic/angular';
import { UserProfile } from '../../../../models/user-profile.model';
import { RoleAccess } from '../../../../models/role-access.model';
import { UserData } from '../../../../providers/user-data';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-report-sumdate',
  templateUrl: './report-sumdate.page.html',
  styleUrls: ['./report-sumdate.page.scss'],
})
export class ReportSumdatePage implements OnInit {

  userProfile: UserProfile = new UserProfile();
  reportData: any=[];
  idCounter: any;
  counterData: any[] = [];
  periode: any;
  token: any;
  rekapPer: any='6';
  userAccess: any;
  roleAccess = new RoleAccess();
  fileName= 'ReportPertanggal.xlsx';

  constructor(
    private storage: Storage,
    private reportService: ReportService,
    private utilService: UtilService,
    private toastCtrl: ToastController,
    private router: Router,
    private userDataProvider: UserData
    ) { }

  ngOnInit() {
    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile'),
      this.storage.get('user_menu_access')
    ])
    .then(([token, profile, access]) => {
      if(token) {
        this.token = token;
        this.userAccess = access;
        this.utilService.loadingPresent('Harap tunggu...')
        .then(() => {
          this.userProfile = new UserProfile(profile);
          let counterId = this.userProfile.counter_id !== 0 ? this.userProfile.counter_id : this.userProfile.counter_id_list;
          let options = { 
            "token": this.token,
            "counter_id": counterId
          };
          this.reportService.getReportCashierIndex(options).subscribe((response) => {
            this.utilService.loadingDismiss();
            this.counterData = response.results.counter_data;
            this.roleAccess = this.userDataProvider.checkAccess(this.router.url, this.userAccess, this.userProfile);
          }, () => {
            this.utilService.loadingDismiss();
            this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
          });
        });
      }
    });
  }

  getReport() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let dateConvert = this.utilService.convertDate(this.periode);
      let newTransDate = dateConvert.years + '-' + dateConvert.months;
      let options = {
        "token": this.token,
        "periode": newTransDate,
        "counter_id": this.idCounter,
      };
      this.reportService.getReportCashier(this.rekapPer, options).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 200) {
          this.reportData = response.results;
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        }
      }, (err) => {
        this.utilService.loadingDismiss();
        console.log(err);
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });

    }, (err) => {
      this.utilService.loadingDismiss();
      console.log(err);
      this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
    });
  }

  exportToExcel() {
    /* table id is passed over here */   
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }
}
