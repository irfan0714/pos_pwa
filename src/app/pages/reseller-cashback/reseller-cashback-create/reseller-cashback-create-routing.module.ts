import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResellerCashbackCreatePage } from './reseller-cashback-create.page';

const routes: Routes = [
  {
    path: '',
    component: ResellerCashbackCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResellerCashbackCreatePageRoutingModule {}
