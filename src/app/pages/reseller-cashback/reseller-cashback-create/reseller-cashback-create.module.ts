import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResellerCashbackCreatePageRoutingModule } from './reseller-cashback-create-routing.module';

import { ResellerCashbackCreatePage } from './reseller-cashback-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ResellerCashbackCreatePageRoutingModule
  ],
  declarations: [ResellerCashbackCreatePage]
})
export class ResellerCashbackCreatePageModule {}
