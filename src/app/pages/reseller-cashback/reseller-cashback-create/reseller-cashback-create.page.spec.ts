import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResellerCashbackCreatePage } from './reseller-cashback-create.page';

describe('ResellerCashbackCreatePage', () => {
  let component: ResellerCashbackCreatePage;
  let fixture: ComponentFixture<ResellerCashbackCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResellerCashbackCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResellerCashbackCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
