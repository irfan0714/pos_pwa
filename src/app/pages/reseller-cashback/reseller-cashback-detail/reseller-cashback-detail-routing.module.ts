import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResellerCashbackDetailPage } from './reseller-cashback-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ResellerCashbackDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResellerCashbackDetailPageRoutingModule {}
