import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResellerCashbackDetailPageRoutingModule } from './reseller-cashback-detail-routing.module';

import { ResellerCashbackDetailPage } from './reseller-cashback-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ResellerCashbackDetailPageRoutingModule
  ],
  declarations: [ResellerCashbackDetailPage]
})
export class ResellerCashbackDetailPageModule {}
