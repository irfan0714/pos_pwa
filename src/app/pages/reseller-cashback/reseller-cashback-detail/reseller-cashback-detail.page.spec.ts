import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResellerCashbackDetailPage } from './reseller-cashback-detail.page';

describe('ResellerCashbackDetailPage', () => {
  let component: ResellerCashbackDetailPage;
  let fixture: ComponentFixture<ResellerCashbackDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResellerCashbackDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResellerCashbackDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
