import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResellerCashbackPageRoutingModule } from './reseller-cashback-routing.module';

import { ResellerCashbackPage } from './reseller-cashback.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResellerCashbackPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [ResellerCashbackPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ResellerCashbackPageModule {}
