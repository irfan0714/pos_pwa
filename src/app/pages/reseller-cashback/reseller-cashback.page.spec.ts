import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResellerCashbackPage } from './reseller-cashback.page';

describe('ResellerCashbackPage', () => {
  let component: ResellerCashbackPage;
  let fixture: ComponentFixture<ResellerCashbackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResellerCashbackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResellerCashbackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
