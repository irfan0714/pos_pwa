import { TestBed } from '@angular/core/testing';

import { ResellerCashbackService } from './reseller-cashback.service';

describe('ResellerCashbackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResellerCashbackService = TestBed.get(ResellerCashbackService);
    expect(service).toBeTruthy();
  });
});
