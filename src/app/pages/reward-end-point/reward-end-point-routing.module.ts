import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RewardEndPointPage } from './reward-end-point.page';

const routes: Routes = [
  {
    path: '',
    component: RewardEndPointPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RewardEndPointPageRoutingModule {}
