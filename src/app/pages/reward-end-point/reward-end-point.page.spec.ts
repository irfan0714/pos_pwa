import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RewardEndPointPage } from './reward-end-point.page';

describe('RewardEndPointPage', () => {
  let component: RewardEndPointPage;
  let fixture: ComponentFixture<RewardEndPointPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardEndPointPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RewardEndPointPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
