import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleAccessPage } from './role-access.page';

const routes: Routes = [
  {
    path: '',
    component: RoleAccessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleAccessPageRoutingModule {}
