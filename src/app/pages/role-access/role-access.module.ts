import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleAccessPageRoutingModule } from './role-access-routing.module';

import { RoleAccessPage } from './role-access.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RoleAccessPageRoutingModule
  ],
  declarations: [RoleAccessPage]
})
export class RoleAccessPageModule {}
