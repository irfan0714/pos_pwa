import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoleAccessPage } from './role-access.page';

describe('RoleAccessPage', () => {
  let component: RoleAccessPage;
  let fixture: ComponentFixture<RoleAccessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleAccessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoleAccessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
