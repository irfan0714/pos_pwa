import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../service/util.service';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';
import { UserProfile } from '../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { RoleAccessService } from './role-access.service';
import { RoleAccess } from '../../models/role-access.model';

@Component({
  selector: 'app-role-access',
  templateUrl: './role-access.page.html',
  styleUrls: ['./role-access.page.scss'],
})
export class RoleAccessPage implements OnInit {

  token: any;
  roleId: any;
  roleName: any;
  menuData: any;
  roleData: any;
  roleAccessData: any;
  userProfile: UserProfile = new UserProfile();

  formRoleAccessEdit: FormGroup;

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private roleAccessService: RoleAccessService
  ) { }

  ngOnInit() {
    this.buildFormRoleAccessEdit();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getData();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  buildFormRoleAccessEdit() {
    this.formRoleAccessEdit = this.fb.group({
      roleAccess: this.fb.array([])
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.roleId = snapshot.roleId;
      this.roleName = snapshot.roleName;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        this.roleAccessService.getRoleAccessforEdit(this.roleId, { "token": this.token }).subscribe((response) => {
          this.utilService.loadingDismiss();
          this.menuData = response.results.menu_data;
          this.roleData = response.results.role_data;
          this.roleAccessData = response.results.role_access_data;
          this.addRoleAccessField();
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  addRoleAccessField() {
    if(this.menuData.length > 0) {
      for(let i = 0; i < this.menuData.length; i++) {
        let viewValue, readValue, readAllValue, createValue,
        updateValue, deleteValue, approveValue, downloadValue = false;

        for(let x = 0; x < this.roleAccessData.length; x++) {
          if(this.roleAccessData[x].menu_id === this.menuData[i].id) {
            if(this.roleAccessData[x].view === '1') { viewValue = true; };
            if(this.roleAccessData[x].read === '1') { readValue = true; };
            if(this.roleAccessData[x].read_all === '1') { readAllValue = true; };
            if(this.roleAccessData[x].create === '1') { createValue = true; };
            if(this.roleAccessData[x].update === '1') { updateValue = true; };
            if(this.roleAccessData[x].delete === '1') { deleteValue = true; };
            if(this.roleAccessData[x].approve === '1') { approveValue = true; };
            if(this.roleAccessData[x].download === '1') { downloadValue = true; };
          }
        }

        let menuName = this.menuData[i].menu_name;

        const accessRole = this.fb.group({
          menuId: [this.menuData[i].id],
          menuName: [menuName],
          view: [viewValue],
          read: [readValue],
          readAll: [readAllValue],
          create: [createValue],
          update: [updateValue],
          delete: [deleteValue],
          approve: [approveValue],
          download: [downloadValue]
        });

        this.getRoleAccessArray.push(accessRole);
      }
    }
  }

  get getRoleAccessArray() {
    return (<FormArray>this.formRoleAccessEdit.get('roleAccess'));
  }

  onSubmitForm() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      const formRoleAccess = this.formRoleAccessEdit.value;
      let arrRoleId: any = [], arrMenuId: any = [], arrViewVal: any = [], arrReadVal: any = [],
      arrReadAllVal: any = [], arrCreateVal: any = [], arrUpdateVal: any = [], arrDeleteVal: any = [],
      arrApproveVal: any = [], arrDownloadVal: any = [];

      for(let x = 0; x < formRoleAccess.roleAccess.length; x++) {
        let viewValue: number = 0, readValue: number = 0, readAllValue: number = 0, createValue: number = 0,
        updateValue: number = 0, deleteValue: number = 0, approveValue: number = 0, downloadValue: number = 0;

        if(formRoleAccess.roleAccess[x].view === true) { viewValue = 1; };
        if(formRoleAccess.roleAccess[x].read === true) { readValue = 1; };
        if(formRoleAccess.roleAccess[x].readAll === true) { readAllValue = 1; };
        if(formRoleAccess.roleAccess[x].create === true) { createValue = 1; };
        if(formRoleAccess.roleAccess[x].update === true) { updateValue = 1; };
        if(formRoleAccess.roleAccess[x].delete === true) { deleteValue = 1; };
        if(formRoleAccess.roleAccess[x].approve === true) { approveValue = 1; };
        if(formRoleAccess.roleAccess[x].download === true) { downloadValue = 1; };

        arrRoleId[x] = this.roleId;
        arrMenuId[x] = formRoleAccess.roleAccess[x].menuId;
        arrViewVal[x] = viewValue;
        arrReadVal[x] = readValue;
        arrReadAllVal[x] = readAllValue;
        arrCreateVal[x] = createValue;
        arrUpdateVal[x] = updateValue;
        arrDeleteVal[x] = deleteValue;
        arrApproveVal[x] = approveValue;
        arrDownloadVal[x] = downloadValue;
      }

      const roleAccess = new RoleAccess();
      roleAccess.role_id = arrRoleId;
      roleAccess.menu_id = arrMenuId;
      roleAccess.view = arrViewVal;
      roleAccess.read = arrReadVal;
      roleAccess.read_all = arrReadAllVal;
      roleAccess.create = arrCreateVal;
      roleAccess.update = arrUpdateVal;
      roleAccess.delete = arrDeleteVal;
      roleAccess.approve = arrApproveVal;
      roleAccess.download = arrDownloadVal;

      let action = this.roleAccessData.length === 0 ? 'post' : 'put';

      this.roleAccessService.processRoleAccess(this.roleId, roleAccess, action).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmUpdate();
        } else {
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/role']);;
          }
        }
      ]
    });

    await alert.present();
  }

}
