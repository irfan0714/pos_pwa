import { TestBed } from '@angular/core/testing';

import { RoleAccessService } from './role-access.service';

describe('RoleAccessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoleAccessService = TestBed.get(RoleAccessService);
    expect(service).toBeTruthy();
  });
});
