import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleCreatePageRoutingModule } from './role-create-routing.module';

import { RoleCreatePage } from './role-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RoleCreatePageRoutingModule
  ],
  declarations: [RoleCreatePage]
})
export class RoleCreatePageModule {}
