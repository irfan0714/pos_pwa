import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SatuanPage } from './satuan.page';

const routes: Routes = [
  {
    path: '',
    component: SatuanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SatuanPageRoutingModule {}
