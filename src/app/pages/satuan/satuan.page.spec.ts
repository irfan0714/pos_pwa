import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SatuanPage } from './satuan.page';

describe('SatuanPage', () => {
  let component: SatuanPage;
  let fixture: ComponentFixture<SatuanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SatuanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
