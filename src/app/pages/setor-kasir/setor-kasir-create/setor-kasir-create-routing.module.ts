import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetorKasirCreatePage } from './setor-kasir-create.page';

const routes: Routes = [
  {
    path: '',
    component: SetorKasirCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetorKasirCreatePageRoutingModule {}
