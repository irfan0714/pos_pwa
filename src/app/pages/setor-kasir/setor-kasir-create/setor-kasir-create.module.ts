import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetorKasirCreatePageRoutingModule } from './setor-kasir-create-routing.module';

import { SetorKasirCreatePage } from './setor-kasir-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SetorKasirCreatePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SetorKasirCreatePage]
})
export class SetorKasirCreatePageModule {}
