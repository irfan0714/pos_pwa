import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetorKasirCreatePage } from './setor-kasir-create.page';

describe('SetorKasirCreatePage', () => {
  let component: SetorKasirCreatePage;
  let fixture: ComponentFixture<SetorKasirCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetorKasirCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetorKasirCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
