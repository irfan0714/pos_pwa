import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetorKasirEditPage } from './setor-kasir-edit.page';

const routes: Routes = [
  {
    path: '',
    component: SetorKasirEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetorKasirEditPageRoutingModule {}
