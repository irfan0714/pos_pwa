import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetorKasirEditPageRoutingModule } from './setor-kasir-edit-routing.module';

import { SetorKasirEditPage } from './setor-kasir-edit.page';

@NgModule({
  imports: [  
    CommonModule,
    FormsModule,
    IonicModule,
    SetorKasirEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SetorKasirEditPage]
})
export class SetorKasirEditPageModule {}
