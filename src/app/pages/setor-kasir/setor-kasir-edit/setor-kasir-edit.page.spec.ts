import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetorKasirEditPage } from './setor-kasir-edit.page';

describe('SetorKasirEditPage', () => {
  let component: SetorKasirEditPage;
  let fixture: ComponentFixture<SetorKasirEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetorKasirEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetorKasirEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
