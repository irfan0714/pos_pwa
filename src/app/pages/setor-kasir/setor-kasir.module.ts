import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetorKasirPageRoutingModule } from './setor-kasir-routing.module';

import { SetorKasirPage } from './setor-kasir.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SetorKasirPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [SetorKasirPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SetorKasirPageModule {}
