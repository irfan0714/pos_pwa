import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetorKasirPage } from './setor-kasir.page';

describe('SetorKasirPage', () => {
  let component: SetorKasirPage;
  let fixture: ComponentFixture<SetorKasirPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetorKasirPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetorKasirPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
