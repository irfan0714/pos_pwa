import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StockOpnameCreatePage } from './stock-opname-create.page';

const routes: Routes = [
  {
    path: '',
    component: StockOpnameCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StockOpnameCreatePageRoutingModule {}
