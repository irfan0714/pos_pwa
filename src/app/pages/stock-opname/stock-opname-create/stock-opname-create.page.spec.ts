import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StockOpnameCreatePage } from './stock-opname-create.page';

describe('StockOpnameCreatePage', () => {
  let component: StockOpnameCreatePage;
  let fixture: ComponentFixture<StockOpnameCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockOpnameCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StockOpnameCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
