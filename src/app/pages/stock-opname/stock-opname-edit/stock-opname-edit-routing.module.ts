import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StockOpnameEditPage } from './stock-opname-edit.page';

const routes: Routes = [
  {
    path: '',
    component: StockOpnameEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StockOpnameEditPageRoutingModule {}
