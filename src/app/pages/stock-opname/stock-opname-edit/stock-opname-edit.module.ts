import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StockOpnameEditPageRoutingModule } from './stock-opname-edit-routing.module';

import { StockOpnameEditPage } from './stock-opname-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    StockOpnameEditPageRoutingModule
  ],
  declarations: [StockOpnameEditPage]
})
export class StockOpnameEditPageModule {}
