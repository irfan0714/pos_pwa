import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StockOpnameEditPage } from './stock-opname-edit.page';

describe('StockOpnameEditPage', () => {
  let component: StockOpnameEditPage;
  let fixture: ComponentFixture<StockOpnameEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockOpnameEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StockOpnameEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
