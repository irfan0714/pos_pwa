import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StockOpnamePageRoutingModule } from './stock-opname-routing.module';

import { StockOpnamePage } from './stock-opname.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StockOpnamePageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [StockOpnamePage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class StockOpnamePageModule {}
