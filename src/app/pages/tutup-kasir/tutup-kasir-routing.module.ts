import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TutupKasirPage } from './tutup-kasir.page';

const routes: Routes = [
  {
    path: '',
    component: TutupKasirPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TutupKasirPageRoutingModule {}
