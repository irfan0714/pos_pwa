import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TutupKasirPage } from './tutup-kasir.page';

describe('TutupKasirPage', () => {
  let component: TutupKasirPage;
  let fixture: ComponentFixture<TutupKasirPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutupKasirPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TutupKasirPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
