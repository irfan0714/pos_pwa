import { TestBed } from '@angular/core/testing';

import { TutupKasirService } from './tutup-kasir.service';

describe('TutupKasirService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TutupKasirService = TestBed.get(TutupKasirService);
    expect(service).toBeTruthy();
  });
});
