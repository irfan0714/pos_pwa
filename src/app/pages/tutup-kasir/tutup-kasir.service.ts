import { Injectable } from '@angular/core';
import { HttpService } from '../../service/http.service';
import { TutupKasir } from '../../models/tutup-kasir.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TutupKasirService {

  constructor(private httpService: HttpService) { }

  addTutupKasir(data: TutupKasir): Observable<any> {
    return this.httpService.post('clean-transaction', data);
  }
}
