import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../../service/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../../../models/user-profile.model';
import { Storage } from '@ionic/storage';
import { UserService } from '../user.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.page.html',
  styleUrls: ['./user-edit.page.scss'],
})
export class UserEditPage implements OnInit {

  formUserEdit: FormGroup;
  token: any;
  userProfile: UserProfile = new UserProfile();
  userId: any;
  counterData: any[] = [];
  branchData: any[] = [];
  roleData: any[] = [];
  accessId: any;

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private utilService: UtilService,
    private alertController: AlertController,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private userService: UserService) { }

  ngOnInit() {
    this.buildFormUserEdit();

    Promise.all([
      this.storage.get('user_token'),
      this.storage.get('user_profile')
    ])
    .then(([token, profile]) => {
      if(token) {
        this.token = token;
        this.userProfile = new UserProfile(profile);
        this.getData();
      } else {
        this.toastCtrl.create({ duration: 2000, message: 'Silahkan login terlebih dahulu.' }).then(t => t.present());
        this.navCtrl.navigateForward(['/login']);
      }
    });
  }

  buildFormUserEdit() {
    this.formUserEdit = this.fb.group({
      roleId: [], counterId: [], username: [], name: [], email: [], branchId: [], accessId: []
    });
  }

  getData() {
    this.activeRoute.queryParams.subscribe((snapshot) => {
      this.userId = snapshot.userId;

      this.utilService.loadingPresent('Harap tunggu...')
      .then(() => {
        let options = { "token": this.token };
        forkJoin([
          this.userService.getUserbyId(this.userId, options),
          this.userService.getRole(options),
          this.userService.getCounter(options)
        ]).subscribe(([user, role, counter]) => {
          this.utilService.loadingDismiss();
          if(role.results.length > 0) { this.roleData = role.results; }
          if(counter.results.length > 0) { this.counterData = counter.results; }
          if(user.results.branch_data.length > 0) { this.branchData = user.results.branch_data; }
          
          if(user.results.user_data.branch_id === 0 && user.results.user_data.counter_id !== 0) {
            this.accessId = '2';
          }
          if(user.results.user_data.branch_id !== 0 && user.results.user_data.counter_id === 0) {
            this.accessId = '1';
          }
          if(user.results.user_data.branch_id === 0 && user.results.user_data.counter_id === 0) {
            this.accessId = '0';
          }

          this.formUserEdit = this.fb.group({
            roleId: [user.results.user_data.role_id, Validators.required],
            counterId: [user.results.user_data.counter_id === 0 ? null : user.results.user_data.counter_id],
            username: [user.results.user_data.username],
            name: [user.results.user_data.name, Validators.required],
            email: [user.results.user_data.email],
            branchId: [user.results.user_data.branch_id === 0 ? null : user.results.user_data.branch_id],
            accessId: [this.accessId]
          });
        }, () => {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
        });
      });
    });
  }

  updateData() {
    this.utilService.loadingPresent('Harap tunggu...')
    .then(() => {
      let counterId: number = 0;
      let branchId: number = 0;
      const formUser = this.formUserEdit.getRawValue();

      if(this.accessId === '1') {
        branchId = formUser.branchId;
      }

      if(this.accessId === '2') {
        counterId = formUser.counterId;
      }
      const userData = new UserProfile();
      userData.role_id = formUser.roleId;
      userData.counter_id = counterId;
      userData.branch_id = branchId;
      userData.name = formUser.name;
      userData.updated_by = this.userProfile.username;

      this.userService.updateUser(this.userId, userData).subscribe((response) => {
        this.utilService.loadingDismiss();
        if(response.status.code === 201) {
          this.showConfirmUpdate();
        } else {
          this.utilService.loadingDismiss();
          this.toastCtrl.create({ duration: 2000, message: 'Terdapat Error' }).then(t => t.present());
        }
      }, () => {
        this.utilService.loadingDismiss();
        this.toastCtrl.create({ duration: 2000, message: 'Gagal terhubung ke server' }).then(t => t.present());
      });
    });
  }

  async showConfirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Notification',
      cssClass:'custom-alert-class',
      message: 'Data sudah tersimpan!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.navigateForward(['/user']);;
          }
        }
      ]
    });

    await alert.present();
  }

  checkAccess() {
    const formUser = this.formUserEdit.getRawValue();
    this.accessId = formUser.accessId;

    this.formUserEdit = this.fb.group({
      roleId: [formUser.roleId, Validators.required],
      counterId: this.accessId === '2' ? [formUser.counterId, Validators.required] : [formUser.counterId],
      username: [formUser.username],
      name: [formUser.name, Validators.required],
      email: [formUser.email],
      branchId: this.accessId === '1' ? [formUser.branchId, Validators.required] : [formUser.branchId],
      accessId: [this.accessId]
    });
  }

}
