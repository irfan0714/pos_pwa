import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../../service/http.service';
import { UserProfile } from '../../models/user-profile.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpService: HttpService) { }

  getUser(params: any): Observable<any> {
    return this.httpService.get('user', params);
  }

  getUserbyId(userId: any, params: any): Observable<any> {
    return this.httpService.get(`user/${userId}/edit`, params);
  }

  getRole(params: any): Observable<any> {
    return this.httpService.get('role-get-all', params);
  }

  getCounter(params: any): Observable<any> {
    return this.httpService.get('counter-all', params);
  }

  pullUser(data: UserProfile): Observable<any> {
    return this.httpService.post('user/pull', data);
  }

  updateUser(userId: any, data: UserProfile): Observable<any> {
    return this.httpService.put(`user/${userId}`, data);
  }
}
