import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VoucherCreatePage } from './voucher-create.page';

const routes: Routes = [
  {
    path: '',
    component: VoucherCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VoucherCreatePageRoutingModule {}
