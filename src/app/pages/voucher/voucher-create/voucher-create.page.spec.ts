import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VoucherCreatePage } from './voucher-create.page';

describe('VoucherCreatePage', () => {
  let component: VoucherCreatePage;
  let fixture: ComponentFixture<VoucherCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VoucherCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
