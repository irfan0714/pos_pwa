import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VoucherEditPage } from './voucher-edit.page';

const routes: Routes = [
  {
    path: '',
    component: VoucherEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VoucherEditPageRoutingModule {}
