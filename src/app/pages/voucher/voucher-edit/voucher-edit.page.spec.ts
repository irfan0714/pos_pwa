import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VoucherEditPage } from './voucher-edit.page';

describe('VoucherEditPage', () => {
  let component: VoucherEditPage;
  let fixture: ComponentFixture<VoucherEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VoucherEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
