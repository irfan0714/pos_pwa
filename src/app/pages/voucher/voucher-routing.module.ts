import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VoucherPage } from './voucher.page';

const routes: Routes = [
  {
    path: '',
    component: VoucherPage
  },
  {
    path: 'voucher-create',
    loadChildren: () => import('./voucher-create/voucher-create.module').then( m => m.VoucherCreatePageModule)
  },
  {
    path: 'voucher-edit',
    loadChildren: () => import('./voucher-edit/voucher-edit.module').then( m => m.VoucherEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VoucherPageRoutingModule {}
