import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VoucherPageRoutingModule } from './voucher-routing.module';

import { VoucherPage } from './voucher.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VoucherPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [VoucherPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class VoucherPageModule {}
