import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WarehouseMutationInDetailPage } from './warehouse-mutation-in-detail.page';

const routes: Routes = [
  {
    path: '',
    component: WarehouseMutationInDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WarehouseMutationInDetailPageRoutingModule {}
