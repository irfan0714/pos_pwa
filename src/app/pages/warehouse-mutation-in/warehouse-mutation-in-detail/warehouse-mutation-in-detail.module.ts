import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WarehouseMutationInDetailPageRoutingModule } from './warehouse-mutation-in-detail-routing.module';

import { WarehouseMutationInDetailPage } from './warehouse-mutation-in-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    WarehouseMutationInDetailPageRoutingModule
  ],
  declarations: [WarehouseMutationInDetailPage]
})
export class WarehouseMutationInDetailPageModule {}
