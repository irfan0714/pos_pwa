import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WarehouseMutationInDetailPage } from './warehouse-mutation-in-detail.page';

describe('WarehouseMutationInDetailPage', () => {
  let component: WarehouseMutationInDetailPage;
  let fixture: ComponentFixture<WarehouseMutationInDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseMutationInDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WarehouseMutationInDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
