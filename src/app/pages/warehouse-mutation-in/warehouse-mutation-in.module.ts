import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WarehouseMutationInPageRoutingModule } from './warehouse-mutation-in-routing.module';

import { WarehouseMutationInPage } from './warehouse-mutation-in.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WarehouseMutationInPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [WarehouseMutationInPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class WarehouseMutationInPageModule {}
