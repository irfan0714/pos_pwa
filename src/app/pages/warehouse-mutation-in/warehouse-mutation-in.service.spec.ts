import { TestBed } from '@angular/core/testing';

import { WarehouseMutationInService } from './warehouse-mutation-in.service';

describe('WarehouseMutationInService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarehouseMutationInService = TestBed.get(WarehouseMutationInService);
    expect(service).toBeTruthy();
  });
});
