import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WarehouseMutationOutCreatePage } from './warehouse-mutation-out-create.page';

const routes: Routes = [
  {
    path: '',
    component: WarehouseMutationOutCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WarehouseMutationOutCreatePageRoutingModule {}
