import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WarehouseMutationOutCreatePageRoutingModule } from './warehouse-mutation-out-create-routing.module';

import { WarehouseMutationOutCreatePage } from './warehouse-mutation-out-create.page';

import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    WarehouseMutationOutCreatePageRoutingModule
  ],
  declarations: [WarehouseMutationOutCreatePage]
})
export class WarehouseMutationOutCreatePageModule {}
