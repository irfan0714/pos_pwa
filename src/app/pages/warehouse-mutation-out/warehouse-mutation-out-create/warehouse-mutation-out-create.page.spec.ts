import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WarehouseMutationOutCreatePage } from './warehouse-mutation-out-create.page';

describe('WarehouseMutationOutCreatePage', () => {
  let component: WarehouseMutationOutCreatePage;
  let fixture: ComponentFixture<WarehouseMutationOutCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseMutationOutCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WarehouseMutationOutCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
