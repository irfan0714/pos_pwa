import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WarehouseMutationOutEditPage } from './warehouse-mutation-out-edit.page';

const routes: Routes = [
  {
    path: '',
    component: WarehouseMutationOutEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WarehouseMutationOutEditPageRoutingModule {}
