import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WarehouseMutationOutEditPageRoutingModule } from './warehouse-mutation-out-edit-routing.module';

import { WarehouseMutationOutEditPage } from './warehouse-mutation-out-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    WarehouseMutationOutEditPageRoutingModule
  ],
  declarations: [WarehouseMutationOutEditPage]
})
export class WarehouseMutationOutEditPageModule {}
