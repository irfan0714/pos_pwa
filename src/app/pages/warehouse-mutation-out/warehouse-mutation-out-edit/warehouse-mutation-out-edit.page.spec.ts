import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WarehouseMutationOutEditPage } from './warehouse-mutation-out-edit.page';

describe('WarehouseMutationOutEditPage', () => {
  let component: WarehouseMutationOutEditPage;
  let fixture: ComponentFixture<WarehouseMutationOutEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseMutationOutEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WarehouseMutationOutEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
