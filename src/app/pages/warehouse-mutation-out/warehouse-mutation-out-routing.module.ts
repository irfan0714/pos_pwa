import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WarehouseMutationOutPage } from './warehouse-mutation-out.page';

const routes: Routes = [
  {
    path: '',
    component: WarehouseMutationOutPage
  },
  {
    path: 'warehouse-mutation-out-create',
    loadChildren: () => import('./warehouse-mutation-out-create/warehouse-mutation-out-create.module').then( m => m.WarehouseMutationOutCreatePageModule)
  },
  {
    path: 'warehouse-mutation-out-edit',
    loadChildren: () => import('./warehouse-mutation-out-edit/warehouse-mutation-out-edit.module').then( m => m.WarehouseMutationOutEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WarehouseMutationOutPageRoutingModule {}
