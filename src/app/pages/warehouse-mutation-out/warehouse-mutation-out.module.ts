import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WarehouseMutationOutPageRoutingModule } from './warehouse-mutation-out-routing.module';

import { WarehouseMutationOutPage } from './warehouse-mutation-out.page';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WarehouseMutationOutPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [WarehouseMutationOutPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class WarehouseMutationOutPageModule {}
