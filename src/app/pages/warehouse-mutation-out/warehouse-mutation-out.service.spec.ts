import { TestBed } from '@angular/core/testing';

import { WarehouseMutationOutService } from './warehouse-mutation-out.service';

describe('WarehouseMutationOutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarehouseMutationOutService = TestBed.get(WarehouseMutationOutService);
    expect(service).toBeTruthy();
  });
});
