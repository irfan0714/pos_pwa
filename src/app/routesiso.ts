const routesiso = [
    {
      path: 'payment-voucher',
      loadChildren: () => import('./pages/payment-voucher/payment-voucher.module').then( m => m.PaymentVoucherPageModule)
    },
    {
      path: 'petty-cash',
      loadChildren: () => import('./pages/petty-cash/petty-cash.module').then( m => m.PettyCashPageModule)
    },
    {
      path: 'petty-cash-category',
      loadChildren: () => import('./pages/petty-cash-category/petty-cash-category.module').then( m => m.PettyCashCategoryPageModule)
    }
]
export default routesiso