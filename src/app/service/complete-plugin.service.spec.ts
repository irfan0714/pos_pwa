import { TestBed } from '@angular/core/testing';

import { CompletePluginService } from './complete-plugin.service';

describe('CompletePluginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompletePluginService = TestBed.get(CompletePluginService);
    expect(service).toBeTruthy();
  });
});
