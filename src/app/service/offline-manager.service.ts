import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { UtilService } from '../service/util.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OfflineManagerService {

  baseUrl: any;
  db: any;

  constructor(
    private http: HttpClient,
    private utilService: UtilService,
    private toastCtrl: ToastController
  ) {
    this.baseUrl = environment.apiUrl;
    this.openDB();
  }

  openDB() {
    this.db = (<any>window).openDatabase('db_pos', '1.0', 'POS Database', 2 * 1024 * 1024);
  }

  checkForEvents() {
    let selectQuery = 'SELECT * FROM offline_store WHERE status = "0"';
    this.db.transaction((tx) => {
      tx.executeSql(selectQuery, [],
      (tx, result) => {
        if(result.rows.length > 0) {
          for(let i = 0; i < result.rows.length; i++) {
            let column: string = result.rows[i].column_name;
            let splitColumn = column.split(';');
            //
          }
        } else {
          //
        }
      }, (error) => {
        console.log(error);
      });
    });
  }
}
