import { TestBed } from '@angular/core/testing';

import { StockManagerService } from './stock-manager.service';

describe('StockManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockManagerService = TestBed.get(StockManagerService);
    expect(service).toBeTruthy();
  });
});
