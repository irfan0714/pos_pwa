import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Storage } from '@ionic/storage';
import { UtilService } from '../service/util.service';

@Injectable({
  providedIn: 'root'
})
export class StockManagerService {

  baseUrl: any;
  token: any;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private utilService: UtilService,
  ) {
    this.baseUrl = environment.apiUrl;
  }

  stockManagementPhase1(token: any, year: any, month: any, warehouseId: any, productId: any, 
    mutationType: any, stock: any) {

    this.token = token;

    const params = new HttpParams()
    .set('token', token)
    .set('year', year)
    .set('warehouse_id', warehouseId)
    .set('product_id', productId);

    this.http.get(this.baseUrl + 'stock/get-stock', { params })
    .subscribe((response: any) => {
      let stockData: any[] = response.results;

      if(stockData.length === 0) {
        let iMonth: any;
        let iYear: any;

        if(month === '01') {
          iMonth = '12';
          iYear = parseInt(year) - 1;
        } else {
          iMonth = parseInt(month) - 1;
          iYear = year;
        }

        if(iMonth < 10 ) {
          iMonth = '0'+iMonth;
        } else {
          iMonth = iMonth.toString();
        }

        //cek stock di bulan dan tahun sebelumnya
        const params = new HttpParams()
          .set('token', token)
          .set('year', iYear)
          .set('warehouse_id', warehouseId)
          .set('product_id', productId);

        this.http.get(this.baseUrl + 'stock/get-stock', { params })
        .subscribe((response: any) => {
          let iStock: any;
          let iValue: any;

          if(response.results.length > 0) {
            let data: any[] = response.results;
            if(iMonth === '01') { iStock = data[0].end_01; iValue = data[0].end_value_01; }
            if(iMonth === '02') { iStock = data[0].end_02; iValue = data[0].end_value_02; }
            if(iMonth === '03') { iStock = data[0].end_03; iValue = data[0].end_value_03; }
            if(iMonth === '04') { iStock = data[0].end_04; iValue = data[0].end_value_04; }
            if(iMonth === '05') { iStock = data[0].end_05; iValue = data[0].end_value_05; }
            if(iMonth === '06') { iStock = data[0].end_06; iValue = data[0].end_value_06; }
            if(iMonth === '07') { iStock = data[0].end_07; iValue = data[0].end_value_07; }
            if(iMonth === '08') { iStock = data[0].end_08; iValue = data[0].end_value_08; }
            if(iMonth === '09') { iStock = data[0].end_09; iValue = data[0].end_value_09; }
            if(iMonth === '10') { iStock = data[0].end_10; iValue = data[0].end_value_10; }
            if(iMonth === '11') { iStock = data[0].end_11; iValue = data[0].end_value_11; }
            if(iMonth === '12') { iStock = data[0].end_12; iValue = data[0].end_value_12; }
          } else {
            iStock = 0;
            iValue = 0;
          }

          //Start: Insert new row stock
          const paramsOpts = new HttpParams()
            .set('token', token)

          const reqOpts = new HttpHeaders()
            .set('Accept', 'application/json'),
          formData = new FormData();
          formData.append('year', year);
          formData.append('month', month);
          formData.append('warehouse_id', warehouseId);
          formData.append('product_id', productId);
          formData.append('stock', iStock);
          formData.append('value', iValue);

          this.http.post(this.baseUrl + 'stock/store-new-row', formData, { headers: reqOpts, params: paramsOpts })
          .subscribe((response: any) => {
            if(response.status.code === 201) {
              let result: any = this.stockManagementPhase2(year, month, warehouseId, productId, mutationType, stock);
              console.log(result);
              if(result.code === 1) {
                return {
                  code      : 1,
                  message   : result.message
                };
              } else {
                return {
                  code      : 0,
                  message   : result.message
                };
              }
            } else {
              return {
                code      : 0,
                message   : 'Gagal input data stock baru'
              };
            }
          }, () => {
            return {
              code      : 0,
              message   : 'Terdapat error saat input data stock baru'
            };
          });
          //End: Insert new row stock
        }, () => {
          return {
            code      : 0,
            message   : 'Terdapat error saat mengambil data stock dari server'
          };
        });
      } else {
        let result: any = this.stockManagementPhase2(year, month, warehouseId, productId, mutationType, stock);
        console.log(result);
        if(result.code === 1) {
          return {
            code      : 1,
            message   : result.message
          };
        } else {
          return {
            code      : 0,
            message   : result.message
          };
        }
      }
    }, () => {
      return {
        code      : 0,
        message   : 'Terdapat error saat mengambil data stock dari server'
      };
    });
  }

  stockManagementPhase2(year: any, month: any, warehouseId: any, productId: any,
    mutationType: any, stock: any) {

    let iMonth: any;
    let iYear: any;
    let earlyStock: any = 0;
    let earlyValue: any = 0;
    let inStock: any = 0;
    let inValue: any = 0;
    let outStock: any = 0;
    let outValue: any = 0;
    let endStock: any = 0;
    let endValue: any = 0;

    if(month === '01') {
      iMonth = '12';
      iYear = parseInt(year) - 1;
    } else {
      iMonth = parseInt(month) - 1;
      iYear = year;
    }

    if(iMonth < 10 ) {
      iMonth = '0'+iMonth;
    } else {
      iMonth = iMonth.toString();
    }

    //Start: update stock awal, ambil dari stock akhir pada bulan sebelumnya
    const params = new HttpParams()
    .set('token', this.token)
    .set('year', iYear)
    .set('warehouse_id', warehouseId)
    .set('product_id', productId);

    this.http.get(this.baseUrl + 'stock/get-stock', { params })
    .subscribe((response: any) => {
      let data: any[] = response.results;

      if(response.results.length > 0) {
        if(iMonth === '01') { earlyStock = data[0].end_01; earlyValue = data[0].end_value_01; }
        if(iMonth === '02') { earlyStock = data[0].end_02; earlyValue = data[0].end_value_02; }
        if(iMonth === '03') { earlyStock = data[0].end_03; earlyValue = data[0].end_value_03; }
        if(iMonth === '04') { earlyStock = data[0].end_04; earlyValue = data[0].end_value_04; }
        if(iMonth === '05') { earlyStock = data[0].end_05; earlyValue = data[0].end_value_05; }
        if(iMonth === '06') { earlyStock = data[0].end_06; earlyValue = data[0].end_value_06; }
        if(iMonth === '07') { earlyStock = data[0].end_07; earlyValue = data[0].end_value_07; }
        if(iMonth === '08') { earlyStock = data[0].end_08; earlyValue = data[0].end_value_08; }
        if(iMonth === '09') { earlyStock = data[0].end_09; earlyValue = data[0].end_value_09; }
        if(iMonth === '10') { earlyStock = data[0].end_10; earlyValue = data[0].end_value_10; }
        if(iMonth === '11') { earlyStock = data[0].end_11; earlyValue = data[0].end_value_11; }
        if(iMonth === '12') { earlyStock = data[0].end_12; earlyValue = data[0].end_value_12; }
      } else {
        earlyStock = 0;
        earlyValue = 0;
      }

      if(mutationType === 'I') {
        if(month === '01') { inStock = data[0].in_01; inValue = data[0].in_value_01; }
        if(month === '02') { inStock = data[0].in_02; inValue = data[0].in_value_02; }
        if(month === '03') { inStock = data[0].in_03; inValue = data[0].in_value_03; }
        if(month === '04') { inStock = data[0].in_04; inValue = data[0].in_value_04; }
        if(month === '05') { inStock = data[0].in_05; inValue = data[0].in_value_05; }
        if(month === '06') { inStock = data[0].in_06; inValue = data[0].in_value_06; }
        if(month === '07') { inStock = data[0].in_07; inValue = data[0].in_value_07; }
        if(month === '08') { inStock = data[0].in_08; inValue = data[0].in_value_08; }
        if(month === '09') { inStock = data[0].in_09; inValue = data[0].in_value_09; }
        if(month === '10') { inStock = data[0].in_10; inValue = data[0].in_value_10; }
        if(month === '11') { inStock = data[0].in_11; inValue = data[0].in_value_11; }
        if(month === '12') { inStock = data[0].in_12; inValue = data[0].in_value_12; }

        inStock = parseInt(inStock) + parseInt(stock);
        inValue = parseInt(inValue) + 0;
      }

      if (mutationType === 'O') {
        if(month === '01') { outStock = data[0].out_01; outValue = data[0].out_value_01; }
        if(month === '02') { outStock = data[0].out_02; outValue = data[0].out_value_02; }
        if(month === '03') { outStock = data[0].out_03; outValue = data[0].out_value_03; }
        if(month === '04') { outStock = data[0].out_04; outValue = data[0].out_value_04; }
        if(month === '05') { outStock = data[0].out_05; outValue = data[0].out_value_05; }
        if(month === '06') { outStock = data[0].out_06; outValue = data[0].out_value_06; }
        if(month === '07') { outStock = data[0].out_07; outValue = data[0].out_value_07; }
        if(month === '08') { outStock = data[0].out_08; outValue = data[0].out_value_08; }
        if(month === '09') { outStock = data[0].out_09; outValue = data[0].out_value_09; }
        if(month === '10') { outStock = data[0].out_10; outValue = data[0].out_value_10; }
        if(month === '11') { outStock = data[0].out_11; outValue = data[0].out_value_11; }
        if(month === '12') { outStock = data[0].out_12; outValue = data[0].out_value_12; }

        outStock = parseInt(outStock) + parseInt(stock);
        outValue = parseInt(outValue) + 0;
      }

      endStock = parseInt(earlyStock) + parseInt(inStock) - parseInt(outStock);
      endValue = parseInt(earlyValue) + parseInt(inValue) - parseInt(outValue);

      const paramsOpts = new HttpParams()
        .set('token', this.token)

      const reqOpts = new HttpHeaders()
        .set('Accept', 'application/json'),
        formData = new FormData()
        formData.append('year', year);
        formData.append('month', month);
        formData.append('warehouse_id', warehouseId);
        formData.append('product_id', productId);
        formData.append('early_stock', earlyStock);
        formData.append('early_value', earlyValue);
        formData.append('in_stock', inStock);
        formData.append('in_value', inValue);
        formData.append('out_stock', outStock);
        formData.append('out_value', outValue);
        formData.append('end_stock', endStock);
        formData.append('end_value', endValue);
        formData.append('_method', 'PUT');

      this.http.post(this.baseUrl + 'stock/update-stock', formData, { headers: reqOpts, params: paramsOpts })
      .subscribe((response: any) => {
        if(response.status.code === 201) {
          return {
            code      : 1,
            message   : 'Berhasil update data stock'
          };
        } else {
          return {
            code      : 0,
            message   : 'Gagal update data stock'
          };
        }
      }, () => {
        return {
          code      : 0,
          message   : 'Terdapat error saat update data stock'
        };
      });
    }, () => {
      return {
        code      : 0,
        message   : 'Terdapat error saat mengambil data stock dari server'
      };
    });
  }
}
